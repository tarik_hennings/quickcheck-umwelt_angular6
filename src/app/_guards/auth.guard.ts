﻿import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router, private cookieService: CookieService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        // Diese Methode wird vom Router vor jeder geschützten Route aufgerufen
        // hier wird einfach ein Test gemacht, ob 
        if (localStorage.getItem('currentUser') ) {
            // logged in so return true
            console.log("Cookie canActivate: ",document.cookie)
            console.log("Authguard: yes, logged in -> currentUser: " + localStorage.getItem('currentUser'))
            return true;
        }
        // not logged in so redirect to login page with the return url
        this.router.navigate(['/login']);//,{ queryParams: { returnUrl: state.url }}
        return false;
    }
    deleteCurrentUser(){
        localStorage.removeItem('currentUser')
        //schick den User zurück zum login
        this.router.navigateByUrl('/login')
    }
}