import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KundeneingabeComponent } from './kundeneingabe.component';

describe('KundeneingabeComponent', () => {
  let component: KundeneingabeComponent;
  let fixture: ComponentFixture<KundeneingabeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KundeneingabeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KundeneingabeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
