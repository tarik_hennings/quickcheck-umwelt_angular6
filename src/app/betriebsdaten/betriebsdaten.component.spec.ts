import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BetriebsdatenComponent } from './betriebsdaten.component';

describe('BetriebsdatenComponent', () => {
  let component: BetriebsdatenComponent;
  let fixture: ComponentFixture<BetriebsdatenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BetriebsdatenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BetriebsdatenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
