import { Anlage } from '../_models/anlage';
import { begleitfrage } from '../_models/begleitfrage';
import { Restabfalltonne } from '../_models/restabfalltonne';
import { Wellnessbereich } from '../_models/wellnessbereich';
import { FeWo } from '../_models/FeWo';
import { Hotel } from './hotel';

export class Einrichtung {
    einrichtungs_id: number
    ferienwohnungseinrichtungs_id: number
    hotel_id: number
    jugendherbergs_id: number
    wellnessbereich_id: number
    anzahl_bettenuebernachtungen_pro_jahr: number
    anzahl_der_einrichtungen: number
    anzahl_der_sterne: number
    anzahl_privater_bewohner: number
    tagungsraumflaeche : number
    beheizte_gesamtflaeche_in_m3: number
    davon_privatwohnflaeche_in_m2: number
    eigenwaesche_in_kg: number
    anzahl_mahlzeiten_im_jahr: number
    anzahl_restaurantsitzplaetze : number
    wasserverbrauch_in_m3: number
    abfall_in_kg_pro_jahr: number
    stromverbrauch_in_kwh : number
    oekostromverbrauch_in_kwh : number
    anteil_mehrgaenge_menues : number
    Hotel: Hotel
    Jugendherberge: any
    FeWo: FeWo
    Wellnessbereich: Wellnessbereich
    Anlagen: Anlage[]
    Begleitfrage: begleitfrage[]
    Restabfalltonnen: Restabfalltonne[]
    privatWohnungInGesamtFlaecheEnthalten: boolean
    kein_privater_wasserzaehler : boolean
	kein_privater_stromzaehler: boolean
    kein_privater_heizungszaehler: boolean
    kein_privater_restmuell : boolean

    constructor(){
        this.einrichtungs_id = 0
        this.ferienwohnungseinrichtungs_id=0
        this.hotel_id = null
        this.jugendherbergs_id = null
        this.wellnessbereich_id = null
        this.anzahl_bettenuebernachtungen_pro_jahr=0
        this.anzahl_der_einrichtungen=0
        this.anzahl_der_sterne=0
        this.anzahl_privater_bewohner=0
        this.tagungsraumflaeche=0
        this.beheizte_gesamtflaeche_in_m3=0
        this.davon_privatwohnflaeche_in_m2=0
        this.eigenwaesche_in_kg=0
        this.anzahl_mahlzeiten_im_jahr=0
        this.wasserverbrauch_in_m3=0
        this.abfall_in_kg_pro_jahr=0
        this.Hotel = null
        this.Jugendherberge = null
        this.FeWo = null
        this.Wellnessbereich = new Wellnessbereich()
        this.Anlagen = new Array<Anlage>()
        this.Begleitfrage= new Array<begleitfrage>()
        this.Restabfalltonnen= new Array<Restabfalltonne>()
        this.privatWohnungInGesamtFlaecheEnthalten = null
    }
}