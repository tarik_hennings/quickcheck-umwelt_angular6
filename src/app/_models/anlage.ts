export class Anlage {
   anlagen_id : number;
   einrichtungs_id: number;
   typ : string;
   name: string;
   einheit: string;
   rohstoff_verbrauch : number;
   strom_eigenverbrauch : number;
   strom_netzeinspeisung : number;
}