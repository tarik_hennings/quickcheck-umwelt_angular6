import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StromHeizungComponent } from './strom-heizung.component';

describe('StromHeizungComponent', () => {
  let component: StromHeizungComponent;
  let fixture: ComponentFixture<StromHeizungComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StromHeizungComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StromHeizungComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
