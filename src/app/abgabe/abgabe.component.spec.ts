import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AbgabeComponent } from './abgabe.component';

describe('AbgabeComponent', () => {
  let component: AbgabeComponent;
  let fixture: ComponentFixture<AbgabeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AbgabeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbgabeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
