import { TestBed } from '@angular/core/testing';

import { PruefService } from './pruefservice.service';

describe('PruefService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PruefService = TestBed.get(PruefService);
    expect(service).toBeTruthy();
  });
});
