import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestabfallComponent } from './restabfall.component';

describe('RestabfallComponent', () => {
  let component: RestabfallComponent;
  let fixture: ComponentFixture<RestabfallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestabfallComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestabfallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
