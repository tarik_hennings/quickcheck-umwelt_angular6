(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/_directives/alert.component.html":
/*!**************************************************!*\
  !*** ./src/app/_directives/alert.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"message\" [ngClass]=\"{ 'alert': message, 'alert-success': message.type === 'success', 'alert-danger': message.type === 'error' }\">{{message.text}}</div>"

/***/ }),

/***/ "./src/app/_directives/alert.component.ts":
/*!************************************************!*\
  !*** ./src/app/_directives/alert.component.ts ***!
  \************************************************/
/*! exports provided: AlertComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlertComponent", function() { return AlertComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_index__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../_services/index */ "./src/app/_services/index.ts");



var AlertComponent = /** @class */ (function () {
    function AlertComponent(alertService) {
        this.alertService = alertService;
    }
    AlertComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.alertService.getMessage().subscribe(function (message) { _this.message = message; });
    };
    AlertComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'alert',
            template: __webpack_require__(/*! ./alert.component.html */ "./src/app/_directives/alert.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_index__WEBPACK_IMPORTED_MODULE_2__["AlertService"]])
    ], AlertComponent);
    return AlertComponent;
}());



/***/ }),

/***/ "./src/app/_directives/index.ts":
/*!**************************************!*\
  !*** ./src/app/_directives/index.ts ***!
  \**************************************/
/*! exports provided: AlertComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _alert_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./alert.component */ "./src/app/_directives/alert.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AlertComponent", function() { return _alert_component__WEBPACK_IMPORTED_MODULE_0__["AlertComponent"]; });




/***/ }),

/***/ "./src/app/_guards/auth.guard.ts":
/*!***************************************!*\
  !*** ./src/app/_guards/auth.guard.ts ***!
  \***************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/index.js");




var AuthGuard = /** @class */ (function () {
    function AuthGuard(router, cookieService) {
        this.router = router;
        this.cookieService = cookieService;
    }
    AuthGuard.prototype.canActivate = function (route, state) {
        // Diese Methode wird vom Router vor jeder geschützten Route aufgerufen
        // hier wird einfach ein Test gemacht, ob 
        if (localStorage.getItem('currentUser')) {
            // logged in so return true
            console.log("Cookie canActivate: ", document.cookie);
            console.log("Authguard: yes, logged in -> currentUser: " + localStorage.getItem('currentUser'));
            return true;
        }
        // not logged in so redirect to login page with the return url
        this.router.navigate(['/login']); //,{ queryParams: { returnUrl: state.url }}
        return false;
    };
    AuthGuard.prototype.deleteCurrentUser = function () {
        localStorage.removeItem('currentUser');
        //schick den User zurück zum login
        this.router.navigateByUrl('/login');
    };
    AuthGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], ngx_cookie_service__WEBPACK_IMPORTED_MODULE_3__["CookieService"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/_guards/index.ts":
/*!**********************************!*\
  !*** ./src/app/_guards/index.ts ***!
  \**********************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _auth_guard__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./auth.guard */ "./src/app/_guards/auth.guard.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return _auth_guard__WEBPACK_IMPORTED_MODULE_0__["AuthGuard"]; });




/***/ }),

/***/ "./src/app/_helpers/fake-backend.ts":
/*!******************************************!*\
  !*** ./src/app/_helpers/fake-backend.ts ***!
  \******************************************/
/*! exports provided: FakeBackendInterceptor, fakeBackendProvider */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FakeBackendInterceptor", function() { return FakeBackendInterceptor; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fakeBackendProvider", function() { return fakeBackendProvider; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_Observable__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/Observable */ "./node_modules/rxjs-compat/_esm5/Observable.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_add_observable_of__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/add/observable/of */ "./node_modules/rxjs-compat/_esm5/add/observable/of.js");
/* harmony import */ var rxjs_add_observable_throw__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/add/observable/throw */ "./node_modules/rxjs-compat/_esm5/add/observable/throw.js");
/* harmony import */ var rxjs_add_operator_delay__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/add/operator/delay */ "./node_modules/rxjs-compat/_esm5/add/operator/delay.js");
/* harmony import */ var rxjs_add_operator_mergeMap__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/add/operator/mergeMap */ "./node_modules/rxjs-compat/_esm5/add/operator/mergeMap.js");
/* harmony import */ var rxjs_add_operator_materialize__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/add/operator/materialize */ "./node_modules/rxjs-compat/_esm5/add/operator/materialize.js");
/* harmony import */ var rxjs_add_operator_dematerialize__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! rxjs/add/operator/dematerialize */ "./node_modules/rxjs-compat/_esm5/add/operator/dematerialize.js");











var FakeBackendInterceptor = /** @class */ (function () {
    function FakeBackendInterceptor() {
    }
    FakeBackendInterceptor.prototype.intercept = function (request, next) {
        // array in local storage for registered users
        var users = JSON.parse(localStorage.getItem('users')) || [];
        // wrap in delayed observable to simulate server api call
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["of"])(null).mergeMap(function () {
            // authenticate
            if (request.url.endsWith('/api/authenticate') && request.method === 'POST') {
                // find if any user matches login credentials
                var filteredUsers = users.filter(function (user) {
                    return user.username === request.body.username && user.password === request.body.password;
                });
                if (filteredUsers.length) {
                    // if login details are valid return 200 OK with user details and fake jwt token
                    var user = filteredUsers[0];
                    var body = {
                        id: user.id,
                        username: user.username,
                        firstName: user.firstName,
                        lastName: user.lastName,
                        token: 'fake-jwt-token'
                    };
                    return rxjs_Observable__WEBPACK_IMPORTED_MODULE_3__["Observable"].of(new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpResponse"]({ status: 200, body: body }));
                }
                else {
                    // else return 400 bad request
                    return rxjs_Observable__WEBPACK_IMPORTED_MODULE_3__["Observable"].throw('Username or password is incorrect');
                }
            }
            // get users
            if (request.url.endsWith('/api/users') && request.method === 'GET') {
                // check for fake auth token in header and return users if valid, this security is implemented server side in a real application
                if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                    return rxjs_Observable__WEBPACK_IMPORTED_MODULE_3__["Observable"].of(new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpResponse"]({ status: 200, body: users }));
                }
                else {
                    // return 401 not authorised if token is null or invalid
                    return rxjs_Observable__WEBPACK_IMPORTED_MODULE_3__["Observable"].throw('Unauthorised');
                }
            }
            // get user by id
            if (request.url.match(/\/api\/users\/\d+$/) && request.method === 'GET') {
                // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
                if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                    // find user by id in users array
                    var urlParts = request.url.split('/');
                    var id_1 = parseInt(urlParts[urlParts.length - 1]);
                    var matchedUsers = users.filter(function (user) { return user.id === id_1; });
                    var user = matchedUsers.length ? matchedUsers[0] : null;
                    return rxjs_Observable__WEBPACK_IMPORTED_MODULE_3__["Observable"].of(new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpResponse"]({ status: 200, body: user }));
                }
                else {
                    // return 401 not authorised if token is null or invalid
                    return rxjs_Observable__WEBPACK_IMPORTED_MODULE_3__["Observable"].throw('Unauthorised');
                }
            }
            // create user
            if (request.url.endsWith('/api/users') && request.method === 'POST') {
                // get new user object from post body
                var newUser_1 = request.body;
                // validation
                var duplicateUser = users.filter(function (user) { return user.username === newUser_1.username; }).length;
                if (duplicateUser) {
                    return rxjs_Observable__WEBPACK_IMPORTED_MODULE_3__["Observable"].throw('Username "' + newUser_1.username + '" is already taken');
                }
                // save new user
                newUser_1.id = users.length + 1;
                users.push(newUser_1);
                localStorage.setItem('users', JSON.stringify(users));
                // respond 200 OK
                return rxjs_Observable__WEBPACK_IMPORTED_MODULE_3__["Observable"].of(new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpResponse"]({ status: 200 }));
            }
            // delete user
            if (request.url.match(/\/api\/users\/\d+$/) && request.method === 'DELETE') {
                // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
                if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                    // find user by id in users array
                    var urlParts = request.url.split('/');
                    var id = parseInt(urlParts[urlParts.length - 1]);
                    for (var i = 0; i < users.length; i++) {
                        var user = users[i];
                        if (user.id === id) {
                            // delete user
                            users.splice(i, 1);
                            localStorage.setItem('users', JSON.stringify(users));
                            break;
                        }
                    }
                    // respond 200 OK
                    return rxjs_Observable__WEBPACK_IMPORTED_MODULE_3__["Observable"].of(new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpResponse"]({ status: 200 }));
                }
                else {
                    // return 401 not authorised if token is null or invalid
                    return rxjs_Observable__WEBPACK_IMPORTED_MODULE_3__["Observable"].throw('Unauthorised');
                }
            }
            // pass through any requests not handled above
            return next.handle(request);
        })
            // call materialize and dematerialize to ensure delay even if an error is thrown (https://github.com/Reactive-Extensions/RxJS/issues/648)
            .materialize()
            .delay(500)
            .dematerialize();
    };
    FakeBackendInterceptor = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], FakeBackendInterceptor);
    return FakeBackendInterceptor;
}());

var fakeBackendProvider = {
    // use fake backend in place of Http service for backend-less development
    provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HTTP_INTERCEPTORS"],
    useClass: FakeBackendInterceptor,
    multi: true
};


/***/ }),

/***/ "./src/app/_helpers/index.ts":
/*!***********************************!*\
  !*** ./src/app/_helpers/index.ts ***!
  \***********************************/
/*! exports provided: JwtInterceptor, FakeBackendInterceptor, fakeBackendProvider */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _jwt_interceptor__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./jwt.interceptor */ "./src/app/_helpers/jwt.interceptor.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "JwtInterceptor", function() { return _jwt_interceptor__WEBPACK_IMPORTED_MODULE_0__["JwtInterceptor"]; });

/* harmony import */ var _fake_backend__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./fake-backend */ "./src/app/_helpers/fake-backend.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "FakeBackendInterceptor", function() { return _fake_backend__WEBPACK_IMPORTED_MODULE_1__["FakeBackendInterceptor"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "fakeBackendProvider", function() { return _fake_backend__WEBPACK_IMPORTED_MODULE_1__["fakeBackendProvider"]; });





/***/ }),

/***/ "./src/app/_helpers/jwt.interceptor.ts":
/*!*********************************************!*\
  !*** ./src/app/_helpers/jwt.interceptor.ts ***!
  \*********************************************/
/*! exports provided: JwtInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JwtInterceptor", function() { return JwtInterceptor; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var JwtInterceptor = /** @class */ (function () {
    function JwtInterceptor() {
    }
    JwtInterceptor.prototype.intercept = function (request, next) {
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (currentUser && currentUser.token) {
            request = request.clone({
                setHeaders: {
                    Authorization: "Bearer " + currentUser.token
                }
            });
        }
        return next.handle(request);
    };
    JwtInterceptor = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], JwtInterceptor);
    return JwtInterceptor;
}());



/***/ }),

/***/ "./src/app/_models/FeWo.ts":
/*!*********************************!*\
  !*** ./src/app/_models/FeWo.ts ***!
  \*********************************/
/*! exports provided: FeWo */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FeWo", function() { return FeWo; });
var FeWo = /** @class */ (function () {
    function FeWo() {
        this.ferienwohnungseinrichtungs_id = 0;
        this.anzahl_der_ferienwohnungen = 0;
        this.anzahl_der_uebernachtungen_mai_bis_oktober = 0;
        this.anzahl_der_uebernachtungen_november_bis_april = 0;
        this.anzahl_der_gaesteanreisen_pro_jahr = 0;
        this.durchschnittliche_groesse_der_fe_wo_in_m2 = 0;
        this.durchschnittliche_maximale_belegungszahl_der_fe_wo = 0;
    }
    return FeWo;
}());



/***/ }),

/***/ "./src/app/_models/adresse.ts":
/*!************************************!*\
  !*** ./src/app/_models/adresse.ts ***!
  \************************************/
/*! exports provided: Adresse */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Adresse", function() { return Adresse; });
var Adresse = /** @class */ (function () {
    function Adresse() {
        this.adress_id = 0;
        this.typ = "";
        this.name = "";
        this.adresszusatz = "";
        this.strasse = "";
        this.ort = "";
        this.postleitzahl = "";
        this.land = "";
    }
    return Adresse;
}());



/***/ }),

/***/ "./src/app/_models/adressen.ts":
/*!*************************************!*\
  !*** ./src/app/_models/adressen.ts ***!
  \*************************************/
/*! exports provided: Adressen */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Adressen", function() { return Adressen; });
/* harmony import */ var _models_adresse__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../_models/adresse */ "./src/app/_models/adresse.ts");

var Adressen = /** @class */ (function () {
    function Adressen() {
        this.Firmenadresse = new _models_adresse__WEBPACK_IMPORTED_MODULE_0__["Adresse"]();
        this.Rechnungsadresse = new _models_adresse__WEBPACK_IMPORTED_MODULE_0__["Adresse"]();
    }
    return Adressen;
}());



/***/ }),

/***/ "./src/app/_models/anlage.ts":
/*!***********************************!*\
  !*** ./src/app/_models/anlage.ts ***!
  \***********************************/
/*! exports provided: Anlage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Anlage", function() { return Anlage; });
var Anlage = /** @class */ (function () {
    function Anlage() {
    }
    return Anlage;
}());



/***/ }),

/***/ "./src/app/_models/begleitfrage.ts":
/*!*****************************************!*\
  !*** ./src/app/_models/begleitfrage.ts ***!
  \*****************************************/
/*! exports provided: begleitfrage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "begleitfrage", function() { return begleitfrage; });
var begleitfrage = /** @class */ (function () {
    function begleitfrage() {
        this.begleitfrage_id = 0;
        this.einrichtungs_id = 0;
        this.datums_typ = "";
        this.antwort = false;
    }
    return begleitfrage;
}());



/***/ }),

/***/ "./src/app/_models/bhkwvalues.ts":
/*!***************************************!*\
  !*** ./src/app/_models/bhkwvalues.ts ***!
  \***************************************/
/*! exports provided: BHKWvalues */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BHKWvalues", function() { return BHKWvalues; });
var BHKWvalues = /** @class */ (function () {
    function BHKWvalues() {
        this.BHKWvalues = [];
        this.BHKWvalues.push("Mit Erdgas (in kWh angegeben) betriebenes BHKW", "Mit Erdgas (in m³ angegeben) betriebenes BHKW", "Mit Heizoel (in Litern angegeben) betriebenes BHKW", "Mit Fluessiggas (in Litern angegeben) betriebenes BHKW", "Mit Fluessiggas (in kg angegeben) betriebenes BHKW", "Mit Fernwaerme (in kWh angegeben) betriebenes BHKW", "Mit Hackschnitzel/Scheitholz (in kg angegeben) betriebenes BHKW", "Mit Pellets (in kg angegeben) betriebenes BHKW", "Mit Solarthermie (in kWh angegeben) betriebenes BHKW", "Mit Geothermie (in kWh angegeben) betriebenes BHKW", "Mit Nahwaerme (in kWh angegeben) betriebenes BHKW");
    }
    return BHKWvalues;
}());



/***/ }),

/***/ "./src/app/_models/grouppruefarray.ts":
/*!********************************************!*\
  !*** ./src/app/_models/grouppruefarray.ts ***!
  \********************************************/
/*! exports provided: GroupPruefArray */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GroupPruefArray", function() { return GroupPruefArray; });
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");

var GroupPruefArray = /** @class */ (function () {
    function GroupPruefArray() {
        this.FormGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormGroup"]({});
        this.FormControlNames = new Array();
        this.FormControlErrors = new Array();
    }
    return GroupPruefArray;
}());



/***/ }),

/***/ "./src/app/_models/heizmittelnamen.ts":
/*!********************************************!*\
  !*** ./src/app/_models/heizmittelnamen.ts ***!
  \********************************************/
/*! exports provided: Heizmittelnamen */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Heizmittelnamen", function() { return Heizmittelnamen; });
var Heizmittelnamen = /** @class */ (function () {
    function Heizmittelnamen() {
        //Heizmittelnamen: {value: string; name: string}[]=[];
        this.Heizmittelnamen = [];
        /*
        this.Heizmittelnamen.push({value:"Erdgas",name: "Erdgas"})
        this.Heizmittelnamen.push({value:"Heizoel",name: "Heizöl"})
        this.Heizmittelnamen.push({value:"Fluessiggas",name: "Flüssiggas"})
        this.Heizmittelnamen.push({value:"Fernwaerme",name: "Fernwärme"})
        this.Heizmittelnamen.push({value:"Heizung",name: "Hackschnitzel/Scheitholz"})
        this.Heizmittelnamen.push({value:"Pellets",name: "Pellets"})
        this.Heizmittelnamen.push({value:"Solarthermie",name: "Solarthermie"})
        this.Heizmittelnamen.push({value:"Geothermie",name: "Geothermie"})
        this.Heizmittelnamen.push({value:"Nahwaerme",name: "Nahwärme"})
        */
        this.Heizmittelnamen.push("Erdgas in kWh", "Erdgas in m³", "Heizoel", "Fluessiggas in Litern", "Fluessiggas in kg", "Fernwaerme", "Hackschnitzel/Scheitholz", "Pellets", "Solarthermie", "Geothermie", "Nahwärme");
    }
    return Heizmittelnamen;
}());



/***/ }),

/***/ "./src/app/_models/heizmittelvalue.ts":
/*!********************************************!*\
  !*** ./src/app/_models/heizmittelvalue.ts ***!
  \********************************************/
/*! exports provided: Heizmittelvalue */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Heizmittelvalue", function() { return Heizmittelvalue; });
var Heizmittelvalue = /** @class */ (function () {
    function Heizmittelvalue() {
        this.Heizmittelvalues = [];
        this.Heizmittelvalues.push("Heizung Erdgas in kWh", "Heizung Erdgas in m³", "Heizung Heizoel in Litern", "Heizung Fluessiggas in Litern", "Heizung Fluessiggas in kg", "Heizung Fernwaerme in kWh", "Heizung Hackschnitzel/Scheitholz in t", "Heizung Hackschnitzel/Scheitholz in m³", "Heizung Pellets in kg", "Heizung Solarthermie in kWh", "Heizung Geothermie in kWh", "Heizung Nahwaerme in kWh");
    }
    return Heizmittelvalue;
}());



/***/ }),

/***/ "./src/app/_models/hotel.ts":
/*!**********************************!*\
  !*** ./src/app/_models/hotel.ts ***!
  \**********************************/
/*! exports provided: Hotel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Hotel", function() { return Hotel; });
var Hotel = /** @class */ (function () {
    function Hotel() {
        this.hotel_id = 0;
        this.hotel_kategorie = '';
    }
    return Hotel;
}());



/***/ }),

/***/ "./src/app/_models/index.ts":
/*!**********************************!*\
  !*** ./src/app/_models/index.ts ***!
  \**********************************/
/*! exports provided: Anlage, User, Restabfalltonne, nullString, nullInt, Adresse, Kunde, Adressen, Umweltcheck, Umweltchecks, FeWo, Heizmittelnamen, Heizmittelvalue, BHKWvalues, Hotel, GroupPruefArray */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _anlage__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./anlage */ "./src/app/_models/anlage.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Anlage", function() { return _anlage__WEBPACK_IMPORTED_MODULE_0__["Anlage"]; });

/* harmony import */ var _user__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./user */ "./src/app/_models/user.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "User", function() { return _user__WEBPACK_IMPORTED_MODULE_1__["User"]; });

/* harmony import */ var _restabfalltonne__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./restabfalltonne */ "./src/app/_models/restabfalltonne.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Restabfalltonne", function() { return _restabfalltonne__WEBPACK_IMPORTED_MODULE_2__["Restabfalltonne"]; });

/* harmony import */ var _nullString__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./nullString */ "./src/app/_models/nullString.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "nullString", function() { return _nullString__WEBPACK_IMPORTED_MODULE_3__["nullString"]; });

/* harmony import */ var _null__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./null */ "./src/app/_models/null.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "nullInt", function() { return _null__WEBPACK_IMPORTED_MODULE_4__["nullInt"]; });

/* harmony import */ var _adresse__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./adresse */ "./src/app/_models/adresse.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Adresse", function() { return _adresse__WEBPACK_IMPORTED_MODULE_5__["Adresse"]; });

/* harmony import */ var _kunde__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./kunde */ "./src/app/_models/kunde.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Kunde", function() { return _kunde__WEBPACK_IMPORTED_MODULE_6__["Kunde"]; });

/* harmony import */ var _adressen__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./adressen */ "./src/app/_models/adressen.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Adressen", function() { return _adressen__WEBPACK_IMPORTED_MODULE_7__["Adressen"]; });

/* harmony import */ var _umweltcheck__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./umweltcheck */ "./src/app/_models/umweltcheck.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Umweltcheck", function() { return _umweltcheck__WEBPACK_IMPORTED_MODULE_8__["Umweltcheck"]; });

/* harmony import */ var _umweltchecks__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./umweltchecks */ "./src/app/_models/umweltchecks.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Umweltchecks", function() { return _umweltchecks__WEBPACK_IMPORTED_MODULE_9__["Umweltchecks"]; });

/* harmony import */ var _FeWo__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./FeWo */ "./src/app/_models/FeWo.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "FeWo", function() { return _FeWo__WEBPACK_IMPORTED_MODULE_10__["FeWo"]; });

/* harmony import */ var _heizmittelnamen__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./heizmittelnamen */ "./src/app/_models/heizmittelnamen.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Heizmittelnamen", function() { return _heizmittelnamen__WEBPACK_IMPORTED_MODULE_11__["Heizmittelnamen"]; });

/* harmony import */ var _heizmittelvalue__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./heizmittelvalue */ "./src/app/_models/heizmittelvalue.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Heizmittelvalue", function() { return _heizmittelvalue__WEBPACK_IMPORTED_MODULE_12__["Heizmittelvalue"]; });

/* harmony import */ var _bhkwvalues__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./bhkwvalues */ "./src/app/_models/bhkwvalues.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "BHKWvalues", function() { return _bhkwvalues__WEBPACK_IMPORTED_MODULE_13__["BHKWvalues"]; });

/* harmony import */ var _hotel__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./hotel */ "./src/app/_models/hotel.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Hotel", function() { return _hotel__WEBPACK_IMPORTED_MODULE_14__["Hotel"]; });

/* harmony import */ var _grouppruefarray__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./grouppruefarray */ "./src/app/_models/grouppruefarray.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "GroupPruefArray", function() { return _grouppruefarray__WEBPACK_IMPORTED_MODULE_15__["GroupPruefArray"]; });



















/***/ }),

/***/ "./src/app/_models/kunde.ts":
/*!**********************************!*\
  !*** ./src/app/_models/kunde.ts ***!
  \**********************************/
/*! exports provided: Kunde */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Kunde", function() { return Kunde; });
var Kunde = /** @class */ (function () {
    function Kunde() {
        this.kunden_id = 0;
        this.name_des_betriebes = '';
        this.kunden_nummer = "";
        this.ansprechpartner = '';
        this.ansprechpartner_email = '';
        this.ansprechpartner_telefon = "";
        this.ansprechpartner_mobil = "";
        this.notiz = "";
        this.firmen_adresse_id = 0;
        this.rechnungs_adresse_id = 0;
    }
    return Kunde;
}());



/***/ }),

/***/ "./src/app/_models/null.ts":
/*!*********************************!*\
  !*** ./src/app/_models/null.ts ***!
  \*********************************/
/*! exports provided: nullInt */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "nullInt", function() { return nullInt; });
var nullInt = /** @class */ (function () {
    function nullInt() {
    }
    return nullInt;
}());



/***/ }),

/***/ "./src/app/_models/nullString.ts":
/*!***************************************!*\
  !*** ./src/app/_models/nullString.ts ***!
  \***************************************/
/*! exports provided: nullString */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "nullString", function() { return nullString; });
var nullString = /** @class */ (function () {
    function nullString() {
    }
    return nullString;
}());



/***/ }),

/***/ "./src/app/_models/restabfalltonne.ts":
/*!********************************************!*\
  !*** ./src/app/_models/restabfalltonne.ts ***!
  \********************************************/
/*! exports provided: Restabfalltonne */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Restabfalltonne", function() { return Restabfalltonne; });
var Restabfalltonne = /** @class */ (function () {
    function Restabfalltonne() {
    }
    return Restabfalltonne;
}());



/***/ }),

/***/ "./src/app/_models/umweltcheck.ts":
/*!****************************************!*\
  !*** ./src/app/_models/umweltcheck.ts ***!
  \****************************************/
/*! exports provided: Umweltcheck */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Umweltcheck", function() { return Umweltcheck; });
var Umweltcheck = /** @class */ (function () {
    function Umweltcheck() {
        this.umweltcheck_id = 0;
        this.einrichtungs_id = 0;
        this.kunden_id = 0;
        this.erstellungdatum = Date.now();
        this.umweltcheck_name = '';
        this.co2_faktoren_id = 1;
    }
    return Umweltcheck;
}());



/***/ }),

/***/ "./src/app/_models/umweltchecks.ts":
/*!*****************************************!*\
  !*** ./src/app/_models/umweltchecks.ts ***!
  \*****************************************/
/*! exports provided: Umweltchecks */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Umweltchecks", function() { return Umweltchecks; });
var Umweltchecks = /** @class */ (function () {
    function Umweltchecks() {
    }
    return Umweltchecks;
}());



/***/ }),

/***/ "./src/app/_models/user.ts":
/*!*********************************!*\
  !*** ./src/app/_models/user.ts ***!
  \*********************************/
/*! exports provided: User */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User", function() { return User; });
var User = /** @class */ (function () {
    function User() {
    }
    return User;
}());



/***/ }),

/***/ "./src/app/_models/wellnessbereich.ts":
/*!********************************************!*\
  !*** ./src/app/_models/wellnessbereich.ts ***!
  \********************************************/
/*! exports provided: Wellnessbereich */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Wellnessbereich", function() { return Wellnessbereich; });
var Wellnessbereich = /** @class */ (function () {
    function Wellnessbereich() {
        this.wellnessbereich_id = 0;
        this.groesse_in_m2 = 0;
        this.anzahl_der_saunen = 0;
        this.volumen_schwimmbad_in_m3 = 0;
        this.Oeffnungswochen_pro_jahr = 0;
    }
    return Wellnessbereich;
}());



/***/ }),

/***/ "./src/app/_services/alert.service.ts":
/*!********************************************!*\
  !*** ./src/app/_services/alert.service.ts ***!
  \********************************************/
/*! exports provided: AlertService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlertService", function() { return AlertService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs_Subject__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/Subject */ "./node_modules/rxjs-compat/_esm5/Subject.js");




var AlertService = /** @class */ (function () {
    function AlertService(router) {
        var _this = this;
        this.router = router;
        this.subject = new rxjs_Subject__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.keepAfterNavigationChange = false;
        // clear alert message on route change
        router.events.subscribe(function (event) {
            if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationStart"]) {
                if (_this.keepAfterNavigationChange) {
                    // only keep for a single location change
                    _this.keepAfterNavigationChange = false;
                }
                else {
                    // clear alert
                    _this.subject.next();
                }
            }
        });
    }
    AlertService.prototype.success = function (message, keepAfterNavigationChange) {
        if (keepAfterNavigationChange === void 0) { keepAfterNavigationChange = false; }
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        this.subject.next({ type: 'success', text: message });
    };
    AlertService.prototype.error = function (message, keepAfterNavigationChange) {
        if (keepAfterNavigationChange === void 0) { keepAfterNavigationChange = false; }
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        this.subject.next({ type: 'error', text: message });
    };
    AlertService.prototype.getMessage = function () {
        return this.subject.asObservable();
    };
    AlertService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AlertService);
    return AlertService;
}());



/***/ }),

/***/ "./src/app/_services/authentication.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/_services/authentication.service.ts ***!
  \*****************************************************/
/*! exports provided: AuthenticationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthenticationService", function() { return AuthenticationService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/index.js");
/* harmony import */ var _services_data_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../_services/data.service */ "./src/app/_services/data.service.ts");






var AuthenticationService = /** @class */ (function () {
    function AuthenticationService(http, cookieService, Dataservice) {
        this.http = http;
        this.cookieService = cookieService;
        this.Dataservice = Dataservice;
    }
    AuthenticationService.prototype.ngOnInit = function () {
    };
    AuthenticationService.prototype.login = function (username, password) {
        //erstmal alle Cookies löschen, 
        //nicht dass man mit einem alten Cookie zum Server kommt
        //wäre einfacher, wenn man in golang das einfach auf Session Cookies gesetzt hätte...
        //alle Cookies löschen:
        this.cookies = this.cookieService.getAll();
        console.log("COOKIES: ", this.cookies);
        this.cookieService.deleteAll();
        this.cookieService.deleteAll('/ ', '/');
        this.cookieService.deleteAll('/ ');
        this.cookieService.deleteAll('./ ');
        this.cookieService.deleteAll('/');
        this.cookieService.deleteAll('./');
        this.cookieService.deleteAll('../');
        return this.http.post("https://www.quickcheckumwelt.de/go/login/", { username: username, password: password })
            .map(function (user) {
            if (user.username) {
                //success, weil Server mit Username und Passwort geantwortet hat !
                console.log("successfull logged in: " + user.username);
                localStorage.setItem('currentUser', JSON.stringify(user));
            }
            return user;
        });
    };
    AuthenticationService.prototype.logout = function () {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        this.http.post("https://www.quickcheckumwelt.de/go/logout/", [])
            .subscribe(function (response) { return console.log(response); }, function (error) {
            console.log("Logout", error);
        });
    };
    AuthenticationService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], ngx_cookie_service__WEBPACK_IMPORTED_MODULE_4__["CookieService"], _services_data_service__WEBPACK_IMPORTED_MODULE_5__["DataService"]])
    ], AuthenticationService);
    return AuthenticationService;
}());



/***/ }),

/***/ "./src/app/_services/data.service.ts":
/*!*******************************************!*\
  !*** ./src/app/_services/data.service.ts ***!
  \*******************************************/
/*! exports provided: DataService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataService", function() { return DataService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _models_index__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../_models/index */ "./src/app/_models/index.ts");
/* harmony import */ var _models_umweltchecks__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../_models/umweltchecks */ "./src/app/_models/umweltchecks.ts");
/* harmony import */ var rxjs_Rx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/Rx */ "./node_modules/rxjs-compat/_esm5/Rx.js");
/* harmony import */ var _guards_index__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../_guards/index */ "./src/app/_guards/index.ts");








var DataService = /** @class */ (function () {
    function DataService(http, authGuardService) {
        this.http = http;
        this.authGuardService = authGuardService;
        this.rootDomain = "https://www.quickcheckumwelt.de";
        this.headersOn = false;
        this.Payload = {};
        this.NummerUmweltcheck = 0;
        this.Kunde = new _models_index__WEBPACK_IMPORTED_MODULE_3__["Kunde"]();
        this.Adressen = new _models_index__WEBPACK_IMPORTED_MODULE_3__["Adressen"]();
        this.Umweltchecks = new _models_umweltchecks__WEBPACK_IMPORTED_MODULE_4__["Umweltchecks"]();
        //initialisieren der Behaviorobjects => getValue gibt das Objekt
        this.KundenBhobj = new rxjs_Rx__WEBPACK_IMPORTED_MODULE_5__["BehaviorSubject"](this.Kunde);
        this.AdressenBhobj = new rxjs_Rx__WEBPACK_IMPORTED_MODULE_5__["BehaviorSubject"](this.Adressen);
        this.UmweltchecksBhobj = new rxjs_Rx__WEBPACK_IMPORTED_MODULE_5__["BehaviorSubject"](this.Umweltchecks);
        //initialisieren der Observables
        this.KundenOberservable = this.KundenBhobj.asObservable();
        this.AdressenOberservable = this.AdressenBhobj.asObservable();
        this.UmweltchecksBhobj = this.UmweltchecksBhobj.asObservable();
        //initialiseren der Header
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ "Access-Control-Allow-Origin": "*" });
        this.headers.append("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE");
        this.headers.append("Access-Control-Allow-Headers", "x-requested-with,content-type, origin, access-control-allow-origin, access-control-allow-headers");
        this.headers.append("Access-Control-Allow-Credentials", "true");
        this.headers.append("content-type", "text/plain; charset=utf-8");
        if (this.headersOn) {
            this.Payload = { headers: this.headers };
        }
        else {
            this.Payload = {};
        }
        this.BHKWvalues = new _models_index__WEBPACK_IMPORTED_MODULE_3__["BHKWvalues"]();
    }
    //Kunde:   
    DataService.prototype.getKunde = function () {
        return this.http.get("https://www.quickcheckumwelt.de/go/getKunde/");
    };
    DataService.prototype.updateKunde = function (Kunde) {
        this.Kunde = Kunde;
        this.KundenBhobj.next(Kunde);
        console.log("updateKunde: KundenBhobj.getValue():", this.KundenBhobj.getValue());
    };
    DataService.prototype.saveKunde = function () {
        var _this = this;
        var Kunde = this.KundenBhobj.getValue();
        console.log("saveKunde: KundenBhobj.getValue():", Kunde);
        this.http.post("https://www.quickcheckumwelt.de/go/saveKunde/", Kunde)
            .subscribe(function (response) { return console.log(response); }, function (error) {
            console.log("DSsaveKunde", error);
            if (error.status == 405 || 403 || 302) {
                _this.authGuardService.deleteCurrentUser();
            }
        });
    };
    //Adressen
    DataService.prototype.getAdressen = function () {
        return this.http.get("https://www.quickcheckumwelt.de/go/getAdressen/");
    };
    //lokales Update
    DataService.prototype.updateFirmenAdresse = function (Firmenadresse) {
        this.Adressen = this.AdressenBhobj.getValue();
        this.Adressen.Firmenadresse = Firmenadresse;
        this.AdressenBhobj.next(this.Adressen);
        console.log("updateAdressen: KundenBhobj.getValue():", this.AdressenBhobj.getValue());
    };
    DataService.prototype.updateRechnungsAdresse = function (Rechnungsadresse) {
        this.Adressen = this.AdressenBhobj.getValue();
        this.Adressen.Firmenadresse = Rechnungsadresse;
        this.AdressenBhobj.next(this.Adressen);
        console.log("updateAdressen: KundenBhobj.getValue():", this.AdressenBhobj.getValue());
    };
    //Serverupdate
    DataService.prototype.saveFirmenAdresse = function () {
        var _this = this;
        var Adressen = this.AdressenBhobj.getValue();
        //Firmenadresse wegschicken:
        return this.http.post("https://www.quickcheckumwelt.de/go/saveAdressen/", Adressen.Firmenadresse)
            .subscribe(function (response) {
            console.log(response);
        }, function (error) {
            console.log(error);
            console.log("DSsaveFirmenAdresse", error);
            if (error.status == 405 || 403 || 302) {
                _this.authGuardService.deleteCurrentUser();
            }
        });
    };
    DataService.prototype.saveRechnungsAdresse = function () {
        var _this = this;
        var Adressen = this.AdressenBhobj.getValue();
        //Rechnungsadresse wegschicken:
        //Frage: Speichert er die Rechnungsadresse -> testen
        this.http.post("https://www.quickcheckumwelt.de/go/saveAdressen/", Adressen.Rechnungsadresse)
            .subscribe(function (response) {
            console.log(response);
        }, function (error) {
            console.log("DSsaveKunde", error);
            if (error.status == 405 || 403 || 302) {
                _this.authGuardService.deleteCurrentUser();
            }
        });
    };
    DataService.prototype.deleteRechnungsAdresse = function () {
        var _this = this;
        if (this.Umweltchecks.Umweltchecks.length > 0) {
            this.http.delete("https://www.quickcheckumwelt.de/go/deleteRechnungsadresse/" + this.Umweltchecks.Umweltchecks[this.NummerUmweltcheck].umweltcheck_id + "/")
                .subscribe(function (response) {
                console.log("DSdeleteRechnungsadresse:", response);
            }, function (error) {
                console.log("DSdeleteRechnungsadresseError", error);
                if (error.status == 405 || 403 || 302) {
                    _this.authGuardService.deleteCurrentUser();
                }
            });
        }
    };
    DataService.prototype.getUmweltChecks = function () {
        return this.http.get("https://www.quickcheckumwelt.de/go/getUmweltchecks/");
    };
    DataService.prototype.updateUmweltchecks = function (Umweltchecks) {
        this.Umweltchecks = Umweltchecks;
        this.UmweltchecksBhobj.next(Umweltchecks);
    };
    DataService.prototype.saveUmweltChecks = function () {
        var _this = this;
        this.http.post("https://www.quickcheckumwelt.de/go/saveAdressen/", _models_umweltchecks__WEBPACK_IMPORTED_MODULE_4__["Umweltchecks"])
            .subscribe(function (response) {
            _this.Umweltchecks = response;
            console.log(_this.Umweltchecks);
        }, function (error) {
            console.log("DSsaveKunde", error);
            if (error.status == 405 || 403 || 302) {
                _this.authGuardService.deleteCurrentUser();
            }
        });
    };
    DataService.prototype.saveHotel = function () {
        var _this = this;
        this.http.post("https://www.quickcheckumwelt.de/go/saveHotel/" + this.Umweltchecks.Umweltchecks[this.NummerUmweltcheck].umweltcheck_id, this.Umweltchecks.Umweltchecks[this.NummerUmweltcheck].Einrichtung.Hotel)
            .subscribe(function (response) {
            console.log(response);
        }, function (error) {
            console.log("DSsaveHotel", error);
            if (error.status == 405 || 403 || 302) {
                _this.authGuardService.deleteCurrentUser();
            }
        });
    };
    DataService.prototype.getAnlagenAusAnlangenArray = function (typ) {
        var anlagen = [];
        if (this.Umweltchecks.Umweltchecks.length > 0) {
            for (var _i = 0, _a = this.Umweltchecks.Umweltchecks[this.NummerUmweltcheck].Einrichtung.Anlagen; _i < _a.length; _i++) {
                var anlage = _a[_i];
                if (anlage.typ.includes(typ)) {
                    //FALL: HEIZUNGEN:
                    //gehe das valuearray durch und füge den Namen des NameArrays als Namensposition:
                    var HeizungsNamen = new _models_index__WEBPACK_IMPORTED_MODULE_3__["Heizmittelnamen"]();
                    var Heizungsvalues = new _models_index__WEBPACK_IMPORTED_MODULE_3__["Heizmittelvalue"]();
                    var i = 0;
                    for (var _b = 0, _c = Heizungsvalues.Heizmittelvalues; _b < _c.length; _b++) {
                        var value = _c[_b];
                        //FALL HEIZUNG
                        if (anlage.typ.includes(value)) {
                            //console.log("HEIZ Anlagentyp:",anlage.typ,"Heizmittelvalues:",Heizungsvalues.Heizmittelvalues[i],"Heizmittelname:",HeizungsNamen.Heizmittelnamen[i])
                            anlage.name = value;
                        }
                        //FALL BHWK
                        if (anlage.typ.includes(this.BHKWvalues.BHKWvalues[i])) {
                            //console.log("HEIZ Anlagentyp:",anlage.typ,"Heizmittelvalues:",Heizungsvalues.Heizmittelvalues[i],"Heizmittelname:",HeizungsNamen.Heizmittelnamen[i])
                            anlage.name = this.BHKWvalues.BHKWvalues[i];
                        }
                        //alle: Einheiten
                        if (anlage.typ.includes("kWh")) {
                            //console.log("HEIZ Anlagentyp:",anlage.typ,"Heizmittelvalues:",Heizungsvalues.Heizmittelvalues[i],"Heizmittelname:",HeizungsNamen.Heizmittelnamen[i])
                            anlage.einheit = "kWh";
                        }
                        if (anlage.typ.includes("kg")) {
                            //console.log("HEIZ Anlagentyp:",anlage.typ,"Heizmittelvalues:",Heizungsvalues.Heizmittelvalues[i],"Heizmittelname:",HeizungsNamen.Heizmittelnamen[i])
                            anlage.einheit = "kg";
                        }
                        if (anlage.typ.includes("Liter")) {
                            //console.log("HEIZ Anlagentyp:",anlage.typ,"Heizmittelvalues:",Heizungsvalues.Heizmittelvalues[i],"Heizmittelname:",HeizungsNamen.Heizmittelnamen[i])
                            anlage.einheit = "Liter";
                        }
                        if (anlage.anlagen_id == 19) {
                            console.log("jetzt");
                        }
                        i++;
                    }
                    anlagen.push(anlage);
                }
            }
        }
        return anlagen;
    };
    //Anlagen:
    //Teständerung
    DataService.prototype.addAnlage = function (anlage) {
        this.Umweltchecks.Umweltchecks[this.NummerUmweltcheck].Einrichtung.Anlagen.push(anlage);
        this.UmweltchecksBhobj.next(this.Umweltchecks);
    };
    DataService.prototype.addAnlagen = function (anlagen) {
        this.Umweltchecks.Umweltchecks[this.NummerUmweltcheck].Einrichtung.Anlagen = anlagen.concat(anlagen, this.Umweltchecks.Umweltchecks[this.NummerUmweltcheck].Einrichtung.Anlagen);
        this.UmweltchecksBhobj.next(this.Umweltchecks);
    };
    DataService.prototype.deleteAnlagen = function () {
        var _this = this;
        if (this.Umweltchecks.Umweltchecks.length > 0) {
            var length = this.Umweltchecks.Umweltchecks[this.NummerUmweltcheck].Einrichtung.Anlagen.length;
            this.Umweltchecks.Umweltchecks[this.NummerUmweltcheck].Einrichtung.Anlagen.forEach(function (item, index) {
                delete _this.Umweltchecks.Umweltchecks[_this.NummerUmweltcheck].Einrichtung.Anlagen[index];
            });
            //Einträge löschen
            for (var _i = 0, _a = this.Umweltchecks.Umweltchecks[this.NummerUmweltcheck].Einrichtung.Anlagen; _i < _a.length; _i++) {
                var a = _a[_i];
                if (length > 0) {
                    this.Umweltchecks.Umweltchecks[this.NummerUmweltcheck].Einrichtung.Anlagen.shift();
                    length = length - 1;
                }
            }
        }
        this.UmweltchecksBhobj.next(this.Umweltchecks);
    };
    //Restabfalltonn
    DataService.prototype.addRestabfalltonne = function (tonne) {
        this.Umweltchecks.Umweltchecks[this.NummerUmweltcheck].Einrichtung.Restabfalltonnen.push(tonne);
        this.UmweltchecksBhobj.next(this.Umweltchecks);
    };
    DataService.prototype.addRestabfalltonnen = function (tonnen) {
        this.Umweltchecks.Umweltchecks[this.NummerUmweltcheck].Einrichtung.Restabfalltonnen = tonnen.concat(tonnen, this.Umweltchecks.Umweltchecks[this.NummerUmweltcheck].Einrichtung.Restabfalltonnen);
        this.UmweltchecksBhobj.next(this.Umweltchecks);
    };
    DataService.prototype.deleteRestabfalltonnenInDS = function () {
        var _this = this;
        if (this.Umweltchecks.Umweltchecks.length > 0) {
            var length = this.Umweltchecks.Umweltchecks[this.NummerUmweltcheck].Einrichtung.Restabfalltonnen.length;
            this.Umweltchecks.Umweltchecks[this.NummerUmweltcheck].Einrichtung.Restabfalltonnen.forEach(function (item, index) {
                delete _this.Umweltchecks.Umweltchecks[_this.NummerUmweltcheck].Einrichtung.Restabfalltonnen[index];
            });
            //die ersten Elemente rausslöschen, gingt nich auch in der foreach
            for (var _i = 0, _a = this.Umweltchecks.Umweltchecks[this.NummerUmweltcheck].Einrichtung.Restabfalltonnen; _i < _a.length; _i++) {
                var r = _a[_i];
                if (length >= -1) {
                    this.Umweltchecks.Umweltchecks[this.NummerUmweltcheck].Einrichtung.Restabfalltonnen.shift();
                    length = length - 1;
                }
            }
            this.Umweltchecks.Umweltchecks[this.NummerUmweltcheck].Einrichtung.Restabfalltonnen.shift();
        }
        this.UmweltchecksBhobj.next(this.Umweltchecks);
        console.log("Dataservice: nach Delte Restabfalltonnen", this.Umweltchecks);
    };
    //Getbegleitfrage
    DataService.prototype.getbegleitfrageAusbegleitfrageArray = function (typ) {
        var begleitfrage = [];
        if (this.Umweltchecks.Umweltchecks.length > 0) {
            for (var _i = 0, _a = this.Umweltchecks.Umweltchecks[this.NummerUmweltcheck].Einrichtung.begleitfragen; _i < _a.length; _i++) {
                var begleitdatum = _a[_i];
                if (begleitdatum.datums_typ === typ) {
                    begleitfrage.push(begleitdatum);
                }
            }
        }
        return begleitfrage;
    };
    DataService.prototype.addbegleitfrage = function (begleitdatum) {
        this.Umweltchecks.Umweltchecks[this.NummerUmweltcheck].Einrichtung.begleitfragen.push(begleitdatum);
        this.UmweltchecksBhobj.next(this.Umweltchecks);
    };
    DataService.prototype.deletebegleitfrage = function () {
        var _this = this;
        if (this.Umweltchecks.Umweltchecks.length > 0) {
            var length = this.Umweltchecks.Umweltchecks[this.NummerUmweltcheck].Einrichtung.begleitfragen.length;
            this.Umweltchecks.Umweltchecks[this.NummerUmweltcheck].Einrichtung.begleitfragen.forEach(function (item, index) {
                delete _this.Umweltchecks.Umweltchecks[_this.NummerUmweltcheck].Einrichtung.begleitfragen[index];
            });
            //Einträge löschen
            for (var _i = 0, _a = this.Umweltchecks.Umweltchecks[this.NummerUmweltcheck].Einrichtung.begleitfragen; _i < _a.length; _i++) {
                var a = _a[_i];
                if (length > 0) {
                    this.Umweltchecks.Umweltchecks[this.NummerUmweltcheck].Einrichtung.begleitfragen.shift();
                    length = length - 1;
                }
            }
        }
        this.UmweltchecksBhobj.next(this.Umweltchecks);
    };
    DataService.prototype.saveRestabfalltonnen = function () {
        var _this = this;
        this.http.post("https://www.quickcheckumwelt.de/go/saveRestabfalltonnen/" + this.Umweltchecks.Umweltchecks[this.NummerUmweltcheck].umweltcheck_id + "/", this.Umweltchecks.Umweltchecks[this.NummerUmweltcheck].Einrichtung.Restabfalltonnen)
            .subscribe(function (response) {
            console.log("DSsaveRestabfalltonnenResponse:", response);
        }, function (error) {
            console.log("DSsaveRestabfalltonnenError", error);
            if (error.status == 405 || 403 || 302) {
                _this.authGuardService.deleteCurrentUser();
            }
        });
    };
    DataService.prototype.deleteRestabfalltonnen = function () {
        var _this = this;
        if (this.Umweltchecks.Umweltchecks.length > 0) {
            this.http.delete("https://www.quickcheckumwelt.de/go/deleteRestabfalltonnen/" + this.Umweltchecks.Umweltchecks[this.NummerUmweltcheck].umweltcheck_id + "/")
                .subscribe(function (response) {
                console.log("DSdeleteRestabfalltonnenResponse:", response);
            }, function (error) {
                console.log("DSdeleteRestabfalltonnenError", error);
                if (error.status == 405 || 403 || 302) {
                    _this.authGuardService.deleteCurrentUser();
                }
            });
        }
    };
    DataService.prototype.saveAnlagen = function () {
        var _this = this;
        this.http.post("https://www.quickcheckumwelt.de/go/saveAnlagen/" + this.Umweltchecks.Umweltchecks[this.NummerUmweltcheck].umweltcheck_id + "/", this.Umweltchecks.Umweltchecks[this.NummerUmweltcheck].Einrichtung.Anlagen)
            .subscribe(function (response) {
            console.log("DSsaveAnlagenResponse:", response);
        }, function (error) {
            console.log("DSsaveAnlagenError", error);
            if (error.status == 405 || 403 || 302) {
                _this.authGuardService.deleteCurrentUser();
            }
        });
    };
    DataService.prototype.saveEinrichtung = function () {
        var _this = this;
        this.http.post("https://www.quickcheckumwelt.de/go/saveEinrichtung/" + this.Umweltchecks.Umweltchecks[this.NummerUmweltcheck].umweltcheck_id + "/", this.Umweltchecks.Umweltchecks[this.NummerUmweltcheck].Einrichtung)
            .subscribe(function (response) {
            console.log("DSsaveEinrichtungResponse:", response);
        }, function (error) {
            console.log("DSEinrichtungError", error);
            if (error.status == 405 || 403 || 302) {
                _this.authGuardService.deleteCurrentUser();
            }
        });
    };
    DataService.prototype.saveFeWo = function () {
        var _this = this;
        this.http.post("https://www.quickcheckumwelt.de/go/saveFeWo/" + this.Umweltchecks.Umweltchecks[this.NummerUmweltcheck].umweltcheck_id + "/", this.Umweltchecks.Umweltchecks[this.NummerUmweltcheck].Einrichtung.FeWo)
            .subscribe(function (response) {
            console.log("DSsaveFeWoResponse:", response);
        }, function (error) {
            console.log("DSsaveFeWoError", error);
            if (error.status == 405 || 403 || 302) {
                _this.authGuardService.deleteCurrentUser();
            }
        });
    };
    DataService.prototype.deleteFeWo = function (FeWoID) {
        var _this = this;
        if (this.Umweltchecks.Umweltchecks.length > 0) {
            this.http.delete("https://www.quickcheckumwelt.de/go/deleteFeWo/" + this.Umweltchecks.Umweltchecks[this.NummerUmweltcheck].umweltcheck_id + "/")
                .subscribe(function (response) {
                console.log("DSdeleteFeWoResponse:", response);
            }, function (error) {
                console.log("DSdeleteFeWoError", error);
                if (error.status == 405 || 403 || 302) {
                    _this.authGuardService.deleteCurrentUser();
                }
            });
        }
    };
    DataService.prototype.saveWellnessbereich = function () {
        var _this = this;
        this.http.post("https://www.quickcheckumwelt.de/go/saveWellnessbereich/" + this.Umweltchecks.Umweltchecks[this.NummerUmweltcheck].umweltcheck_id + "/", this.Umweltchecks.Umweltchecks[this.NummerUmweltcheck].Einrichtung.Wellnessbereich)
            .subscribe(function (response) {
            console.log("DSsaveWellnessbereichResponse:", response);
        }, function (error) {
            console.log("DSsaveWellnessbereichError", error);
            if (error.status == 405 || 403 || 302) {
                _this.authGuardService.deleteCurrentUser();
            }
        });
    };
    DataService.prototype.deleteWellnessbereich = function () {
        var _this = this;
        if (this.Umweltchecks.Umweltchecks.length > 0) {
            this.http.delete("https://www.quickcheckumwelt.de/go/deleteWellnessbereich/" + this.Umweltchecks.Umweltchecks[this.NummerUmweltcheck].umweltcheck_id + "/")
                .subscribe(function (response) {
                console.log("DSdeleteWellnessbereichResponse:", response);
            }, function (error) {
                console.log("DSdeleteWellnessbereichError", error);
                if (error.status == 405 || 403 || 302) {
                    _this.authGuardService.deleteCurrentUser();
                }
            });
        }
    };
    DataService.prototype.deleteHotel = function () {
        var _this = this;
        if (this.Umweltchecks.Umweltchecks.length > 0) {
            this.http.delete("https://www.quickcheckumwelt.de/go/deleteHotel/" + this.Umweltchecks.Umweltchecks[this.NummerUmweltcheck].umweltcheck_id + "/")
                .subscribe(function (response) {
                console.log("DSdeleteHotelResponse:", response);
            }, function (error) {
                console.log("DSdeleteHotelError", error);
                if (error.status == 405 || 403 || 302) {
                    _this.authGuardService.deleteCurrentUser();
                }
            });
        }
    };
    DataService.prototype.savebegleitfrage = function () {
        var _this = this;
        this.http.post("https://www.quickcheckumwelt.de/go/savebegleitfrage/" + this.Umweltchecks.Umweltchecks[this.NummerUmweltcheck].umweltcheck_id + "/", this.Umweltchecks.Umweltchecks[this.NummerUmweltcheck].Einrichtung.begleitfragen)
            .subscribe(function (response) {
            console.log("DSsavebegleitfrageResponse:", response);
        }, function (error) {
            console.log("DSsavebegleitfrageError", error);
            if (error.status == 405 || 403 || 302) {
                _this.authGuardService.deleteCurrentUser();
            }
        });
    };
    DataService.prototype.abgabeQuickcheckUmwelt = function () {
        var _this = this;
        this.http.post("https://www.quickcheckumwelt.de/go/abgabeQuickcheckUmwelt/" + this.Umweltchecks.Umweltchecks[this.NummerUmweltcheck].umweltcheck_id + "/", "{\"response\":\"abgabeQuickcheckUmwelt\" ")
            .subscribe(function (response) {
            console.log("DSabgabeQuickcheckUmwelt", _this.Umweltchecks.Umweltchecks[_this.NummerUmweltcheck]);
            console.log("DSabgabeQuickcheckUmweltResponse:", response);
        }, function (error) {
            console.log("DSabgabeQuickcheckUmwelt", _this.Umweltchecks.Umweltchecks[_this.NummerUmweltcheck]);
            console.log("DSabgabeQuickcheckUmweltError", error);
            if (error.status == 405 || 403 || 302) {
                _this.authGuardService.deleteCurrentUser();
            }
        });
    };
    DataService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: "root",
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _guards_index__WEBPACK_IMPORTED_MODULE_6__["AuthGuard"]])
    ], DataService);
    return DataService;
}());



/***/ }),

/***/ "./src/app/_services/index.ts":
/*!************************************!*\
  !*** ./src/app/_services/index.ts ***!
  \************************************/
/*! exports provided: AlertService, AuthenticationService, UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _alert_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./alert.service */ "./src/app/_services/alert.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AlertService", function() { return _alert_service__WEBPACK_IMPORTED_MODULE_0__["AlertService"]; });

/* harmony import */ var _authentication_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./authentication.service */ "./src/app/_services/authentication.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AuthenticationService", function() { return _authentication_service__WEBPACK_IMPORTED_MODULE_1__["AuthenticationService"]; });

/* harmony import */ var _user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./user.service */ "./src/app/_services/user.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return _user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"]; });






/***/ }),

/***/ "./src/app/_services/pruefservice.service.ts":
/*!***************************************************!*\
  !*** ./src/app/_services/pruefservice.service.ts ***!
  \***************************************************/
/*! exports provided: PruefService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PruefService", function() { return PruefService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_grouppruefarray__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../_models/grouppruefarray */ "./src/app/_models/grouppruefarray.ts");
/* harmony import */ var rxjs_Rx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/Rx */ "./node_modules/rxjs-compat/_esm5/Rx.js");




var PruefService = /** @class */ (function () {
    function PruefService() {
        this.NameArray = [];
        this.ErrorArray = [];
        this.InvalidFormGroups = new Array();
        this.InvalidFormGroupBhSubj = new rxjs_Rx__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"](this.InvalidFormGroups);
        this.InvalidFormGroupOberservable = this.InvalidFormGroupBhSubj.asObservable();
    }
    PruefService.prototype.ResetPruefungService = function () {
        this.InvalidFormGroups = [];
        this.InvalidFormGroups.length = 0;
        this.InvalidFormGroupBhSubj.next(this.InvalidFormGroups);
    };
    PruefService.prototype.ResetNamensAndErrors = function () {
        //this.ErrorArray.length=0
        this.ErrorArray = [];
        //this.NameArray.length=0
        this.NameArray = [];
    };
    PruefService.prototype.pushNameErrorArray = function (name, error) {
        this.ErrorArray.push(error);
        this.NameArray.push(name);
    };
    PruefService.prototype.pushInvalidFormgroup = function (name, FormGroup) {
        var Group = new _models_grouppruefarray__WEBPACK_IMPORTED_MODULE_2__["GroupPruefArray"]();
        Group.name = name;
        Group.FormGroup = FormGroup;
        Group.FormControlNames = this.NameArray;
        Group.FormControlErrors = this.ErrorArray;
        //pruefung, ob das Array leer => Keine Fehler 
        if (Group.FormControlNames.length > 0) {
            this.InvalidFormGroups.push(Group);
            this.InvalidFormGroupBhSubj.next(this.InvalidFormGroups);
        }
        console.log("pruefservice: InvalidFormGroups", this.InvalidFormGroups);
        this.ResetNamensAndErrors();
    };
    PruefService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], PruefService);
    return PruefService;
}());



/***/ }),

/***/ "./src/app/_services/user.service.ts":
/*!*******************************************!*\
  !*** ./src/app/_services/user.service.ts ***!
  \*******************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _models_index__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../_models/index */ "./src/app/_models/index.ts");




var UserService = /** @class */ (function () {
    function UserService(http) {
        this.http = http;
    }
    UserService.prototype.create = function (user) {
        var kunde = new _models_index__WEBPACK_IMPORTED_MODULE_3__["Kunde"]();
        kunde.ansprechpartner = user.firstName + " " + user.lastName;
        kunde.username = user.username;
        kunde.passwort_hash = user.password;
        console.log("Regiser User: ", kunde);
        return this.http.post('/go/register/', kunde);
    };
    UserService.prototype.orderNewPassword = function (user) {
        return this.http.post('/go/orderNewPassword/', user);
    };
    UserService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], UserService);
    return UserService;
}());



/***/ }),

/***/ "./src/app/abgabe/abgabe.component.css":
/*!*********************************************!*\
  !*** ./src/app/abgabe/abgabe.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FiZ2FiZS9hYmdhYmUuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/abgabe/abgabe.component.html":
/*!**********************************************!*\
  !*** ./src/app/abgabe/abgabe.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  Sie haben die Quickcheck Umwelt erfolgreich abgegeben und werden in wenigen Tagen über Ihre Benchmark informiert!\n</p>\n"

/***/ }),

/***/ "./src/app/abgabe/abgabe.component.ts":
/*!********************************************!*\
  !*** ./src/app/abgabe/abgabe.component.ts ***!
  \********************************************/
/*! exports provided: AbgabeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AbgabeComponent", function() { return AbgabeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../_services/data.service */ "./src/app/_services/data.service.ts");



var AbgabeComponent = /** @class */ (function () {
    function AbgabeComponent(Dataservice) {
        this.Dataservice = Dataservice;
    }
    AbgabeComponent.prototype.ngOnInit = function () {
        var _this = this;
        //Listen to saveBetriebsdaten-Event vom Parentcomponent: eingabe.component => SAVE local Data!
        this.abgabeQuickcheckUmwelt.subscribe(function () {
            //lokal: schreibe die Werte in die Tonnen, dann in den Dataservice -
            _this.Dataservice.abgabeQuickcheckUmwelt();
        }, function (err) { console.log(err); }, function (complete) { console.log("abgabeQuickcheckUmwelt vom Frontent versendet"); });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], AbgabeComponent.prototype, "abgabeQuickcheckUmwelt", void 0);
    AbgabeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-abgabe',
            template: __webpack_require__(/*! ./abgabe.component.html */ "./src/app/abgabe/abgabe.component.html"),
            styles: [__webpack_require__(/*! ./abgabe.component.css */ "./src/app/abgabe/abgabe.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_data_service__WEBPACK_IMPORTED_MODULE_2__["DataService"]])
    ], AbgabeComponent);
    return AbgabeComponent;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-navigation></app-navigation>    \r\n  \r\n   <div class=\"container\" style=\"padding-top:80px;\">\r\n        <div class=\"col-sm-12 col-lg-offset-0.75\">\r\n            <alert></alert>\r\n            <router-outlet></router-outlet>\r\n        </div>\r\n    </div>\r\n    <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.2.0/css/all.css\" integrity=\"sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ\" crossorigin=\"anonymous\">"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./_services/data.service */ "./src/app/_services/data.service.ts");
/* harmony import */ var _assets_app_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../assets/app.css */ "./src/assets/app.css");
/* harmony import */ var _assets_app_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_assets_app_css__WEBPACK_IMPORTED_MODULE_3__);




var AppComponent = /** @class */ (function () {
    function AppComponent(Dataservice) {
        this.Dataservice = Dataservice;
        this.Dataservice.getKunde();
    }
    AppComponent.prototype.ngOnInit = function () {
    };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_data_service__WEBPACK_IMPORTED_MODULE_2__["DataService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _helpers_index__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./_helpers/index */ "./src/app/_helpers/index.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _app_routing__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app.routing */ "./src/app/app.routing.ts");
/* harmony import */ var _directives_index__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./_directives/index */ "./src/app/_directives/index.ts");
/* harmony import */ var _guards_index__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./_guards/index */ "./src/app/_guards/index.ts");
/* harmony import */ var _services_index__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./_services/index */ "./src/app/_services/index.ts");
/* harmony import */ var _home_index__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./home/index */ "./src/app/home/index.ts");
/* harmony import */ var _login_index__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./login/index */ "./src/app/login/index.ts");
/* harmony import */ var _register_index__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./register/index */ "./src/app/register/index.ts");
/* harmony import */ var _landingpage_landingpage_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./landingpage/landingpage.component */ "./src/app/landingpage/landingpage.component.ts");
/* harmony import */ var angular_font_awesome__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! angular-font-awesome */ "./node_modules/angular-font-awesome/dist/angular-font-awesome.es5.js");
/* harmony import */ var _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @fortawesome/angular-fontawesome */ "./node_modules/@fortawesome/angular-fontawesome/fesm5/angular-fontawesome.js");
/* harmony import */ var _navigation_navigation_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./navigation/navigation.component */ "./src/app/navigation/navigation.component.ts");
/* harmony import */ var _eingabe_eingabe_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./eingabe/eingabe.component */ "./src/app/eingabe/eingabe.component.ts");
/* harmony import */ var _footer_footer_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./footer/footer.component */ "./src/app/footer/footer.component.ts");
/* harmony import */ var _impressum_impressum_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./impressum/impressum.component */ "./src/app/impressum/impressum.component.ts");
/* harmony import */ var _datenschutz_datenschutz_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./datenschutz/datenschutz.component */ "./src/app/datenschutz/datenschutz.component.ts");
/* harmony import */ var _name_editor_name_editor_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./name-editor/name-editor.component */ "./src/app/name-editor/name-editor.component.ts");
/* harmony import */ var _kundeneingabe_kundeneingabe_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./kundeneingabe/kundeneingabe.component */ "./src/app/kundeneingabe/kundeneingabe.component.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! @angular/material/input */ "./node_modules/@angular/material/esm5/input.es5.js");
/* harmony import */ var _test_test_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./test/test.component */ "./src/app/test/test.component.ts");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm5/icon.es5.js");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/esm5/select.es5.js");
/* harmony import */ var _angular_material_core__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! @angular/material/core */ "./node_modules/@angular/material/esm5/core.es5.js");
/* harmony import */ var _betriebsdaten_betriebsdaten_component__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./betriebsdaten/betriebsdaten.component */ "./src/app/betriebsdaten/betriebsdaten.component.ts");
/* harmony import */ var _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! @angular/material/slide-toggle */ "./node_modules/@angular/material/esm5/slide-toggle.es5.js");
/* harmony import */ var _strom_heizung_strom_heizung_component__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./strom-heizung/strom-heizung.component */ "./src/app/strom-heizung/strom-heizung.component.ts");
/* harmony import */ var _restabfall_restabfall_component__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./restabfall/restabfall.component */ "./src/app/restabfall/restabfall.component.ts");
/* harmony import */ var _angular_material_stepper__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! @angular/material/stepper */ "./node_modules/@angular/material/esm5/stepper.es5.js");
/* harmony import */ var _services_data_service__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ./_services/data.service */ "./src/app/_services/data.service.ts");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/index.js");
/* harmony import */ var _abgabe_abgabe_component__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! ./abgabe/abgabe.component */ "./src/app/abgabe/abgabe.component.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _pruefung_pruefung_component__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! ./pruefung/pruefung.component */ "./src/app/pruefung/pruefung.component.ts");
/* harmony import */ var _intro_intro_component__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! ./intro/intro.component */ "./src/app/intro/intro.component.ts");
/* harmony import */ var _wasser_wasser_component__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! ./wasser/wasser.component */ "./src/app/wasser/wasser.component.ts");
/* harmony import */ var _strom_strom_component__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! ./strom/strom.component */ "./src/app/strom/strom.component.ts");





// used to create fake backend









































var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
                _app_routing__WEBPACK_IMPORTED_MODULE_7__["routing"],
                angular_font_awesome__WEBPACK_IMPORTED_MODULE_15__["AngularFontAwesomeModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_24__["BrowserAnimationsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_25__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_25__["MatCheckboxModule"],
                _angular_material_input__WEBPACK_IMPORTED_MODULE_26__["MatInputModule"],
                _angular_material_icon__WEBPACK_IMPORTED_MODULE_28__["MatIconModule"],
                _angular_material_select__WEBPACK_IMPORTED_MODULE_29__["MatSelectModule"],
                _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_32__["MatSlideToggleModule"],
                _angular_material_stepper__WEBPACK_IMPORTED_MODULE_35__["MatStepperModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_25__["MatRadioModule"],
                _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_16__["FontAwesomeModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_39__["NgbModule"]
                //ErrorStateMatcher,
            ],
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"],
                _directives_index__WEBPACK_IMPORTED_MODULE_8__["AlertComponent"],
                _home_index__WEBPACK_IMPORTED_MODULE_11__["HomeComponent"],
                _login_index__WEBPACK_IMPORTED_MODULE_12__["LoginComponent"],
                _register_index__WEBPACK_IMPORTED_MODULE_13__["RegisterComponent"],
                _landingpage_landingpage_component__WEBPACK_IMPORTED_MODULE_14__["LandingpageComponent"],
                _navigation_navigation_component__WEBPACK_IMPORTED_MODULE_17__["NavigationComponent"],
                _eingabe_eingabe_component__WEBPACK_IMPORTED_MODULE_18__["EingabeComponent"],
                _footer_footer_component__WEBPACK_IMPORTED_MODULE_19__["FooterComponent"],
                _impressum_impressum_component__WEBPACK_IMPORTED_MODULE_20__["ImpressumComponent"],
                _datenschutz_datenschutz_component__WEBPACK_IMPORTED_MODULE_21__["DatenschutzComponent"],
                _name_editor_name_editor_component__WEBPACK_IMPORTED_MODULE_22__["NameEditorComponent"],
                _kundeneingabe_kundeneingabe_component__WEBPACK_IMPORTED_MODULE_23__["KundeneingabeComponent"],
                _test_test_component__WEBPACK_IMPORTED_MODULE_27__["TestComponent"],
                _betriebsdaten_betriebsdaten_component__WEBPACK_IMPORTED_MODULE_31__["BetriebsdatenComponent"],
                _strom_heizung_strom_heizung_component__WEBPACK_IMPORTED_MODULE_33__["StromHeizungComponent"],
                _restabfall_restabfall_component__WEBPACK_IMPORTED_MODULE_34__["RestabfallComponent"],
                _abgabe_abgabe_component__WEBPACK_IMPORTED_MODULE_38__["AbgabeComponent"],
                _pruefung_pruefung_component__WEBPACK_IMPORTED_MODULE_40__["PruefungComponent"],
                _intro_intro_component__WEBPACK_IMPORTED_MODULE_41__["IntroComponent"],
                _wasser_wasser_component__WEBPACK_IMPORTED_MODULE_42__["WasserComponent"],
                _strom_strom_component__WEBPACK_IMPORTED_MODULE_43__["StromComponent"]
            ],
            providers: [
                _guards_index__WEBPACK_IMPORTED_MODULE_9__["AuthGuard"],
                _services_index__WEBPACK_IMPORTED_MODULE_10__["AlertService"],
                _services_index__WEBPACK_IMPORTED_MODULE_10__["AuthenticationService"],
                _angular_material_core__WEBPACK_IMPORTED_MODULE_30__["ErrorStateMatcher"],
                _services_index__WEBPACK_IMPORTED_MODULE_10__["UserService"],
                _services_data_service__WEBPACK_IMPORTED_MODULE_36__["DataService"],
                ngx_cookie_service__WEBPACK_IMPORTED_MODULE_37__["CookieService"],
                {
                    provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HTTP_INTERCEPTORS"],
                    useClass: _helpers_index__WEBPACK_IMPORTED_MODULE_5__["JwtInterceptor"],
                    multi: true
                },
                // provider used to create fake backend
                _helpers_index__WEBPACK_IMPORTED_MODULE_5__["fakeBackendProvider"]
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/app.routing.ts":
/*!********************************!*\
  !*** ./src/app/app.routing.ts ***!
  \********************************/
/*! exports provided: routing */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routing", function() { return routing; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _home_index__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./home/index */ "./src/app/home/index.ts");
/* harmony import */ var _login_index__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./login/index */ "./src/app/login/index.ts");
/* harmony import */ var _register_index__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./register/index */ "./src/app/register/index.ts");
/* harmony import */ var _guards_index__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./_guards/index */ "./src/app/_guards/index.ts");
/* harmony import */ var _landingpage_landingpage_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./landingpage/landingpage.component */ "./src/app/landingpage/landingpage.component.ts");
/* harmony import */ var _eingabe_eingabe_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./eingabe/eingabe.component */ "./src/app/eingabe/eingabe.component.ts");
/* harmony import */ var _impressum_impressum_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./impressum/impressum.component */ "./src/app/impressum/impressum.component.ts");
/* harmony import */ var _datenschutz_datenschutz_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./datenschutz/datenschutz.component */ "./src/app/datenschutz/datenschutz.component.ts");









var appRoutes = [
    { path: '', component: _landingpage_landingpage_component__WEBPACK_IMPORTED_MODULE_5__["LandingpageComponent"] },
    { path: 'home', component: _home_index__WEBPACK_IMPORTED_MODULE_1__["HomeComponent"], canActivate: [_guards_index__WEBPACK_IMPORTED_MODULE_4__["AuthGuard"]] },
    { path: 'login', component: _login_index__WEBPACK_IMPORTED_MODULE_2__["LoginComponent"] },
    { path: 'logout', component: _login_index__WEBPACK_IMPORTED_MODULE_2__["LoginComponent"] },
    { path: 'register', component: _register_index__WEBPACK_IMPORTED_MODULE_3__["RegisterComponent"] },
    { path: 'eingabe', component: _eingabe_eingabe_component__WEBPACK_IMPORTED_MODULE_6__["EingabeComponent"], canActivate: [_guards_index__WEBPACK_IMPORTED_MODULE_4__["AuthGuard"]] },
    { path: 'impressum', component: _impressum_impressum_component__WEBPACK_IMPORTED_MODULE_7__["ImpressumComponent"] },
    { path: 'datenschutz', component: _datenschutz_datenschutz_component__WEBPACK_IMPORTED_MODULE_8__["DatenschutzComponent"] },
    { path: 'redirectToLogin', component: _login_index__WEBPACK_IMPORTED_MODULE_2__["LoginComponent"] },
    // otherwise redirect to home
    { path: '**', redirectTo: '/login' }
];
var routing = _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forRoot(appRoutes);


/***/ }),

/***/ "./src/app/betriebsdaten/betriebsdaten.component.css":
/*!***********************************************************!*\
  !*** ./src/app/betriebsdaten/betriebsdaten.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2JldHJpZWJzZGF0ZW4vYmV0cmllYnNkYXRlbi5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/betriebsdaten/betriebsdaten.component.html":
/*!************************************************************!*\
  !*** ./src/app/betriebsdaten/betriebsdaten.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\"> \r\n    <div class=\"row\">        \r\n            <div class=\"col-md-12 col-sm-12\">\r\n                    <form class=\"form card p-2\">\r\n                        <h6 class=\"h6\">Welche Art von Einrichtung betreiben Sie? Hotel/s oder Ferienwohnung/en?</h6>                        \r\n                        <mat-form-field class=\"full-width\">\r\n                                <mat-select [(value)]=\"selected\" placeholder=\"Bitte wählen Sie aus der Liste\">\r\n                                    <mat-option value=\"Hotels\">Hotel</mat-option>\r\n                                    <mat-option value=\"Ferienwohnungen\">Ferienwohnung/en | Ferienhaus</mat-option>\r\n                                </mat-select>\r\n                        </mat-form-field>\r\n                    </form>\r\n                    <form class=\"form\" [formGroup]=\"FeWoFormGroup\">\r\n                         <div class=\"card p-2\" *ngIf=\"selected=='Ferienwohnungen'\"><!--wenn FeWo ausgewählt-->\r\n                            <h6>Wie viele Gästeanreisen haben Sie im Jahr?</h6>\r\n                            <h6 *ngIf=\"selected=='Ferienwohnungen'\" class=\"fontweightnormal\">Beispiel: Familie Müller (3 Personen) besucht Ihre Ferienwohnung im Frühjahr und Herbst für je eine Woche = 6 Gästeanreisen</h6>\r\n                            <mat-form-field class=\"full-width\">\r\n                                        <input [errorStateMatcher]=\"matcher\" matInput type=\"text\" pattern=\"\\d+\" min=\"0\" minlength=\"1\" required placeholder=\"Anzahl der Gästeanreisen pro Jahr\" formControlName=\"anzahl_der_gaesteanreisen_pro_jahr\" [formControl]=\"FeWoFormGroup.controls.anzahl_der_gaesteanreisen_pro_jahr\">            \r\n                                        <mat-icon *ngIf=\"FeWoFormGroup.controls.anzahl_der_gaesteanreisen_pro_jahr.valid\" class=\"success-icon\" matSuffix >done</mat-icon>                                \r\n                            </mat-form-field> \r\n                            <h6>Wie viele Bettenübernachtungen haben Sie von Mai bis Oktober?</h6>\r\n                            <mat-form-field class=\"full-width\">\r\n                                        <input [errorStateMatcher]=\"matcher\" matInput type=\"text\" pattern=\"\\d+\" min=\"0\" minlength=\"1\" required placeholder=\"Übernachtungen von Mai bis Oktober\" formControlName=\"anzahl_der_uebernachtungen_mai_bis_oktober\" [formControl]=\"FeWoFormGroup.controls.anzahl_der_uebernachtungen_mai_bis_oktober\">                                        \r\n                                        <mat-icon *ngIf=\"FeWoFormGroup.controls.anzahl_der_uebernachtungen_mai_bis_oktober.valid\" class=\"success-icon\" matSuffix >done</mat-icon>                                \r\n                            </mat-form-field>    \r\n                            <h6>Wie viele Bettenübernachtungen haben Sie von November bis April?</h6>\r\n                            <mat-form-field class=\"full-width\">\r\n                                        <input [errorStateMatcher]=\"matcher\" matInput type=\"text\" pattern=\"\\d+\" min=\"0\" minlength=\"1\" required placeholder=\"Übernachtungen von November bis April\" formControlName=\"anzahl_der_uebernachtungen_november_bis_april\" [formControl]=\"FeWoFormGroup.controls.anzahl_der_uebernachtungen_november_bis_april\">\r\n                                        \r\n                                        <mat-icon *ngIf=\"FeWoFormGroup.controls.anzahl_der_uebernachtungen_november_bis_april.valid\" class=\"success-icon\" matSuffix >done</mat-icon>\r\n                            </mat-form-field>\r\n                            <h6>Wie groß ist durchschnittlich eine Ferienwohnung?</h6>\r\n                            <h6 *ngIf=\"selected=='Ferienwohnungen'\" class=\"fontweightnormal\">\r\n                                Beispiel: <br>\r\n                                Ferienwohnung 1: 50 m² <br>\r\n                                Ferienwohnung 2: 60 m² <br>\r\n                                Ferienwohnung 3: 70 m² <br>\r\n                                => Durchschnittliche Größe einer Ferienwohnung: (50 m²+ 60 m² + 70 m²)/3 = 60 m²\r\n                            </h6>\r\n                            <mat-form-field class=\"full-width\">\r\n                                    <input [errorStateMatcher]=\"matcher\" matInput type=\"number\" min=\"0\" minlength=\"1\" required placeholder=\"durchschnittliche Größe einer Ferienwohnung\" formControlName=\"durchschnittliche_groesse_der_fe_wo_in_m2\" [formControl]=\"FeWoFormGroup.controls.durchschnittliche_groesse_der_fe_wo_in_m2\">\r\n                                    <span matSuffix>m³</span>\r\n                                    <mat-icon *ngIf=\"FeWoFormGroup.controls.durchschnittliche_groesse_der_fe_wo_in_m2.valid\" class=\"success-icon\" matSuffix >done</mat-icon>\r\n                            </mat-form-field>\r\n                            <h6>Wie hoch ist maximale durchschnittliche Belegungszahl einer Ferienwohnung?</h6>\r\n                            <h6 *ngIf=\"selected=='Ferienwohnungen'\" class=\"fontweightnormal\">\r\n                                    Beispiel: <br>\r\n                                    Ferienwohnung 1: 1-5 Personen <br>\r\n                                    Ferienwohnung 2: 1-3 Personen <br>\r\n                                    Durchschnittliche maximale Belegungszahl einer Ferienwohnung: (5 + 3)/ 2 =  4 Personen                                    \r\n                            </h6>\r\n                            <mat-form-field class=\"full-width\">\r\n                                    <input [errorStateMatcher]=\"matcher\" matInput type=\"number\" min=\"0\" minlength=\"1\" required placeholder=\"durchschnittliche Belegung einer Ferienwohnung\" formControlName=\"durchschnittliche_maximale_belegungszahl_der_fe_wo\" [formControl]=\"FeWoFormGroup.controls.durchschnittliche_maximale_belegungszahl_der_fe_wo\">\r\n                                    \r\n                                    <mat-icon *ngIf=\"FeWoFormGroup.controls.durchschnittliche_maximale_belegungszahl_der_fe_wo.valid\" class=\"success-icon\" matSuffix >done</mat-icon>\r\n                            </mat-form-field>\r\n                        </div>                          \r\n                    </form>\r\n            </div>  \r\n            <div class=\"col-md-12 col-sm-12\">\r\n                <form class=\"form\" [formGroup]=\"betriebsdatenFormGroup\">\r\n                    <div class=\"card p-2\">\r\n                        <h6 *ngIf=\"selected=='Hotels'\"> Wie viele Zimmer hat Ihr Hotel?</h6>\r\n                        <h6 *ngIf=\"selected=='Ferienwohnungen'\"> Wie viele {{selected}} betreiben Sie?</h6>\r\n                        <mat-form-field class=\"full-width\">\r\n                                    <mat-select [value]=\"betriebsdatenFormGroup.controls.anzahl_der_einrichtungen.value\" [formControl]=\"betriebsdatenFormGroup.controls.anzahl_der_einrichtungen\" placeholder=\"Bitte wählen Sie aus der Liste\">\r\n                                        <mat-option *ngFor=\"let anzahl of Nummer_Anzahl_der_Einrichtungen;let i=index\" [value]=\"i\">{{i}}</mat-option>                                                          \r\n                                    </mat-select>\r\n                        </mat-form-field>\r\n                        <h6>Wie viele Bettenübernachtungen haben Sie pro Jahr? (Pax per year)</h6>\r\n                        <h6 *ngIf=\"selected=='Ferienwohnungen'\" class=\"fontweightnormal\">Beispiel: 4 Personen in einer Ferienwohnung = 4 Bettenübernachtungen</h6>\r\n                        <h6 *ngIf=\"selected=='Hotels'\" class=\"fontweightnormal\">Beispiel: 2 Personen in einem Doppelzimmer = 2 Bettenübernachtungen</h6>\r\n                        <mat-form-field class=\"full-width\">\r\n                                    <input [errorStateMatcher]=\"matcher\" matInput type=\"text\" pattern=\"\\d+\" min=\"0\" minlength=\"1\" required placeholder=\"Anzahl der Bettenübernachtungen / PAX per night\" formControlName=\"anzahl_bettenuebernachtungen_pro_jahr\" [formControl]=\"betriebsdatenFormGroup.controls.anzahl_bettenuebernachtungen_pro_jahr\">\r\n                                    <mat-icon *ngIf=\"betriebsdatenFormGroup.controls.anzahl_bettenuebernachtungen_pro_jahr.valid\" class=\"success-icon\" matSuffix >done</mat-icon>\r\n                        </mat-form-field>\r\n                        <div *ngIf=\"selected=='Hotels'\">\r\n                            <h6>Gehört Ihr Hotel zu dem Hoteltyp garni?</h6>\r\n                            <h6 class=\"fontweightnormal\">Ein Hotel garni ist ein Hotelbetrieb, der Beherbergung, Frühstück, Getränke und höchstens kleine Speisen anbietet.</h6>\r\n                            <mat-slide-toggle #garni [color]=\"color\" [checked]=\"betriebsdatenFormGroup.controls.checkedFormControl9_garni.value\" formControlName=\"checkedFormControl9_garni\" [formControl]=\"betriebsdatenFormGroup.controls.checkedFormControl9_garni\">{{betriebsdatenFormGroup.controls.checkedFormControl9_garni.value ?'Ja' : 'Nein'}}</mat-slide-toggle>\r\n                        </div>                     \r\n                    </div>\r\n                    <div class=\"card p-2\">\r\n                        <h6>Sind Sie offiziell Sterne zertifiziert bzw. haben Sie offiziell Sterne?</h6>\r\n                        <mat-slide-toggle #sterne aria-label type=\"checkbox\" [color]=\"color\" [checked]=\"betriebsdatenFormGroup.controls.checkedFormControl_sterne.value\" formControlName=\"checkedFormControl_sterne\" [formControl]=\"betriebsdatenFormGroup.controls.checkedFormControl_sterne\">\r\n                            <h6 class=\"mt-2 align-middle\">{{betriebsdatenFormGroup.controls.checkedFormControl_sterne.value ? 'Ja' : 'Nein'}}</h6></mat-slide-toggle>\r\n                        <h6 *ngIf=\"betriebsdatenFormGroup.controls.checkedFormControl_sterne.value; else keineSterne\">Wie viele Sterne hat Ihre Unterkunft?</h6>\r\n                        <ng-template #keineSterne><h6>Zu Vergleichzwecken mit anderen Einrichtungen: Wie viele Sterne würden Sie Ihrer Unterkunft geben?</h6></ng-template>\r\n                        <h6 *ngIf=\"selected=='Ferienwohnungen'\" class=\"fontweightnormal\">Wenn Ihre einzelnen Ferienwohnungen unterschiedlich viele Sterne haben, nehmen Sie bitte den Durchschnitt.</h6>\r\n                        <h6 class=\"fontweightnormal\">Informationen zu den Kriterien finden Sie <a href=\"https://www.hotelstars.eu/de/deutschland/kriterien/kriterienkatalog\" target=\"_blank\">-> hier</a></h6>\r\n                            <div class=\"mx-auto\">  \r\n                                    <a  *ngFor=\"let item of starList; let i=index \" (click)=\"setStar(i)\">  \r\n                                    <i class=\"fa-star fa-2x green\" [ngClass]=\"{'far':item,'fas':!item}\"></i>\r\n                                    </a>             \r\n                            </div>\r\n                    </div>\r\n                    <div class=\"card p-2\">\r\n                        <h6>Wie viele Restaurantsitzplätze (exklusive Bankettplätze) haben Sie?</h6>\r\n                        <mat-form-field class=\"full-width\">\r\n                                    <input [errorStateMatcher]=\"matcher\" matInput type=\"text\" pattern=\"\\d+\" min=\"0\" minlength=\"0\" placeholder=\"Anzahl der Restaurantsitzplätze\" formControlName=\"anzahl_restaurantsitzplaetze\" [formControl]=\"betriebsdatenFormGroup.controls.anzahl_restaurantsitzplaetze\">\r\n                                    <mat-icon *ngIf=\"betriebsdatenFormGroup.controls.anzahl_restaurantsitzplaetze.valid\" class=\"success-icon\" matSuffix >done</mat-icon>\r\n                        </mat-form-field>\r\n                    \r\n                        <h6>Wie viele Gedecke/Mahlzeiten/Frühstücke geben Sie im Jahr aus?</h6>\r\n                        <h6 class=\"fontweightnormal\">1 Hauptgericht (auch mit mehreren Gängen) = 1 Gedeck bzw. 1 Mahlzeit</h6>        \r\n                        <mat-form-field class=\"full-width\">\r\n                                    <input [errorStateMatcher]=\"matcher\" matInput type=\"text\" pattern=\"\\d+\" min=\"0\" minlength=\"0\" placeholder=\"Anzahl ausgegebener Gedecke pro Jahr\" formControlName=\"anzahl_mahlzeiten_im_jahr\" [formControl]=\"betriebsdatenFormGroup.controls.anzahl_mahlzeiten_im_jahr\">\r\n                                    <mat-icon *ngIf=\"betriebsdatenFormGroup.controls.anzahl_mahlzeiten_im_jahr.valid\" class=\"success-icon\" matSuffix >done</mat-icon>\r\n                        </mat-form-field>\r\n\r\n                        <h6>Wie viele Prozent sind davon Mehrgängemenüs?</h6>\r\n                        <mat-form-field class=\"full-width\">\r\n                                    <input [errorStateMatcher]=\"matcher\" matInput type=\"text\" pattern=\"\\d+\" min=\"0\" minlength=\"0\" placeholder=\"Anteil an Mehrgängemenüs in Prozent\" formControlName=\"anteil_mehrgaenge_menues\" [formControl]=\"betriebsdatenFormGroup.controls.anteil_mehrgaenge_menues\">\r\n                                    <mat-icon *ngIf=\"betriebsdatenFormGroup.controls.anteil_mehrgaenge_menues.valid\" class=\"success-icon\" matSuffix >done</mat-icon>\r\n                        </mat-form-field>\r\n                    </div>\r\n                    <div class=\"card p-2\">\r\n                        <h6>Wie groß ist Ihre beheizte Gesamtfläche in m²?</h6>\r\n                        <mat-form-field class=\"full-width\">\r\n                            <input [errorStateMatcher]=\"matcher\" matInput type=\"number\" min=\"0\" minlength=\"1\" required placeholder=\"Beheizte Gesamtfläche in m²\" formControlName=\"beheizte_gesamtflaeche_in_m3\" [formControl]=\"betriebsdatenFormGroup.controls.beheizte_gesamtflaeche_in_m3\">\r\n                            <span matSuffix>m²</span>\r\n                            <mat-icon *ngIf=\"betriebsdatenFormGroup.controls.beheizte_gesamtflaeche_in_m3.valid\" class=\"success-icon\" matSuffix >done</mat-icon>\r\n                        </mat-form-field>\r\n                    <h6>Wie viel kg Eigenwäsche produzieren Sie im Jahr? (inkl. Wäsche aus dem Wellnessbereich.)</h6>\r\n                    <mat-form-field class=\"full-width\">\r\n                                <input [errorStateMatcher]=\"matcher\" matInput type=\"number\" min=\"0\" minlength=\"1\" required placeholder=\"Eigenwäsche pro Jahr in kg\" formControlName=\"eigenwaesche_in_kg\" [formControl]=\"betriebsdatenFormGroup.controls.eigenwaesche_in_kg\">\r\n                                <span matSuffix>kg</span>\r\n                                <mat-icon *ngIf=\"betriebsdatenFormGroup.controls.eigenwaesche_in_kg.valid\" class=\"success-icon\" matSuffix >done</mat-icon>\r\n                    </mat-form-field>\r\n                    </div>\r\n                </form>   \r\n            </div>\r\n            <div class=\"col-md-12 col-sm-12\">\r\n            <form class=\"form card p-2\" [formGroup]=\"privatFormGroup\">\r\n                <h6>Wohnen Sie auch privat in Ihrer Einrichtung?</h6>\r\n                <mat-slide-toggle #p [color]=\"color\" [checked]=\"privatFormGroup.controls.checkedFormControl2.value\" formControlName=\"checkedFormControl2\" [formControl]=\"privatFormGroup.controls.checkedFormControl2\">{{privatFormGroup.controls.checkedFormControl2.value ?'Ja' : 'Nein'}}</mat-slide-toggle>\r\n                <div *ngIf=\"privatFormGroup.controls.checkedFormControl2.value\">\r\n                        <h6>Wie viele Menschen wohnen dort privat?</h6>\r\n                        <h6 class=\"fontweightnormal\">Achtung: Kinder zählen als Personen</h6>\r\n                        <mat-form-field class=\"full-width\">                                   \r\n                                    <mat-select value=\"privatFormGroup.controls.anzahl_privater_bewohner.value\" required placeholder=\"Anzahl der Privatbewohner\" [formControl]=\"privatFormGroup.controls.anzahl_privater_bewohner\"> \r\n                                        <mat-option *ngFor=\"let number of nummer_anzahl_privater_bewohner; let i = index\" [value]=\"i+1\" ><!--Faulheit lass nach :D-->\r\n                                               {{i+1}}\r\n                                        </mat-option>\r\n                                    </mat-select>\r\n                        </mat-form-field>\r\n                        <h6>Wie groß ist die Privatwohnung in m²?</h6>\r\n                        <mat-form-field class=\"full-width\"  >\r\n                                    <input matInput min=\"0\" type=\"number\" minlength=\"1\" required placeholder=\"Größe der Privatwohnung in m²\" formControlName=\"DavonPrivatwohnflaecheInM2\" [formControl]=\"privatFormGroup.controls.DavonPrivatwohnflaecheInM2\">\r\n                                    <span matSuffix>m²</span>\r\n                                    <mat-icon *ngIf=\"privatFormGroup.controls.DavonPrivatwohnflaecheInM2.valid\" class=\"success-icon\" matSuffix >done</mat-icon>                                \r\n                        </mat-form-field>\r\n                        <h6>Ist die Fläche der Privatwohnung in der angegebenen Gesamtfläche enthalten?</h6>\r\n                        <mat-slide-toggle #f aria-label [color]=\"color\" [(checked)]=\"privatFormGroup.controls.checkedFormControl3.value\" formControlName=\"checkedFormControl3\" [formControl]=\"privatFormGroup.controls.checkedFormControl3\"><h6 class=\"mt-2 align-middle\">{{privatFormGroup.controls.checkedFormControl3.value ?'Ja' : 'Nein'}}</h6></mat-slide-toggle>\r\n                        <h6 *ngIf=\"selected=='Ferienwohnungen'\">Laufen die Verbräuche Ihrer eigenen Privatwohnung über dieselben Zähler wie die der FeWo(s)?</h6>\r\n                        <h6 *ngIf=\"selected=='Hotels'\">Laufen die Verbräuche Ihrer Privatwohnung über dieselben Zähler wie die des Hotels?</h6>\r\n                        <mat-slide-toggle #pv aria-label [color]=\"color\" [(checked)]=\"privatFormGroup.controls.checkedFormControl4_privateVerbraeuche.value\" formControlName=\"checkedFormControl4_privateVerbraeuche\" [formControl]=\"privatFormGroup.controls.checkedFormControl4_privateVerbraeuche\"><h6 class=\"mt-2 align-middle\">{{privatFormGroup.controls.checkedFormControl4_privateVerbraeuche.value ?'Ja, und zwar folgende:' : 'Nein, private Zähler sind getrennt'}}</h6></mat-slide-toggle>\r\n                        <div *ngIf=\"privatFormGroup.controls.checkedFormControl4_privateVerbraeuche.value\" class=\"px-4\">\r\n                                <mat-slide-toggle #privatStrom aria-label [color]=\"color\" [(checked)]=\"privatFormGroup.controls.checkedFormControl5_privateVerbraeucheStrom.value\" formControlName=\"checkedFormControl5_privateVerbraeucheStrom\" [formControl]=\"privatFormGroup.controls.checkedFormControl5_privateVerbraeucheStrom\"><h6 class=\"mt-2 align-middle\">{{privatFormGroup.controls.checkedFormControl5_privateVerbraeucheStrom.value ?'Ja, Stromzähler sind dieselben' : 'Nein, es gibt getrennte Stromzähler'}}</h6></mat-slide-toggle><h6></h6>\r\n                                <mat-slide-toggle #privatHeizung aria-label [color]=\"color\" [(checked)]=\"privatFormGroup.controls.checkedFormControl6_privateVerbraeucheHeizung.value\" formControlName=\"checkedFormControl6_privateVerbraeucheHeizung\" [formControl]=\"privatFormGroup.controls.checkedFormControl6_privateVerbraeucheHeizung\"><h6 class=\"mt-2 align-middle\">{{privatFormGroup.controls.checkedFormControl6_privateVerbraeucheHeizung.value ?'Ja, Heizungsverbrauchszähler sind dieselben' : 'Nein, es gibt getrennte Heizungsverbrauchszähler'}}</h6></mat-slide-toggle><h6></h6>\r\n                                <mat-slide-toggle #privatWasser aria-label [color]=\"color\" [(checked)]=\"privatFormGroup.controls.checkedFormControl7_privateVerbraeucheWasser.value\" formControlName=\"checkedFormControl7_privateVerbraeucheWasser\" [formControl]=\"privatFormGroup.controls.checkedFormControl7_privateVerbraeucheWasser\"><h6 class=\"mt-2 align-middle\">{{privatFormGroup.controls.checkedFormControl7_privateVerbraeucheWasser.value ?'Ja, Wasserzähler sind dieselben' : 'Nein, es gibt getrennte Wasserzähler'}}</h6></mat-slide-toggle><h6></h6>\r\n                                <mat-slide-toggle #privatRestmuell aria-label [color]=\"color\" [(checked)]=\"privatFormGroup.controls.checkedFormControl8_privateVerbraeucheRestmuell.value\" formControlName=\"checkedFormControl8_privateVerbraeucheRestmuell\" [formControl]=\"privatFormGroup.controls.checkedFormControl8_privateVerbraeucheRestmuell\"><h6 class=\"mt-2 align-middle\">{{privatFormGroup.controls.checkedFormControl8_privateVerbraeucheRestmuell.value ?'Ja, der Müll geht in dieselben Tonnen' : 'Nein, es gibt private Mülltonnen'}}</h6></mat-slide-toggle>\r\n                        </div>\r\n                    </div>\r\n                <h6 [hidden]=\"true\">Nutzen Sie LEDs als Beleuchtung Ihrer Einrichtung/en?</h6>\r\n                        <mat-slide-toggle [hidden]=\"true\" #bg aria-label [color]=\"color\" [checked]=\"privatFormGroup.controls.checkedFormControl5_led.value\" formControlName=\"checkedFormControl5_led\" [formControl]=\"privatFormGroup.controls.checkedFormControl5_led\"><h6 class=\"mt-2 align-middle\">{{privatFormGroup.controls.checkedFormControl5_led.value ?'Ja' : 'Nein'}}</h6></mat-slide-toggle>\r\n\r\n            </form>            \r\n            </div>\r\n        \r\n\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/betriebsdaten/betriebsdaten.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/betriebsdaten/betriebsdaten.component.ts ***!
  \**********************************************************/
/*! exports provided: BetriebsdatenComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BetriebsdatenComponent", function() { return BetriebsdatenComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/core */ "./node_modules/@angular/material/esm5/core.es5.js");
/* harmony import */ var _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/slide-toggle */ "./node_modules/@angular/material/esm5/slide-toggle.es5.js");
/* harmony import */ var _services_data_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../_services/data.service */ "./src/app/_services/data.service.ts");
/* harmony import */ var _models_FeWo__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../_models/FeWo */ "./src/app/_models/FeWo.ts");
/* harmony import */ var _models_hotel__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../_models/hotel */ "./src/app/_models/hotel.ts");
/* harmony import */ var _models_begleitfrage__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../_models/begleitfrage */ "./src/app/_models/begleitfrage.ts");
/* harmony import */ var _services_pruefservice_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../_services/pruefservice.service */ "./src/app/_services/pruefservice.service.ts");











var BetriebsdatenComponent = /** @class */ (function () {
    function BetriebsdatenComponent(Dataservice, Pruefservice) {
        this.Dataservice = Dataservice;
        this.Pruefservice = Pruefservice;
        this.valuePrivat = false;
        this.valueFlacheInGesamtflaeche = false;
        this.anzahl_der_einrichtungen_value = '1';
        //Sternche für die Sternebewertung
        this.color = 'primary';
        this.checked = false;
        this.checked2 = false;
        //STAR RATING
        this.title = 'Star Rating';
        this.starList = [true, true, true, true, true]; // create a list which contains status of 5 stars
        this.nummer_anzahl_privater_bewohner = Array(500).map(function (x, i) { return i; });
        this.Nummer_Anzahl_der_Einrichtungen = Array(500).map(function (x, i) { return i; });
        this.selected = 'Hotels';
        this.anzahl_der_einrichtungen_value = '1';
        this.betriebsdatenFormGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            anzahl_bettenuebernachtungen_pro_jahr: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].min(0), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(1)]),
            anzahl_der_einrichtungen: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            beheizte_gesamtflaeche_in_m3: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            davon_privatwohnflaeche_in_m2: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            eigenwaesche_in_kg: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].min(0)]),
            anzahl_mahlzeiten_im_jahr: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            anteil_mehrgaenge_menues: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            abfall_in_kg_pro_jahr: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            checkedFormControl_sterne: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            checkedFormControl9_garni: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            anzahl_restaurantsitzplaetze: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern("\d+")]),
        });
        this.FeWoFormGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            anzahl_der_gaesteanreisen_pro_jahr: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern("\d+")]),
            anzahl_der_uebernachtungen_mai_bis_oktober: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern("\d+")]),
            anzahl_der_uebernachtungen_november_bis_april: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern("\d+")]),
            durchschnittliche_groesse_der_fe_wo_in_m2: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern("\d+")]),
            durchschnittliche_maximale_belegungszahl_der_fe_wo: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern("\d+")]),
        });
        this.privatFormGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            checkedFormControl2: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            anzahl_privater_bewohner: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            DavonPrivatwohnflaecheInM2: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            checkedFormControl3: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            checkedFormControl4_privateVerbraeuche: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            checkedFormControl5_privateVerbraeucheStrom: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            checkedFormControl6_privateVerbraeucheHeizung: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            checkedFormControl7_privateVerbraeucheWasser: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            checkedFormControl8_privateVerbraeucheRestmuell: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            checkedFormControl5_led: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
        });
        this.matcher = new _angular_material_core__WEBPACK_IMPORTED_MODULE_3__["ErrorStateMatcher"]();
        this.kalenderwochen = Array(52).map(function (x, i) { return i; });
        this.nummer_anzahl_privater_bewohner = Array(25).map(function (x, i) { return i; });
    }
    BetriebsdatenComponent.prototype.ngOnInit = function () {
        //listen auf dem Umweltcheckobservable und hol das Zeug da raus: 
        // Call subscribe() to start listening for updates.    
        var _this = this;
        this.Dataservice.UmweltchecksBhobj.subscribe(
        //Problem: wird einmal aufgerufnen beim Konstruktor und einmal, wenn die Datengeupdated werden.
        function (Umweltchecks) {
            console.log("BETRIEBSDATENCOMPONENTE:", Umweltchecks);
            if (Umweltchecks.Umweltchecks) {
                _this.pullDatainBetriebsdatenInBetriebsdatenComponente();
            }
        }, function (error) { console.log("ERROR in Betriebsdatencomponente.", error); });
        //Listen to saveBetriebsdaten-Event vom Parentcomponent: eingabe.component => SAVE local Data!
        this.saveBetriebsdaten.subscribe(function () {
            //lokal: schreibe die Werte in die Tonnen, dann in den Dataservice -
            _this.saveBetriebsdatenInDataservice();
        }, function (err) { console.log(err); }, function (complete) { console.log("Betriebsdaten erfolgereich vom Frontent versendet"); });
        this.FeWoFormGroup.controls.anzahl_der_uebernachtungen_mai_bis_oktober.valueChanges.subscribe(function () {
            var BÜNproJahr = Number(_this.FeWoFormGroup.controls.anzahl_der_uebernachtungen_mai_bis_oktober.value) + Number(_this.FeWoFormGroup.controls.anzahl_der_uebernachtungen_november_bis_april.value);
            _this.betriebsdatenFormGroup.controls.anzahl_bettenuebernachtungen_pro_jahr.setValue(BÜNproJahr);
        });
        this.FeWoFormGroup.controls.anzahl_der_uebernachtungen_november_bis_april.valueChanges.subscribe(function () {
            var BÜNproJahr = Number(_this.FeWoFormGroup.controls.anzahl_der_uebernachtungen_mai_bis_oktober.value) + Number(_this.FeWoFormGroup.controls.anzahl_der_uebernachtungen_november_bis_april.value);
            _this.betriebsdatenFormGroup.controls.anzahl_bettenuebernachtungen_pro_jahr.setValue(BÜNproJahr);
        });
        this.pruefeEingabe.subscribe(function () {
            _this.pruefeEingabeDerBetriebsdatencomponente();
        });
    };
    BetriebsdatenComponent.prototype.pullDatainBetriebsdatenInBetriebsdatenComponente = function () {
        var _this = this;
        if (this.Dataservice.Umweltchecks.Umweltchecks.length > 0) {
            if (this.umweltcheckSubscription) {
                this.umweltcheckSubscription.unsubscribe();
            }
            //Werte reinschmeißen aus dem Dataservice:
            this.betriebsdatenFormGroup.controls.anzahl_bettenuebernachtungen_pro_jahr.setValue(this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.anzahl_bettenuebernachtungen_pro_jahr);
            this.betriebsdatenFormGroup.controls.anzahl_der_einrichtungen.setValue(this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.anzahl_der_einrichtungen);
            this.betriebsdatenFormGroup.controls.beheizte_gesamtflaeche_in_m3.setValue(this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.beheizte_gesamtflaeche_in_m3);
            this.betriebsdatenFormGroup.controls.davon_privatwohnflaeche_in_m2.setValue(this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.davon_privatwohnflaeche_in_m2);
            this.betriebsdatenFormGroup.controls.eigenwaesche_in_kg.setValue(this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.eigenwaesche_in_kg);
            this.betriebsdatenFormGroup.controls.anzahl_mahlzeiten_im_jahr.setValue(this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.anzahl_mahlzeiten_im_jahr);
            this.betriebsdatenFormGroup.controls.anteil_mehrgaenge_menues.setValue(this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.anteil_mehrgaenge_menues);
            this.betriebsdatenFormGroup.controls.anzahl_restaurantsitzplaetze.setValue(this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.anzahl_restaurantsitzplaetze);
            this.betriebsdatenFormGroup.controls.abfall_in_kg_pro_jahr.setValue(this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.abfall_in_kg_pro_jahr);
            //console.log("ANZAHL DER STERNE: ",Math.abs(this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.anzahl_der_sterne))
            this.setStar(Math.abs(this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.anzahl_der_sterne) - 1);
            this.betriebsdatenFormGroup.controls.checkedFormControl_sterne.setValue(Math.abs(this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.anzahl_der_sterne));
            if (this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.anzahl_der_sterne < 0) {
                this._SlideToggle_Sterne.toggle();
            }
            //Ferienwohnung: Wenn vorhanden:
            if (this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.FeWo) {
                this.FeWoFormGroup.controls.anzahl_der_gaesteanreisen_pro_jahr.setValue(this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.FeWo.anzahl_der_gaesteanreisen_pro_jahr);
                this.FeWoFormGroup.controls.anzahl_der_uebernachtungen_mai_bis_oktober.setValue(this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.FeWo.anzahl_der_uebernachtungen_mai_bis_oktober);
                this.FeWoFormGroup.controls.anzahl_der_uebernachtungen_november_bis_april.setValue(this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.FeWo.anzahl_der_uebernachtungen_november_bis_april);
                this.FeWoFormGroup.controls.durchschnittliche_groesse_der_fe_wo_in_m2.setValue(this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.FeWo.durchschnittliche_groesse_der_fe_wo_in_m2);
                this.FeWoFormGroup.controls.durchschnittliche_maximale_belegungszahl_der_fe_wo.setValue(this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.FeWo.durchschnittliche_maximale_belegungszahl_der_fe_wo);
                this.selected = 'Ferienwohnungen';
            }
            //Hotel: wenn Du eins bist, dann
            if (this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.hotel_id > 0) {
                this.selected = 'Hotels';
                //Garni: Ja / Nein?
                if (this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.Hotel) {
                    if (this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.Hotel.hotel_kategorie == "garni") {
                        this._SlideToggle_garni.toggle();
                    }
                }
            }
        }
        //Privatpersonen:
        if (this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.anzahl_privater_bewohner > 0) {
            this._SlideToggle_privat.toggle();
            //eine Sekunde warten, weil ng-if das erst in die DOM laden muss #AngularFail
            setTimeout(function () {
                _this.privatFormGroup.controls.anzahl_privater_bewohner.setValue(_this.Dataservice.Umweltchecks.Umweltchecks[_this.Dataservice.NummerUmweltcheck].Einrichtung.anzahl_privater_bewohner);
                _this.privatFormGroup.controls.DavonPrivatwohnflaecheInM2.setValue(_this.Dataservice.Umweltchecks.Umweltchecks[_this.Dataservice.NummerUmweltcheck].Einrichtung.davon_privatwohnflaeche_in_m2);
                //der Toggle wird durch ngIf an und ausgeschaltet:
                //dadurch ist _SlideToggle_privateVerbraeucheSelberZaehler undefinded, wenn 
                //wohnen da private nicht getoggled ist
                _this._SlideToggle_privateVerbraeucheSelberZaehler.toggleChange.subscribe(function (val) {
                    _this._SlideToggle_privatHeizung.toggle();
                    _this._SlideToggle_privatWasser.toggle();
                    _this._SlideToggle_privatStrom.toggle();
                    _this._SlideToggle_privatRestmuell.toggle();
                });
            }, 500);
            //private verbraeuche:
            if (this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.kein_privater_heizungszaehler ||
                this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.kein_privater_stromzaehler ||
                this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.kein_privater_wasserzaehler) {
                //es gibt mindestens einen gemeinsamen zaehler: 
                //dann toggle den Haupttoggle:
                setTimeout(function () { _this._SlideToggle_privateVerbraeucheSelberZaehler.toggle(); }, 500);
                setTimeout(function () {
                    if (_this.Dataservice.Umweltchecks.Umweltchecks[_this.Dataservice.NummerUmweltcheck].Einrichtung.kein_privater_heizungszaehler) { //gemeinsamen Hiezungs
                        _this._SlideToggle_privatHeizung.toggle();
                    }
                    if (_this.Dataservice.Umweltchecks.Umweltchecks[_this.Dataservice.NummerUmweltcheck].Einrichtung.kein_privater_stromzaehler) { //gemeinsamen Hiezungs
                        _this._SlideToggle_privatStrom.toggle();
                    }
                    if (_this.Dataservice.Umweltchecks.Umweltchecks[_this.Dataservice.NummerUmweltcheck].Einrichtung.kein_privater_wasserzaehler) { //gemeinsamen Hiezungs
                        _this._SlideToggle_privatWasser.toggle();
                    }
                }, 500);
            }
            //begleitfrage LED
            var begleitfrageLED = this.Dataservice.getbegleitfrageAusbegleitfrageArray('LED');
            if (begleitfrageLED[0]) {
                if (begleitfrageLED[0].antwort)
                    this._SlideToggle_begleitfrageLED.toggle();
            }
        }
    };
    //Create a function which receives the value counting of stars click, 
    //and according to that value we do change the value of that star in list.
    BetriebsdatenComponent.prototype.setStar = function (data) {
        this.rating = data + 1;
        for (var i = 0; i <= 4; i++) {
            if (i <= data) {
                this.starList[i] = false;
            }
            else {
                this.starList[i] = true;
            }
        }
    };
    BetriebsdatenComponent.prototype.saveBetriebsdatenInDataservice = function () {
        if (this.Dataservice.Umweltchecks.Umweltchecks.length > 0) {
            //UPDATE Einrichtung:
            //im Backend wird immer zusamment mit einem Umweltcheck eine Einrichtung angelegt
            //deswegen können wir die Einrichtung immer Updaten und müssen Sie nicht neu anlegen
            console.log("SaveBetriebsdatenInDataservice:", this.betriebsdatenFormGroup.controls.anzahl_bettenuebernachtungen_pro_jahr.value);
            this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.anzahl_bettenuebernachtungen_pro_jahr = Number(this.betriebsdatenFormGroup.controls.anzahl_bettenuebernachtungen_pro_jahr.value);
            this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.anzahl_der_einrichtungen = Number(this.betriebsdatenFormGroup.controls.anzahl_der_einrichtungen.value);
            this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.beheizte_gesamtflaeche_in_m3 = Number(this.betriebsdatenFormGroup.controls.beheizte_gesamtflaeche_in_m3.value);
            this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.davon_privatwohnflaeche_in_m2 = Number(this.betriebsdatenFormGroup.controls.davon_privatwohnflaeche_in_m2.value);
            this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.eigenwaesche_in_kg = Number(this.betriebsdatenFormGroup.controls.eigenwaesche_in_kg.value);
            this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.anzahl_mahlzeiten_im_jahr = Number(this.betriebsdatenFormGroup.controls.anzahl_mahlzeiten_im_jahr.value);
            this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.anteil_mehrgaenge_menues = Number(this.betriebsdatenFormGroup.controls.anteil_mehrgaenge_menues.value);
            this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.anzahl_restaurantsitzplaetze = Number(this.betriebsdatenFormGroup.controls.anzahl_restaurantsitzplaetze.value);
            this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.abfall_in_kg_pro_jahr = Number(this.betriebsdatenFormGroup.controls.abfall_in_kg_pro_jahr.value);
            //FALL: Hotel
            if (this.selected == 'Hotels') {
                var hotel = new _models_hotel__WEBPACK_IMPORTED_MODULE_7__["Hotel"]();
                if (this._SlideToggle_garni && this._SlideToggle_garni.checked) {
                    hotel.hotel_kategorie = "garni";
                }
                else {
                    hotel.hotel_kategorie = "";
                }
                this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.Hotel = hotel;
                this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.FeWo = null;
                this.Dataservice.saveHotel();
                console.log("Hotel: save Hotel");
            }
            if (this._SlideToggle_Sterne.checked) { //ja
                this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.anzahl_der_sterne = this.rating;
            }
            else { //nein
                this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.anzahl_der_sterne = this.rating * (-1);
            }
            this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.anzahl_privater_bewohner = Number(this.privatFormGroup.controls.anzahl_privater_bewohner.value);
            this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.davon_privatwohnflaeche_in_m2 = Number(this.privatFormGroup.controls.DavonPrivatwohnflaecheInM2.value);
            //lege eine an, wenn ausgewaehlt:
            if (this.selected == 'Ferienwohnungen') {
                //lösche das Hotel am Datensatz
                this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.Hotel = null;
                this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.hotel_id = null;
                //UPDATE Ferienwohnungseinrichtung oder lege an    
                if (this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.FeWo) {
                    this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.FeWo.anzahl_der_ferienwohnungen = Number(this.betriebsdatenFormGroup.controls.anzahl_der_einrichtungen.value);
                    this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.FeWo.anzahl_der_gaesteanreisen_pro_jahr = Number(this.FeWoFormGroup.controls.anzahl_der_gaesteanreisen_pro_jahr.value);
                    this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.FeWo.anzahl_der_uebernachtungen_november_bis_april = Number(this.FeWoFormGroup.controls.anzahl_der_uebernachtungen_november_bis_april.value);
                    this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.FeWo.anzahl_der_uebernachtungen_mai_bis_oktober = Number(this.FeWoFormGroup.controls.anzahl_der_uebernachtungen_mai_bis_oktober.value);
                    this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.FeWo.durchschnittliche_groesse_der_fe_wo_in_m2 = Number(this.FeWoFormGroup.controls.durchschnittliche_groesse_der_fe_wo_in_m2.value);
                    this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.FeWo.durchschnittliche_maximale_belegungszahl_der_fe_wo = Number(this.FeWoFormGroup.controls.durchschnittliche_maximale_belegungszahl_der_fe_wo.value);
                    this.Dataservice.saveFeWo();
                }
                else {
                    //lege neue an: 
                    var FeWoh = new _models_FeWo__WEBPACK_IMPORTED_MODULE_6__["FeWo"]();
                    FeWoh.anzahl_der_ferienwohnungen = Number(this.betriebsdatenFormGroup.controls.anzahl_der_einrichtungen.value);
                    FeWoh.anzahl_der_gaesteanreisen_pro_jahr = Number(this.FeWoFormGroup.controls.anzahl_der_gaesteanreisen_pro_jahr.value);
                    FeWoh.anzahl_der_uebernachtungen_mai_bis_oktober = Number(this.FeWoFormGroup.controls.anzahl_der_uebernachtungen_mai_bis_oktober.value);
                    FeWoh.anzahl_der_uebernachtungen_november_bis_april = Number(this.FeWoFormGroup.controls.anzahl_der_uebernachtungen_november_bis_april.value);
                    FeWoh.durchschnittliche_groesse_der_fe_wo_in_m2 = Number(this.FeWoFormGroup.controls.durchschnittliche_groesse_der_fe_wo_in_m2.value);
                    FeWoh.durchschnittliche_maximale_belegungszahl_der_fe_wo = Number(this.FeWoFormGroup.controls.durchschnittliche_maximale_belegungszahl_der_fe_wo.value);
                    this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.FeWo = FeWoh;
                    this.Dataservice.saveFeWo();
                }
                this.Dataservice.deleteHotel();
            }
            else {
                //nicht selected (also es ist ein Hotel)
                //lösche die FeWo, wenn das nicht selected ist: = null
                if (this.selected != 'Ferienwohnungen') {
                    var FeWoID = 1;
                    this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.FeWo = null;
                    this.Dataservice.deleteFeWo(FeWoID);
                }
            }
            //Flaeche Enthalten Toggle: -> Berechnung:
            if (this._SlideToggle_flaecheEnthalten) {
                if (this._SlideToggle_flaecheEnthalten.checked) {
                    this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.privatWohnungInGesamtFlaecheEnthalten = true;
                }
            }
            //Laufen die Verbraeuche eigenen Privatwohnung über dieselben Zaehler wie die der FeWo(s)?
            if (this._SlideToggle_privateVerbraeucheSelberZaehler) {
                if (this._SlideToggle_privatHeizung && this._SlideToggle_privatHeizung.checked) {
                    this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.kein_privater_heizungszaehler = true;
                }
                if (this._SlideToggle_privatStrom && this._SlideToggle_privatStrom.checked) {
                    this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.kein_privater_stromzaehler = true;
                }
                if (this._SlideToggle_privatWasser && this._SlideToggle_privatWasser.checked) {
                    this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.kein_privater_wasserzaehler = true;
                }
            }
            //begleitfrage  
            if (this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.begleitfragen) {
                //1. begleitfrage löschen:
                this.Dataservice.deletebegleitfrage();
                //2. anhaengen
                var begleitdatumLED = new _models_begleitfrage__WEBPACK_IMPORTED_MODULE_8__["begleitfrage"]();
                begleitdatumLED.datums_typ = 'LED';
                begleitdatumLED.antwort = this._SlideToggle_begleitfrageLED.checked;
                //dataservice pushen
                this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.begleitfragen.push(begleitdatumLED);
            }
            //jetzt Behavoir Object updaten: 
            this.Dataservice.UmweltchecksBhobj.next(this.Dataservice.Umweltchecks);
            //schicke alle http-Requests ab (weil hier erst alle Daten komplett sind: 
            this.Dataservice.saveEinrichtung();
            this.Dataservice.savebegleitfrage();
        }
    };
    BetriebsdatenComponent.prototype.pruefeEingabeDerBetriebsdatencomponente = function () {
        /*
    this.betriebsdatenFormGroup = new FormGroup({
      anzahl_bettenuebernachtungen_pro_jahr  :  new FormControl('', [Validators.required,Validators.min(0),Validators.minLength(1)]),
      anzahl_der_einrichtungen  :  new FormControl('', [Validators.required]),
      beheizte_gesamtflaeche_in_m3  :  new FormControl('', [Validators.required]),
      davon_privatwohnflaeche_in_m2  :  new FormControl('', [Validators.required]),
      eigenwaesche_in_kg  :  new FormControl('', [Validators.required]),
      anzahl_mahlzeiten_im_jahr  :  new FormControl('', [Validators.required]),
      abfall_in_kg_pro_jahr  :  new FormControl('', [Validators.required]) ,
      checkedFormControl_sterne :  new FormControl('', [Validators.required]),
    })*/
        if (this.betriebsdatenFormGroup.controls.anzahl_bettenuebernachtungen_pro_jahr.invalid)
            this.Pruefservice.pushNameErrorArray("Anzahl der Bettenübernachtungen pro Jahr", this.betriebsdatenFormGroup.controls.anzahl_bettenuebernachtungen_pro_jahr.errors);
        if (this.betriebsdatenFormGroup.controls.anzahl_der_einrichtungen.invalid)
            this.Pruefservice.pushNameErrorArray("Anzahl der Einrichtungen", this.betriebsdatenFormGroup.controls.anzahl_der_einrichtungen.errors);
        if (this.betriebsdatenFormGroup.controls.beheizte_gesamtflaeche_in_m3.invalid)
            this.Pruefservice.pushNameErrorArray("Beheizte Gesamtlfaeche", this.betriebsdatenFormGroup.controls.beheizte_gesamtflaeche_in_m3.errors);
        if (this.betriebsdatenFormGroup.controls.eigenwaesche_in_kg.invalid)
            this.Pruefservice.pushNameErrorArray("Eigenwäsche", this.betriebsdatenFormGroup.controls.eigenwaesche_in_kg.errors);
        if (this.betriebsdatenFormGroup.controls.anzahl_mahlzeiten_im_jahr.invalid)
            this.Pruefservice.pushNameErrorArray("Anzahl der ausgegebenen Mahlzeiten pro Jahr", this.betriebsdatenFormGroup.controls.anzahl_mahlzeiten_im_jahr.errors);
        if (this.betriebsdatenFormGroup.controls.anteil_mehrgaenge_menues.invalid)
            this.Pruefservice.pushNameErrorArray("Anteil Mehrgaenge Menues", this.betriebsdatenFormGroup.controls.anteil_mehrgaenge_menues.errors);
        if (Number(this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.oekostromverbrauch_in_kwh) / (Number(this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.stromverbrauch_in_kwh) + Number(this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.oekostromverbrauch_in_kwh)) * 100 < 0 || (Number(this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.oekostromverbrauch_in_kwh)) / (Number(this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.stromverbrauch_in_kwh) + Number(this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.oekostromverbrauch_in_kwh)) * 100 > 100) {
            this.Pruefservice.pushNameErrorArray("Ökostromverbrauch in %: muss zwischen 0 % und 100 % liegen", this.betriebsdatenFormGroup.controls.oekostromverbrauch_in_kwh.errors);
        }
        if (this.betriebsdatenFormGroup.controls.checkedFormControl_sterne.invalid)
            this.Pruefservice.pushNameErrorArray("Anzahl der Sterne", this.betriebsdatenFormGroup.controls.checkedFormControl_sterne.errors);
        this.Pruefservice.pushInvalidFormgroup("Betriebsdaten", this.betriebsdatenFormGroup);
        if (this.selected == 'Ferienwohnungen' && this.betriebsdatenFormGroup.controls.anzahl_der_gaesteanreisen_pro_jahr) {
            if (this.betriebsdatenFormGroup.controls.anzahl_der_gaesteanreisen_pro_jahr.invalid)
                this.Pruefservice.pushNameErrorArray("Anzahl der Gästeanreisen", this.betriebsdatenFormGroup.controls.anzahl_der_gaesteanreisen_pro_jahr.errors);
            if (this.betriebsdatenFormGroup.controls.anzahl_der_uebernachtungen_mai_bis_oktober.invalid)
                this.Pruefservice.pushNameErrorArray("Anzahl der Übernachtungen Mai bis Oktober", this.betriebsdatenFormGroup.controls.anzahl_der_uebernachtungen_mai_bis_oktober.errors);
            if (this.betriebsdatenFormGroup.controls.anzahl_der_uebernachtungen_november_bis_april.invalid)
                this.Pruefservice.pushNameErrorArray("Anzahl der Übernachtungen November bis April", this.betriebsdatenFormGroup.controls.anzahl_der_uebernachtungen_november_bis_april.errors);
            if (this.betriebsdatenFormGroup.controls.durchschnittliche_groesse_der_fe_wo_in_m2.invalid)
                this.Pruefservice.pushNameErrorArray("Durschnittliche Größe der Ferienwohnung in m²", this.betriebsdatenFormGroup.controls.durchschnittliche_groesse_der_fe_wo_in_m2.errors);
            if (this.betriebsdatenFormGroup.controls.durchschnittliche_maximale_belegungszahl_der_fe_wo.invalid)
                this.Pruefservice.pushNameErrorArray("Durchschnittliche maximale Belegungszahl", this.betriebsdatenFormGroup.controls.durchschnittliche_maximale_belegungszahl_der_fe_wo.errors);
            this.Pruefservice.pushInvalidFormgroup("Ferienwohnungen", this.FeWoFormGroup);
        }
        /*this.FeWoFormGroup = new FormGroup({
             anzahl_der_gaesteanreisen_pro_jahr:  new FormControl('', [Validators.required]),
             anzahl_der_uebernachtungen_mai_bis_oktober:  new FormControl('', [Validators.required]),
             anzahl_der_uebernachtungen_november_bis_april:  new FormControl('', [Validators.required]),
             durchschnittliche_groesse_der_fe_wo_in_m2:  new FormControl('', [Validators.required]),
             durchschnittliche_maximale_belegungszahl_der_fe_wo:  new FormControl('', [Validators.required]),
           })*/
        console.log("pruef Privat wohnen");
        if (this._SlideToggle_privat.checked) {
            if (this.privatFormGroup.controls.anzahl_privater_bewohner.invalid)
                this.Pruefservice.pushNameErrorArray("Anzahl privater Bewohner", this.privatFormGroup.controls.anzahl_privater_bewohner.errors);
            if (this.privatFormGroup.controls.DavonPrivatwohnflaecheInM2.invalid)
                this.Pruefservice.pushNameErrorArray("Private Wohnfläche", this.privatFormGroup.controls.DavonPrivatwohnflaecheInM2.errors);
            //if(this.privatFormGroup.controls.checkedFormControl3.invalid)
            //  this.Pruefservice.pushNameErrorArray("In Gesamtfläche enthalten",this.privatFormGroup.controls.checkedFormControl3.errors)
            //if(this.privatFormGroup.controls.checkedFormControl4_privateVerbraeuche.invalid)
            //  this.Pruefservice.pushNameErrorArray("Laufen private Verbräuche über den gleichen Zähler?",this.privatFormGroup.controls.checkedFormControl4_privateVerbraeuche.errors)
            //private Verbräuche
            /*if(this._SlideToggle_privateVerbraeucheSelberZaehler.checked){
              if(this.privatFormGroup.controls.checkedFormControl5_privateVerbraeucheStrom.invalid)
                this.Pruefservice.pushNameErrorArray("Private Verbraeuche Strom",this.privatFormGroup.controls.checkedFormControl5_privateVerbraeucheStrom.errors)
              if(this.privatFormGroup.controls.checkedFormControl6_privateVerbraeucheHeizung.invalid)
                this.Pruefservice.pushNameErrorArray("Private Verbraeuche Heizung",this.privatFormGroup.controls.checkedFormControl6_privateVerbraeucheHeizung.errors)
              if(this.privatFormGroup.controls.checkedFormControl7_privateVerbraeucheWasser.invalid)
                this.Pruefservice.pushNameErrorArray("Private Verbräuche Wasser",this.privatFormGroup.controls.checkedFormControl7_privateVerbraeucheWasser.errors)
              }*/
            //Wenn es private Verbräuche gibt, muss einer gechecked sein:
            if (this._SlideToggle_privateVerbraeucheSelberZaehler.checked) {
                //dann muss einer der toggles angeschoben sein, sonst gibs Fehlermeldung
                if (!((this._SlideToggle_privatHeizung && this._SlideToggle_privatHeizung.checked) || (this._SlideToggle_privatStrom && this._SlideToggle_privatStrom.checked) || (this._SlideToggle_privatWasser && this._SlideToggle_privatWasser.checked))) {
                    this.Pruefservice.pushNameErrorArray("Private Verbräuche: Bitte geben Sie an, ob Strom, Heizung über Wasser über denselben Zähler läuft.", this.privatFormGroup.controls.checkedFormControl7_privateVerbraeucheWasser.errors);
                }
            }
            this.Pruefservice.pushInvalidFormgroup("Privat Wohnen in der Ferienwohnung", this.privatFormGroup);
        }
        /*this.privatFormGroup = new FormGroup({
          checkedFormControl2 : new FormControl('', [Validators.required]),
          anzahl_privater_bewohner  :  new FormControl('', [Validators.required]),
          DavonPrivatwohnflaecheInM2 : new FormControl('', [Validators.required]),
          checkedFormControl3 : new FormControl('', [Validators.required]),
          checkedFormControl4_privateVerbraeuche :  new FormControl('', [Validators.required]),
          checkedFormControl5_privateVerbraeucheStrom :  new FormControl('', [Validators.required]),
          checkedFormControl6_privateVerbraeucheHeizung :  new FormControl('', [Validators.required]),
          checkedFormControl7_privateVerbraeucheWasser :  new FormControl('', [Validators.required]),
    
          checkedFormControl5_led :  new FormControl('', [Validators.required]),
        })*/
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], BetriebsdatenComponent.prototype, "saveBetriebsdaten", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], BetriebsdatenComponent.prototype, "pruefeEingabe", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('p'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_4__["MatSlideToggle"])
    ], BetriebsdatenComponent.prototype, "_SlideToggle_privat", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('f'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_4__["MatSlideToggle"])
    ], BetriebsdatenComponent.prototype, "_SlideToggle_flaecheEnthalten", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('pv'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_4__["MatSlideToggle"])
    ], BetriebsdatenComponent.prototype, "_SlideToggle_privateVerbraeucheSelberZaehler", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('privatStrom'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_4__["MatSlideToggle"])
    ], BetriebsdatenComponent.prototype, "_SlideToggle_privatStrom", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('privatWasser'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_4__["MatSlideToggle"])
    ], BetriebsdatenComponent.prototype, "_SlideToggle_privatWasser", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('privatHeizung'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_4__["MatSlideToggle"])
    ], BetriebsdatenComponent.prototype, "_SlideToggle_privatHeizung", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('privatRestmuell'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_4__["MatSlideToggle"])
    ], BetriebsdatenComponent.prototype, "_SlideToggle_privatRestmuell", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('bg'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_4__["MatSlideToggle"])
    ], BetriebsdatenComponent.prototype, "_SlideToggle_begleitfrageLED", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('sterne'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_4__["MatSlideToggle"])
    ], BetriebsdatenComponent.prototype, "_SlideToggle_Sterne", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('garni'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_4__["MatSlideToggle"])
    ], BetriebsdatenComponent.prototype, "_SlideToggle_garni", void 0);
    BetriebsdatenComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-betriebsdaten',
            template: __webpack_require__(/*! ./betriebsdaten.component.html */ "./src/app/betriebsdaten/betriebsdaten.component.html"),
            styles: [__webpack_require__(/*! ./betriebsdaten.component.css */ "./src/app/betriebsdaten/betriebsdaten.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_data_service__WEBPACK_IMPORTED_MODULE_5__["DataService"], _services_pruefservice_service__WEBPACK_IMPORTED_MODULE_9__["PruefService"]])
    ], BetriebsdatenComponent);
    return BetriebsdatenComponent;
}());



/***/ }),

/***/ "./src/app/datenschutz/datenschutz.component.css":
/*!*******************************************************!*\
  !*** ./src/app/datenschutz/datenschutz.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2RhdGVuc2NodXR6L2RhdGVuc2NodXR6LmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/datenschutz/datenschutz.component.html":
/*!********************************************************!*\
  !*** ./src/app/datenschutz/datenschutz.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"c72\" class=\"card p-4 frame frame-default frame-type-text frame-layout-0\">\r\n  <header><h1 class=\"\">\r\n    Datenschutzerklärung\r\n  </h1>\r\n</header>\r\n  <p>Wir freuen uns sehr über Ihr Interesse an unserem Unternehmen. Datenschutz hat einen besonders hohen Stellenwert für die Geschäftsleitung der Viabono GmbH. Eine Nutzung der Internetseiten der Viabono GmbH ist grundsätzlich ohne jede Angabe personenbezogener Daten möglich. Sofern eine betroffene Person besondere Services unseres Unternehmens über unsere Internetseite in Anspruch nehmen möchte, könnte jedoch eine Verarbeitung personenbezogener Daten erforderlich werden. Ist die Verarbeitung personenbezogener Daten erforderlich und besteht für eine solche Verarbeitung keine gesetzliche Grundlage, holen wir generell eine Einwilligung der betroffenen Person ein.</p>\r\n  <p>Die Verarbeitung personenbezogener Daten, beispielsweise des Namens, der Anschrift, E-Mail-Adresse oder Telefonnummer einer betroffenen Person, erfolgt stets im Einklang mit der Datenschutz-Grundverordnung und in Übereinstimmung mit den für die Viabono GmbH geltenden landesspezifischen Datenschutzbestimmungen. Mittels dieser Datenschutzerklärung möchte unser Unternehmen die Öffentlichkeit über Art, Umfang und Zweck der von uns erhobenen, genutzten und verarbeiteten personenbezogenen Daten informieren. Ferner werden betroffene Personen mittels dieser Datenschutzerklärung über die ihnen zustehenden Rechte aufgeklärt.</p><p>Die Viabono GmbH hat als für die Verarbeitung Verantwortlicher zahlreiche technische und organisatorische Maßnahmen umgesetzt, um einen möglichst lückenlosen Schutz der über diese Internetseite verarbeiteten personenbezogenen Daten sicherzustellen. Dennoch können Internetbasierte Datenübertragungen grundsätzlich Sicherheitslücken aufweisen, sodass ein absoluter Schutz nicht gewährleistet werden kann. Aus diesem Grund steht es jeder betroffenen Person frei, personenbezogene Daten auch auf alternativen Wegen, beispielsweise telefonisch, an uns zu übermitteln.<br> \r\n    &nbsp;</p>\r\n    <h4>1. Begriffsbestimmungen</h4>\r\n    <p>Die Datenschutzerklärung der Viabono GmbH beruht auf den Begrifflichkeiten, die durch den Europäischen Richtlinien- und Verordnungsgeber beim Erlass der Datenschutz-Grundverordnung (DS-GVO) verwendet wurden. Unsere Datenschutzerklärung soll sowohl für die Öffentlichkeit als auch für unsere Kunden und Geschäftspartner einfach lesbar und verständlich sein. Um dies zu gewährleisten, möchten wir vorab die verwendeten Begrifflichkeiten erläutern.</p>\r\n    <p>Wir verwenden in dieser Datenschutzerklärung unter anderem die folgenden Begriffe:</p>\r\n    <ul><li><h4>a) personenbezogene Daten</h4><p>Personenbezogene Daten sind alle Informationen, die sich auf eine identifizierte oder identifizierbare natürliche Person (im Folgenden „betroffene Person“) beziehen. Als identifizierbar wird eine natürliche Person angesehen, die direkt oder indirekt, insbesondere mittels Zuordnung zu einer Kennung wie einem Namen, zu einer Kennnummer, zu Standortdaten, zu einer Online-Kennung oder zu einem oder mehreren besonderen Merkmalen, die Ausdruck der physischen, physiologischen, genetischen, psychischen, wirtschaftlichen, kulturellen oder sozialen Identität dieser natürlichen Person sind, identifiziert werden kann.</p></li><li><h4>b) betroffene Person</h4><p>Betroffene Person ist jede identifizierte oder identifizierbare natürliche Person, deren personenbezogene Daten von dem für die Verarbeitung Verantwortlichen verarbeitet werden.</p></li><li><h4>c) Verarbeitung</h4><p>Verarbeitung ist jeder mit oder ohne Hilfe automatisierter Verfahren ausgeführte Vorgang oder jede solche Vorgangsreihe im Zusammenhang mit personenbezogenen Daten wie das Erheben, das Erfassen, die Organisation, das Ordnen, die Speicherung, die Anpassung oder Veränderung, das Auslesen, das Abfragen, die Verwendung, die Offenlegung durch Übermittlung, Verbreitung oder eine andere Form der Bereitstellung, den Abgleich oder die Verknüpfung, die Einschränkung, das Löschen oder die Vernichtung.</p></li><li><h4>d) Einschränkung der Verarbeitung</h4><p>Einschränkung der Verarbeitung ist die Markierung gespeicherter personenbezogener Daten mit dem Ziel, ihre künftige Verarbeitung einzuschränken.</p></li><li><h4>e) Profiling</h4><p>Profiling ist jede Art der automatisierten Verarbeitung personenbezogener Daten, die darin besteht, dass diese personenbezogenen Daten verwendet werden, um bestimmte persönliche Aspekte, die sich auf eine natürliche Person beziehen, zu bewerten, insbesondere, um Aspekte bezüglich Arbeitsleistung, wirtschaftlicher Lage, Gesundheit, persönlicher Vorlieben, Interessen, Zuverlässigkeit, Verhalten, Aufenthaltsort oder Ortswechsel dieser natürlichen Person zu analysieren oder vorherzusagen.</p></li><li><h4>f) Pseudonymisierung</h4><p>Pseudonymisierung ist die Verarbeitung personenbezogener Daten in einer Weise, auf welche die personenbezogenen Daten ohne Hinzuziehung zusätzlicher Informationen nicht mehr einer spezifischen betroffenen Person zugeordnet werden können, sofern diese zusätzlichen Informationen gesondert aufbewahrt werden und technischen und organisatorischen Maßnahmen unterliegen, die gewährleisten, dass die personenbezogenen Daten nicht einer identifizierten oder identifizierbaren natürlichen Person zugewiesen werden.</p></li><li><h4>g) Verantwortlicher oder für die Verarbeitung Verantwortlicher</h4><p>Verantwortlicher oder für die Verarbeitung Verantwortlicher ist die natürliche oder juristische Person, Behörde, Einrichtung oder andere Stelle, die allein oder gemeinsam mit anderen über die Zwecke und Mittel der Verarbeitung von personenbezogenen Daten entscheidet. Sind die Zwecke und Mittel dieser Verarbeitung durch das Unionsrecht oder das Recht der Mitgliedstaaten vorgegeben, so kann der Verantwortliche beziehungsweise können die bestimmten Kriterien seiner Benennung nach dem Unionsrecht oder dem Recht der Mitgliedstaaten vorgesehen werden.</p></li><li><h4>h) Auftragsverarbeiter</h4><p>Auftragsverarbeiter ist eine natürliche oder juristische Person, Behörde, Einrichtung oder andere Stelle, die personenbezogene Daten im Auftrag des Verantwortlichen verarbeitet.</p></li><li><h4>i) Empfänger</h4><p>Empfänger ist eine natürliche oder juristische Person, Behörde, Einrichtung oder andere Stelle, der personenbezogene Daten offengelegt werden, unabhängig davon, ob es sich bei ihr um einen Dritten handelt oder nicht. Behörden, die im Rahmen eines bestimmten Untersuchungsauftrags nach dem Unionsrecht oder dem Recht der Mitgliedstaaten möglicherweise personenbezogene Daten erhalten, gelten jedoch nicht als Empfänger.</p></li><li><h4>j) Dritter</h4><p>Dritter ist eine natürliche oder juristische Person, Behörde, Einrichtung oder andere Stelle außer der betroffenen Person, dem Verantwortlichen, dem Auftragsverarbeiter und den Personen, die unter der unmittelbaren Verantwortung des Verantwortlichen oder des Auftragsverarbeiters befugt sind, die personenbezogenen Daten zu verarbeiten.</p></li><li><h4>k) Einwilligung</h4><p>Einwilligung ist jede von der betroffenen Person freiwillig für den bestimmten Fall in informierter Weise und unmissverständlich abgegebene Willensbekundung in Form einer Erklärung oder einer sonstigen eindeutigen bestätigenden Handlung, mit der die betroffene Person zu verstehen gibt, dass sie mit der Verarbeitung der sie betreffenden personenbezogenen Daten einverstanden ist.<br> \t&nbsp;</p></li></ul><h4>2. Name und Anschrift des für die Verarbeitung Verantwortlichen</h4><p>Verantwortlicher im Sinne der Datenschutz-Grundverordnung, sonstiger in den Mitgliedstaaten der Europäischen Union geltenden Datenschutzgesetze und anderer Bestimmungen mit datenschutzrechtlichem Charakter ist die:</p><p>Viabono GmbH<br> Hauptstr. 230<br> 51503 Rösrath<br> Deutschland</p><p>Tel.: 02205 91 98 350<br> E-Mail: info(at)viabono.de<br> Website: www.viabono.de<br> &nbsp;</p><h4>3. Cookies</h4><p>Die Internetseiten der Viabono GmbH verwenden Cookies. Cookies sind Textdateien, welche über einen Internetbrowser auf einem Computersystem abgelegt und gespeichert werden.</p><p>Zahlreiche Internetseiten und Server verwenden Cookies. Viele Cookies enthalten eine sogenannte Cookie-ID. Eine Cookie-ID ist eine eindeutige Kennung des Cookies. Sie besteht aus einer Zeichenfolge, durch welche Internetseiten und Server dem konkreten Internetbrowser zugeordnet werden können, in dem das Cookie gespeichert wurde. Dies ermöglicht es den besuchten Internetseiten und Servern, den individuellen Browser der betroffenen Person von anderen Internetbrowsern, die andere Cookies enthalten, zu unterscheiden. Ein bestimmter Internetbrowser kann über die eindeutige Cookie-ID wiedererkannt und identifiziert werden.</p><p>Durch den Einsatz von Cookies kann die Viabono GmbH den Nutzern dieser Internetseite nutzerfreundlichere Services bereitstellen, die ohne die Cookie-Setzung nicht möglich wären.</p><p>Mittels eines Cookies können die Informationen und Angebote auf unserer Internetseite im Sinne des Benutzers optimiert werden. Cookies ermöglichen uns, wie bereits erwähnt, die Benutzer unserer Internetseite wiederzuerkennen. Zweck dieser Wiedererkennung ist es, den Nutzern die Verwendung unserer Internetseite zu erleichtern. Der Benutzer einer Internetseite, die Cookies verwendet, muss beispielsweise nicht bei jedem Besuch der Internetseite erneut seine Zugangsdaten eingeben, weil dies von der Internetseite und dem auf dem Computersystem des Benutzers abgelegten Cookie übernommen wird. Ein weiteres Beispiel ist das Cookie eines Warenkorbes im Online-Shop. Der Online-Shop merkt sich die Artikel, die ein Kunde in den virtuellen Warenkorb gelegt hat, über ein Cookie.</p><p>Die betroffene Person kann die Setzung von Cookies durch unsere Internetseite jederzeit mittels einer entsprechenden Einstellung des genutzten Internetbrowsers verhindern und damit der Setzung von Cookies dauerhaft widersprechen. Ferner können bereits gesetzte Cookies jederzeit über einen Internetbrowser oder andere Softwareprogramme gelöscht werden. Dies ist in allen gängigen Internetbrowsern möglich. Deaktiviert die betroffene Person die Setzung von Cookies in dem genutzten Internetbrowser, sind unter Umständen nicht alle Funktionen unserer Internetseite vollumfänglich nutzbar.<br> &nbsp;</p><h4>4. Erfassung von allgemeinen Daten und Informationen</h4><p>Die Internetseite der Viabono GmbH erfasst mit jedem Aufruf der Internetseite durch eine betroffene Person oder ein automatisiertes System eine Reihe von allgemeinen Daten und Informationen. Diese allgemeinen Daten und Informationen werden in den Logfiles des Servers gespeichert. Erfasst werden können die (1) verwendeten Browsertypen und Versionen, (2) das vom zugreifenden System verwendete Betriebssystem, (3) die Internetseite, von welcher ein zugreifendes System auf unsere Internetseite gelangt (sogenannte Referrer), (4) die Unterwebseiten, welche über ein zugreifendes System auf unserer Internetseite angesteuert werden, (5) das Datum und die Uhrzeit eines Zugriffs auf die Internetseite, (6) eine Internet-Protokoll-Adresse (IP-Adresse), (7) der Internet-Service-Provider des zugreifenden Systems und (8) sonstige ähnliche Daten und Informationen, die der Gefahrenabwehr im Falle von Angriffen auf unsere informationstechnologischen Systeme dienen.</p><p>Bei der Nutzung dieser allgemeinen Daten und Informationen zieht die Viabono GmbH keine Rückschlüsse auf die betroffene Person. Diese Informationen werden vielmehr benötigt, um (1) die Inhalte unserer Internetseite korrekt auszuliefern, (2) die Inhalte unserer Internetseite sowie die Werbung für diese zu optimieren, (3) die dauerhafte Funktionsfähigkeit unserer informationstechnologischen Systeme und der Technik unserer Internetseite zu gewährleisten sowie (4) um Strafverfolgungsbehörden im Falle eines Cyberangriffes die zur Strafverfolgung notwendigen Informationen bereitzustellen. Diese anonym erhobenen Daten und Informationen werden durch die Viabono GmbH daher einerseits statistisch und ferner mit dem Ziel ausgewertet, den Datenschutz und die Datensicherheit in unserem Unternehmen zu erhöhen, um letztlich ein optimales Schutzniveau für die von uns verarbeiteten personenbezogenen Daten sicherzustellen. Die anonymen Daten der Server-Logfiles werden getrennt von allen durch eine betroffene Person angegebenen personenbezogenen Daten gespeichert.<br> &nbsp;</p><h4>5. Abonnement unseres Newsletters</h4><p>Auf der Internetseite der Viabono GmbH wird den Benutzern die Möglichkeit eingeräumt, den Newsletter unseres Unternehmens zu abonnieren. Welche personenbezogenen Daten bei der Bestellung des Newsletters an den für die Verarbeitung Verantwortlichen übermittelt werden, ergibt sich aus der hierzu verwendeten Eingabemaske.<br> Die Viabono GmbH informiert ihre Kunden und Geschäftspartner in regelmäßigen Abständen im Wege eines Newsletters über Angebote des Unternehmens. Der Newsletter unseres Unternehmens kann von der betroffenen Person grundsätzlich nur dann empfangen werden, wenn (1) die betroffene Person über eine gültige E-Mail-Adresse verfügt und (2) die betroffene Person sich für den Newsletterversand registriert. An die von einer betroffenen Person erstmalig für den Newsletterversand eingetragene E-Mail-Adresse wird aus rechtlichen Gründen eine Bestätigungsmail im Double-Opt-In-Verfahren versendet. Diese Bestätigungsmail dient der Überprüfung, ob der Inhaber der E-Mail-Adresse als betroffene Person den Empfang des Newsletters autorisiert hat.<br> Bei der Anmeldung zum Newsletter speichern wir ferner die vom Internet-Service-Provider (ISP) vergebene IP-Adresse des von der betroffenen Person zum Zeitpunkt der Anmeldung verwendeten Computersystems sowie das Datum und die Uhrzeit der Anmeldung. Die Erhebung dieser Daten ist erforderlich, um den(möglichen) Missbrauch der E-Mail-Adresse einer betroffenen Person zu einem späteren Zeitpunkt nachvollziehen zu können und dient deshalb der rechtlichen Absicherung des für die Verarbeitung Verantwortlichen.<br> Die im Rahmen einer Anmeldung zum Newsletter erhobenen personenbezogenen Daten werden ausschließlich zum Versand unseres Newsletters verwendet. Ferner könnten Abonnenten des Newsletters per E-Mail informiert werden, sofern dies für den Betrieb des Newsletter-Dienstes oder eine diesbezügliche Registrierung erforderlich ist, wie dies im Falle von Änderungen am Newsletterangebot oder bei der Veränderung der technischen Gegebenheiten der Fall sein könnte. Es erfolgt keine Weitergabe der im Rahmen des Newsletter-Dienstes erhobenen personenbezogenen Daten an Dritte. Das Abonnement unseres Newsletters kann durch die betroffene Person jederzeit gekündigt werden. Die Einwilligung in die Speicherung personenbezogener Daten, die die betroffene Person uns für den Newsletterversand erteilt hat, kann jederzeit widerrufen werden. Zum Zwecke des Widerrufs der Einwilligung findet sich in jedem Newsletter ein entsprechender Link. Ferner besteht die Möglichkeit, sich jederzeit auch direkt auf der Internetseite des für die Verarbeitung Verantwortlichen vom Newsletterversand abzumelden oder dies dem für die Verarbeitung Verantwortlichen auf andere Weise mitzuteilen.<br> &nbsp;</p><h4>6. Newsletter-Tracking</h4><p>Die Newsletter der Viabono GmbH enthalten sogenannte Zählpixel. Ein Zählpixel ist eine Miniaturgrafik, die in solche E-Mails eingebettet wird, welche im HTML-Format versendet werden, um eine Logdatei-Aufzeichnung und eine Logdatei-Analyse zu ermöglichen. Dadurch kann eine statistische Auswertung des Erfolges oder Misserfolges von Online-Marketing-Kampagnen durchgeführt werden. Anhand des eingebetteten Zählpixels kann die Viabono GmbH erkennen, ob und wann eine E-Mail von einer betroffenen Person geöffnet wurde und welche in der E-Mail befindlichen Links von der betroffenen Person aufgerufen wurden.<br> Solche über die in den Newslettern enthaltenen Zählpixel erhobenen personenbezogenen Daten, werden von dem für die Verarbeitung Verantwortlichen gespeichert und ausgewertet, um den Newsletterversand zu optimieren und den Inhalt zukünftiger Newsletter noch besser den Interessen der betroffenen Person anzupassen. Diese personenbezogenen Daten werden nicht an Dritte weitergegeben. Betroffene Personen sind jederzeit berechtigt, die diesbezügliche gesonderte, über das Double-Opt-In-Verfahren abgegebene Einwilligungserklärung zu widerrufen. Nach einem Widerruf werden diese personenbezogenen Daten von dem für die Verarbeitung Verantwortlichen gelöscht. Eine Abmeldung vom Erhalt des Newsletters deutet die Viabono GmbH automatisch als Widerruf.<br> &nbsp;</p><h4>7. Newsletter-Tracking&nbsp;Registrierung auf unserer Internetseite</h4><p>Die betroffene Person hat die Möglichkeit, sich auf der Internetseite des für die Verarbeitung Verantwortlichen unter Angabe von personenbezogenen Daten zu registrieren. Welche personenbezogenen Daten dabei an den für die Verarbeitung Verantwortlichen übermittelt werden, ergibt sich aus der jeweiligen Eingabemaske, die für die Registrierung verwendet wird. Die von der betroffenen Person eingegebenen personenbezogenen Daten werden ausschließlich für die interne Verwendung bei dem für die Verarbeitung Verantwortlichen und für eigene Zwecke erhoben und gespeichert. Der für die Verarbeitung Verantwortliche kann die Weitergabe an einen oder mehrere Auftragsverarbeiter, beispielsweise einen Paketdienstleister, veranlassen, der die personenbezogenen Daten ebenfalls ausschließlich für eine interne Verwendung, die dem für die Verarbeitung Verantwortlichen zuzurechnen ist, nutzt.</p><p>Durch eine Registrierung auf der Internetseite des für die Verarbeitung Verantwortlichen wird ferner die vom Internet-Service-Provider (ISP) der betroffenen Person vergebene IP-Adresse, das Datum sowie die Uhrzeit der Registrierung gespeichert. Die Speicherung dieser Daten erfolgt vor dem Hintergrund, dass nur so der Missbrauch unserer Dienste verhindert werden kann, und diese Daten im Bedarfsfall ermöglichen, begangene Straftaten aufzuklären. Insofern ist die Speicherung dieser Daten zur Absicherung des für die Verarbeitung Verantwortlichen erforderlich. Eine Weitergabe dieser Daten an Dritte erfolgt grundsätzlich nicht, sofern keine gesetzliche Pflicht zur Weitergabe besteht oder die Weitergabe der Strafverfolgung dient.</p><p>Die Registrierung der betroffenen Person unter freiwilliger Angabe personenbezogener Daten dient dem für die Verarbeitung Verantwortlichen dazu, der betroffenen Person Inhalte oder Leistungen anzubieten, die aufgrund der Natur der Sache nur registrierten Benutzern angeboten werden können. Registrierten Personen steht die Möglichkeit frei, die bei der Registrierung angegebenen personenbezogenen Daten jederzeit abzuändern oder vollständig aus dem Datenbestand des für die Verarbeitung Verantwortlichen löschen zu lassen.</p><p>Der für die Verarbeitung Verantwortliche erteilt jeder betroffenen Person jederzeit auf Anfrage Auskunft darüber, welche personenbezogenen Daten über die betroffene Person gespeichert sind. Ferner berichtigt oder löscht der für die Verarbeitung Verantwortliche personenbezogene Daten auf Wunsch oder Hinweis der betroffenen Person, soweit dem keine gesetzlichen Aufbewahrungspflichten entgegenstehen. Ein in dieser Datenschutzerklärung namentlich benannter Datenschutzbeauftragter und die Gesamtheit der Mitarbeiter des für die Verarbeitung Verantwortlichen stehen der betroffenen Person in diesem Zusammenhang als Ansprechpartner zur Verfügung.<br> &nbsp;</p><h4>8. Kontaktmöglichkeit über die Internetseite</h4><p>Die Internetseite der Viabono GmbH enthält aufgrund von gesetzlichen Vorschriften Angaben, die eine schnelle elektronische Kontaktaufnahme zu unserem Unternehmen sowie eine unmittelbare Kommunikation mit uns ermöglichen, was ebenfalls eine allgemeine Adresse der sogenannten elektronischen Post (E-Mail-Adresse) umfasst. Sofern eine betroffene Person per E-Mail oder über ein Kontaktformular den Kontakt mit dem für die Verarbeitung Verantwortlichen aufnimmt, werden die von der betroffenen Person übermittelten personenbezogenen Daten automatisch gespeichert. Solche auf freiwilliger Basis von einer betroffenen Person an den für die Verarbeitung Verantwortlichen übermittelten personenbezogenen Daten werden für Zwecke der Bearbeitung oder der Kontaktaufnahme zur betroffenen Person gespeichert. Es erfolgt keine Weitergabe dieser personenbezogenen Daten an Dritte.<br> &nbsp;</p><h4>9. Routinemäßige Löschung und Sperrung von personenbezogenen Daten</h4><p>Der für die Verarbeitung Verantwortliche verarbeitet und speichert personenbezogene Daten der betroffenen Person nur für den Zeitraum, der zur Erreichung des Speicherungszwecks erforderlich ist oder sofern dies durch den Europäischen Richtlinien- und Verordnungsgeber oder einen anderen Gesetzgeber in Gesetzen oder Vorschriften, welchen der für die Verarbeitung Verantwortliche unterliegt, vorgesehen wurde.</p><p>Entfällt der Speicherungszweck oder läuft eine vom Europäischen Richtlinien- und Verordnungsgeber oder einem anderen zuständigen Gesetzgeber vorgeschriebene Speicherfrist ab, werden die personenbezogenen Daten routinemäßig und entsprechend den gesetzlichen Vorschriften gesperrt oder gelöscht.<br> &nbsp;</p><h4>10. Rechte der betroffenen Person</h4><ul><li><h4>a) Recht auf Bestätigung</h4><p>Jede betroffene Person hat das vom Europäischen Richtlinien- und Verordnungsgeber eingeräumte Recht, von dem für die Verarbeitung Verantwortlichen eine Bestätigung darüber zu verlangen, ob sie betreffende personenbezogene Daten verarbeitet werden. Möchte eine betroffene Person dieses Bestätigungsrecht in Anspruch nehmen, kann sie sich hierzu jederzeit an unseren Datenschutzbeauftragten oder einen anderen Mitarbeiter des für die Verarbeitung Verantwortlichen wenden.</p></li><li><h4>b) Recht auf Auskunft</h4><p>Jede von der Verarbeitung personenbezogener Daten betroffene Person hat das vom Europäischen Richtlinien- und Verordnungsgeber gewährte Recht, jederzeit von dem für die Verarbeitung Verantwortlichen unentgeltliche Auskunft über die zu seiner Person gespeicherten personenbezogenen Daten und eine Kopie dieser Auskunft zu erhalten. Ferner hat der Europäische Richtlinien- und Verordnungsgeber der betroffenen Person Auskunft über folgende Informationen zugestanden:</p><ul><li>die Verarbeitungszwecke</li><li>die Kategorien personenbezogener Daten, die verarbeitet werden</li><li>die Empfänger oder Kategorien von Empfängern, gegenüber denen die personenbezogenen Daten offengelegt worden sind oder noch offengelegt werden, insbesondere bei Empfängern in Drittländern oder bei internationalen Organisationen</li><li>falls möglich die geplante Dauer, für die die personenbezogenen Daten gespeichert werden, oder, falls dies nicht möglich ist, die Kriterien für die Festlegung dieser Dauer</li><li>das Bestehen eines Rechts auf Berichtigung oder Löschung der sie betreffenden personenbezogenen Daten oder auf Einschränkung der Verarbeitung durch den Verantwortlichen oder eines Widerspruchsrechts gegen diese Verarbeitung</li><li>das Bestehen eines Beschwerderechts bei einer Aufsichtsbehörde</li><li>wenn die personenbezogenen Daten nicht bei der betroffenen Person erhoben werden: Alle verfügbaren Informationen über die Herkunft der Daten</li><li>das Bestehen einer automatisierten Entscheidungsfindung einschließlich Profiling gemäß Artikel 22 Abs.1 und 4 DS-GVO und — zumindest in diesen Fällen — aussagekräftige Informationen über die involvierte Logik sowie die Tragweite und die angestrebten Auswirkungen einer derartigen Verarbeitung für die betroffene Person</li></ul><p>Ferner steht der betroffenen Person ein Auskunftsrecht darüber zu, ob personenbezogene Daten an ein Drittland oder an eine internationale Organisation übermittelt wurden. Sofern dies der Fall ist, so steht der betroffenen Person im Übrigen das Recht zu, Auskunft über die geeigneten Garantien im Zusammenhang mit der Übermittlung zu erhalten.</p><p>Möchte eine betroffene Person dieses Auskunftsrecht in Anspruch nehmen, kann sie sich hierzu jederzeit an unseren Datenschutzbeauftragten oder einen anderen Mitarbeiter des für die Verarbeitung Verantwortlichen wenden.</p></li><li><h4>c) Recht auf Berichtigung</h4><p>Jede von der Verarbeitung personenbezogener Daten betroffene Person hat das vom Europäischen Richtlinien- und Verordnungsgeber gewährte Recht, die unverzügliche Berichtigung sie betreffender unrichtiger personenbezogener Daten zu verlangen. Ferner steht der betroffenen Person das Recht zu, unter Berücksichtigung der Zwecke der Verarbeitung, die Vervollständigung unvollständiger personenbezogener Daten — auch mittels einer ergänzenden Erklärung — zu verlangen.</p><p>Möchte eine betroffene Person dieses Berichtigungsrecht in Anspruch nehmen, kann sie sich hierzu jederzeit an unseren Datenschutzbeauftragten oder einen anderen Mitarbeiter des für die Verarbeitung Verantwortlichen wenden.</p></li><li><h4>d) Recht auf Löschung (Recht auf Vergessen werden)</h4><p>Jede von der Verarbeitung personenbezogener Daten betroffene Person hat das vom Europäischen Richtlinien- und Verordnungsgeber gewährte Recht, von dem Verantwortlichen zu verlangen, dass die sie betreffenden personenbezogenen Daten unverzüglich gelöscht werden, sofern einer der folgenden Gründe zutrifft und soweit die Verarbeitung nicht erforderlich ist:</p><ul><li>Die personenbezogenen Daten wurden für solche Zwecke erhoben oder auf sonstige Weise verarbeitet, für welche sie nicht mehr notwendig sind.</li><li>Die betroffene Person widerruft ihre Einwilligung, auf die sich die Verarbeitung gemäß Art. 6 Abs. 1 Buchstabe a DS-GVO oder Art. 9 Abs. 2 Buchstabe a DS-GVO stützte, und es fehlt an einer anderweitigen Rechtsgrundlage für die Verarbeitung.</li><li>Die betroffene Person legt gemäß Art. 21 Abs. 1 DS-GVO Widerspruch gegen die Verarbeitung ein, und es liegen keine vorrangigen berechtigten Gründe für die Verarbeitung vor, oder die betroffene Person legt gemäß Art. 21 Abs. 2 DS-GVO Widerspruch gegen die Verarbeitung ein.</li><li>Die personenbezogenen Daten wurden unrechtmäßig verarbeitet.</li><li>Die Löschung der personenbezogenen Daten ist zur Erfüllung einer rechtlichen Verpflichtung nach dem Unionsrecht oder dem Recht der Mitgliedstaaten erforderlich, dem der Verantwortliche unterliegt.</li><li>Die personenbezogenen Daten wurden in Bezug auf angebotene Dienste der Informationsgesellschaft gemäß Art. 8 Abs. 1 DS-GVO erhoben.</li></ul><p>Sofern einer der oben genannten Gründe zutrifft und eine betroffene Person die Löschung von personenbezogenen Daten, die bei der Viabono GmbH gespeichert sind, veranlassen möchte, kann sie sich hierzu jederzeit an unseren Datenschutzbeauftragten oder einen anderen Mitarbeiter des für die Verarbeitung Verantwortlichen wenden. Der Datenschutzbeauftragte der Viabono GmbH oder ein anderer Mitarbeiter wird veranlassen, dass dem Löschverlangen unverzüglich nachgekommen wird.</p><p>Wurden die personenbezogenen Daten von der Viabono GmbH öffentlich gemacht und ist unser Unternehmen als Verantwortlicher gemäß Art. 17 Abs. 1 DS-GVO zur Löschung der personenbezogenen Daten verpflichtet, so trifft die Viabono GmbH unter Berücksichtigung der verfügbaren Technologie und der Implementierungskosten angemessene Maßnahmen, auch technischer Art, um andere für die Datenverarbeitung Verantwortliche, welche die veröffentlichten personenbezogenen Daten verarbeiten, darüber in Kenntnis zu setzen, dass die betroffene Person von diesen anderen für die Datenverarbeitung Verantwortlichen die Löschung sämtlicher Links zu diesen personenbezogenen Daten oder von Kopien oder Replikationen dieser personenbezogenen Daten verlangt hat, soweit die Verarbeitung nicht erforderlich ist. Der Datenschutzbeauftragte der Viabono GmbH oder ein anderer Mitarbeiter wird im Einzelfall das Notwendige veranlassen.</p></li><li><h4>e) Recht auf Einschränkung der Verarbeitung</h4><p>Jede von der Verarbeitung personenbezogener Daten betroffene Person hat das vom Europäischen Richtlinien- und Verordnungsgeber gewährte Recht, von dem Verantwortlichen die Einschränkung der Verarbeitung zu verlangen, wenn eine der folgenden Voraussetzungen gegeben ist:</p><ul><li>Die Richtigkeit der personenbezogenen Daten wird von der betroffenen Person bestritten, und zwar für eine Dauer, die es dem Verantwortlichen ermöglicht, die Richtigkeit der personenbezogenen Daten zu überprüfen.</li><li>Die Verarbeitung ist unrechtmäßig, die betroffene Person lehnt die Löschung der personenbezogenen Daten ab und verlangt stattdessen die Einschränkung der Nutzung der personenbezogenen Daten.</li><li>Der Verantwortliche benötigt die personenbezogenen Daten für die Zwecke der Verarbeitung nicht länger, die betroffene Person benötigt sie jedoch zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen.</li><li>Die betroffene Person hat Widerspruch gegen die Verarbeitung gem. Art. 21 Abs. 1 DS-GVO eingelegt und es steht noch nicht fest, ob die berechtigten Gründe des Verantwortlichen gegenüber denen der betroffenen Person überwiegen.</li></ul><p>Sofern eine der oben genannten Voraussetzungen gegeben ist und eine betroffene Person die Einschränkung von personenbezogenen Daten, die bei der Viabono GmbH gespeichert sind, verlangen möchte, kann sie sich hierzu jederzeit an unseren Datenschutzbeauftragten oder einen anderen Mitarbeiter des für die Verarbeitung Verantwortlichen wenden. Der Datenschutzbeauftragte der Viabono GmbH oder ein anderer Mitarbeiter wird die Einschränkung der Verarbeitung veranlassen.</p></li><li><h4>f) Recht auf Datenübertragbarkeit</h4><p>Jede von der Verarbeitung personenbezogener Daten betroffene Person hat das vom Europäischen Richtlinien- und Verordnungsgeber gewährte Recht, die sie betreffenden personenbezogenen Daten, welche durch die betroffene Person einem Verantwortlichen bereitgestellt wurden, in einem strukturierten, gängigen und maschinenlesbaren Format zu erhalten. Sie hat außerdem das Recht, diese Daten einem anderen Verantwortlichen ohne Behinderung durch den Verantwortlichen, dem die personenbezogenen Daten bereitgestellt wurden, zu übermitteln, sofern die Verarbeitung auf der Einwilligung gemäß Art. 6 Abs. 1 Buchstabe a DS-GVO oder Art. 9 Abs. 2 Buchstabe a DS-GVO oder auf einem Vertrag gemäß Art. 6 Abs. 1 Buchstabe b DS-GVO beruht und die Verarbeitung mithilfe automatisierter Verfahren erfolgt, sofern die Verarbeitung nicht für die Wahrnehmung einer Aufgabe erforderlich ist, die im öffentlichen Interesse liegt oder in Ausübung öffentlicher Gewalt erfolgt, welche dem Verantwortlichen übertragen wurde.</p><p>Ferner hat die betroffene Person bei der Ausübung ihres Rechts auf Datenübertragbarkeit gemäß Art. 20 Abs. 1 DS-GVO das Recht, zu erwirken, dass die personenbezogenen Daten direkt von einem Verantwortlichen an einen anderen Verantwortlichen übermittelt werden, soweit dies technisch machbar ist und sofern hiervon nicht die Rechte und Freiheiten anderer Personen beeinträchtigt werden.</p><p>Zur Geltendmachung des Rechts auf Datenübertragbarkeit kann sich die betroffene Person jederzeit an den von der Viabono GmbH bestellten Datenschutzbeauftragten oder einen anderen Mitarbeiter wenden.</p></li><li><h4>g) Recht auf Widerspruch</h4><p>Jede von der Verarbeitung personenbezogener Daten betroffene Person hat das vom Europäischen Richtlinien- und Verordnungsgeber gewährte Recht, aus Gründen, die sich aus ihrer besonderen Situation ergeben, jederzeit gegen die Verarbeitung sie betreffender personenbezogener Daten, die aufgrund von Art. 6 Abs. 1 Buchstaben e oder f DS-GVO erfolgt, Widerspruch einzulegen. Dies gilt auch für ein auf diese Bestimmungen gestütztes Profiling.</p><p>Die Viabono GmbH verarbeitet die personenbezogenen Daten im Falle des Widerspruchs nicht mehr, es sei denn, wir können zwingende schutzwürdige Gründe für die Verarbeitung nachweisen, die den Interessen, Rechten und Freiheiten der betroffenen Person überwiegen, oder die Verarbeitung dient der Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen.</p><p>Verarbeitet die Viabono GmbH personenbezogene Daten, um Direktwerbung zu betreiben, so hat die betroffene Person das Recht, jederzeit Widerspruch gegen die Verarbeitung der personenbezogenen Daten zum Zwecke derartiger Werbung einzulegen. Dies gilt auch für das Profiling, soweit es mit solcher Direktwerbung in Verbindung steht. Widerspricht die betroffene Person gegenüber der Viabono GmbH der Verarbeitung für Zwecke der Direktwerbung, so wird die Viabono GmbH die personenbezogenen Daten nicht mehr für diese Zwecke verarbeiten.</p><p>Zudem hat die betroffene Person das Recht, aus Gründen, die sich aus ihrer besonderen Situation ergeben, gegen die sie betreffende Verarbeitung personenbezogener Daten, die bei der Viabono GmbH zu wissenschaftlichen oder historischen Forschungszwecken oder zu statistischen Zwecken gemäß Art. 89 Abs. 1 DS-GVO erfolgen, Widerspruch einzulegen, es sei denn, eine solche Verarbeitung ist zur Erfüllung einer im öffentlichen Interesse liegenden Aufgabe erforderlich.</p><p>Zur Ausübung des Rechts auf Widerspruch kann sich die betroffene Person direkt an den Datenschutzbeauftragten der Viabono GmbH oder einen anderen Mitarbeiter wenden. Der betroffenen Person steht es ferner frei, im Zusammenhang mit der Nutzung von Diensten der Informationsgesellschaft, ungeachtet der Richtlinie 2002/58/EG, ihr Widerspruchsrecht mittels automatisierter Verfahren auszuüben, bei denen technische Spezifikationen verwendet werden.</p></li><li><h4>h) Automatisierte Entscheidungen im Einzelfall einschließlich Profiling</h4><p>Jede von der Verarbeitung personenbezogener Daten betroffene Person hat das vom Europäischen Richtlinien- und Verordnungsgeber gewährte Recht, nicht einer ausschließlich auf einer automatisierten Verarbeitung — einschließlich Profiling — beruhenden Entscheidung unterworfen zu werden, die ihr gegenüber rechtliche Wirkung entfaltet oder sie in ähnlicher Weise erheblich beeinträchtigt, sofern die Entscheidung (1) nicht für den Abschluss oder die Erfüllung eines Vertrags zwischen der betroffenen Person und dem Verantwortlichen erforderlich ist, oder (2) aufgrund von Rechtsvorschriften der Union oder der Mitgliedstaaten, denen der Verantwortliche unterliegt, zulässig ist und diese Rechtsvorschriften angemessene Maßnahmen zur Wahrung der Rechte und Freiheiten sowie der berechtigten Interessen der betroffenen Person enthalten oder (3) mit ausdrücklicher Einwilligung der betroffenen Person erfolgt.</p><p>Ist die Entscheidung (1) für den Abschluss oder die Erfüllung eines Vertrags zwischen der betroffenen Person und dem Verantwortlichen erforderlich oder (2) erfolgt sie mit ausdrücklicher Einwilligung der betroffenen Person, trifft die Viabono GmbH angemessene Maßnahmen, um die Rechte und Freiheiten sowie die berechtigten Interessen der betroffenen Person zu wahren, wozu mindestens das Recht auf Erwirkung des Eingreifens einer Person seitens des Verantwortlichen, auf Darlegung des eigenen Standpunkts und auf Anfechtung der Entscheidung gehört.</p><p>Möchte die betroffene Person Rechte mit Bezug auf automatisierte Entscheidungen geltend machen, kann sie sich hierzu jederzeit an unseren Datenschutzbeauftragten oder einen anderen Mitarbeiter des für die Verarbeitung Verantwortlichen wenden.</p></li><li><h4>i) Recht auf Widerruf einer datenschutzrechtlichen Einwilligung</h4><p>Jede von der Verarbeitung personenbezogener Daten betroffene Person hat das vom Europäischen Richtlinien- und Verordnungsgeber gewährte Recht, eine Einwilligung zur Verarbeitung personenbezogener Daten jederzeit zu widerrufen.</p><p>Möchte die betroffene Person ihr Recht auf Widerruf einer Einwilligung geltend machen, kann sie sich hierzu jederzeit an unseren Datenschutzbeauftragten oder einen anderen Mitarbeiter des für die Verarbeitung Verantwortlichen wenden.<br> \t&nbsp;</p></li></ul><h4>11. Datenschutzbestimmungen zu Einsatz und Verwendung von affilinet</h4><p>Der für die Verarbeitung Verantwortliche hat auf dieser Internetseite Komponenten des Unternehmens affilinet integriert. Affilinet ist ein deutsches Affiliate-Netzwerk, welches Affiliate-Marketing anbietet.</p><p>Affiliate-Marketing ist eine Internetgestützte Vertriebsform, die es kommerziellen Betreibern von Internetseiten, den sogenannten Merchants oder Advertisern, ermöglicht, Werbung, die meist über Klick- oder Sale-Provisionen vergütet wird, auf Internetseiten Dritter, also bei Vertriebspartnern, die auch Affiliates oder Publisher genannt werden, einzublenden. Der Merchant stellt über das Affiliate-Netzwerk ein Werbemittel, also einen Werbebanner oder andere geeignete Mittel der Internetwerbung, zur Verfügung, welche in der Folge von einem Affiliate auf eigenen Internetseiten eingebunden oder über sonstige Kanäle, wie etwa das Keyword-Advertising oder E-Mail-Marketing, beworben werden.</p><p>Betreibergesellschaft von Affilinet ist die affilinet GmbH, Sapporobogen 6-8, 80637 München, Deutschland.</p><p>Affilinet setzt ein Cookie auf dem informationstechnologischen System der betroffenen Person. Was Cookies sind, wurde oben bereits erläutert. Das Tracking-Cookie von Affilinet speichert keinerlei personenbezogene Daten. Gespeichert werden lediglich die Identifikationsnummer des Affiliate, also des den potentiellen Kunden vermittelnden Partners, sowie die Ordnungsnummer des Besuchers einer Internetseite und des angeklickten Werbemittels. Zweck der Speicherung dieser Daten ist die Abwicklung von Provisionszahlungen zwischen einem Merchant und dem Affiliate, welche über das Affiliate-Netzwerk, also Affilinet, abgewickelt werden.</p><p>Die betroffene Person kann die Setzung von Cookies durch unsere Internetseite, wie oben bereits dargestellt, jederzeit mittels einer entsprechenden Einstellung des genutzten Internetbrowsers verhindern und damit der Setzung von Cookies dauerhaft widersprechen. Eine solche Einstellung des genutzten Internetbrowsers würde auch verhindern, dass Affilinet ein Cookie auf dem informationstechnologischen System der betroffenen Person setzt. Zudem können von Affilinet bereits gesetzte Cookies jederzeit über einen Internetbrowser oder andere Softwareprogramme gelöscht werden.</p><p>Die geltenden Datenschutzbestimmungen von Affilinet können unter <a href=\"https://www.affili.net/de/footeritem/datenschutz\" target=\"_blank\">www.affili.net/de/footeritem/datenschutz</a> abgerufen werden.<br> &nbsp;</p><h4>12. Datenschutzbestimmungen zu Einsatz und Verwendung von Google Analytics (mit Anonymisierungsfunktion)</h4><p>Der für die Verarbeitung Verantwortliche hat auf dieser Internetseite die Komponente Google Analytics (mit Anonymisierungsfunktion) integriert. Google Analytics ist ein Web-Analyse-Dienst. Web-Analyse ist die Erhebung, Sammlung und Auswertung von Daten über das Verhalten von Besuchern von Internetseiten. Ein Web-Analyse-Dienst erfasst unter anderem Daten darüber, von welcher Internetseite eine betroffene Person auf eine Internetseite gekommen ist (sogenannte Referrer), auf welche Unterseiten der Internetseite zugegriffen oder wie oft und für welche Verweildauer eine Unterseite betrachtet wurde. Eine Web-Analyse wird überwiegend zur Optimierung einer Internetseite und zur Kosten-Nutzen-Analyse von Internetwerbung eingesetzt.</p><p>Betreibergesellschaft der Google-Analytics-Komponente ist die Google Inc., 1600 Amphitheatre Pkwy, Mountain View, CA 94043-1351, USA.</p><p>Der für die Verarbeitung Verantwortliche verwendet für die Web-Analyse über Google Analytics den Zusatz \"_gat._anonymizeIp\". Mittels dieses Zusatzes wird die IP-Adresse des Internetanschlusses der betroffenen Person von Google gekürzt und anonymisiert, wenn der Zugriff auf unsere Internetseiten aus einem Mitgliedstaat der Europäischen Union oder aus einem anderen Vertragsstaat des Abkommens über den Europäischen Wirtschaftsraum erfolgt.</p><p>Der Zweck der Google-Analytics-Komponente ist die Analyse der Besucherströme auf unserer Internetseite. Google nutzt die gewonnenen Daten und Informationen unter anderem dazu, die Nutzung unserer Internetseite auszuwerten, um für uns Online-Reports, welche die Aktivitäten auf unseren Internetseiten aufzeigen, zusammenzustellen, und um weitere mit der Nutzung unserer Internetseite in Verbindung stehende Dienstleistungen zu erbringen.</p><p>Google Analytics setzt ein Cookie auf dem informationstechnologischen System der betroffenen Person. Was Cookies sind, wurde oben bereits erläutert. Mit Setzung des Cookies wird Google eine Analyse der Benutzung unserer Internetseite ermöglicht. Durch jeden Aufruf einer der Einzelseiten dieser Internetseite, die durch den für die Verarbeitung Verantwortlichen betrieben wird und auf welcher eine Google-Analytics-Komponente integriert wurde, wird der Internetbrowser auf dem informationstechnologischen System der betroffenen Person automatisch durch die jeweilige Google-Analytics-Komponente veranlasst, Daten zum Zwecke der Online-Analyse an Google zu übermitteln. Im Rahmen dieses technischen Verfahrens erhält Google Kenntnis über personenbezogene Daten, wie der IP-Adresse der betroffenen Person, die Google unter anderem dazu dienen, die Herkunft der Besucher und Klicks nachzuvollziehen und in der Folge Provisionsabrechnungen zu ermöglichen.</p><p>Mittels des Cookies werden personenbezogene Informationen, beispielsweise die Zugriffszeit, der Ort, von welchem ein Zugriff ausging und die Häufigkeit der Besuche unserer Internetseite durch die betroffene Person, gespeichert. Bei jedem Besuch unserer Internetseiten werden diese personenbezogenen Daten, einschließlich der IP-Adresse des von der betroffenen Person genutzten Internetanschlusses, an Google in den Vereinigten Staaten von Amerika übertragen. Diese personenbezogenen Daten werden durch Google in den Vereinigten Staaten von Amerika gespeichert. Google gibt diese über das technische Verfahren erhobenen personenbezogenen Daten unter Umständen an Dritte weiter.</p><p>Die betroffene Person kann die Setzung von Cookies durch unsere Internetseite, wie oben bereits dargestellt, jederzeit mittels einer entsprechenden Einstellung des genutzten Internetbrowsers verhindern und damit der Setzung von Cookies dauerhaft widersprechen. Eine solche Einstellung des genutzten Internetbrowsers würde auch verhindern, dass Google ein Cookie auf dem informationstechnologischen System der betroffenen Person setzt. Zudem kann ein von Google Analytics bereits gesetzter Cookie jederzeit über den Internetbrowser oder andere Softwareprogramme gelöscht werden.</p><p>Ferner besteht für die betroffene Person die Möglichkeit, einer Erfassung der durch Google Analytics erzeugten, auf eine Nutzung dieser Internetseite bezogenen Daten sowie der Verarbeitung dieser Daten durch Google zu widersprechen und eine solche zu verhindern. Hierzu muss die betroffene Person ein Browser-Add-On unter dem Link <a href=\"https://tools.google.com/dlpage/gaoptout\" target=\"_blank\">tools.google.com/dlpage/gaoptout</a> herunterladen und installieren. Dieses Browser-Add-On teilt Google Analytics über JavaScript mit, dass keine Daten und Informationen zu den Besuchen von Internetseiten an Google Analytics übermittelt werden dürfen. Die Installation des Browser-Add-Ons wird von Google als Widerspruch gewertet. Wird das informationstechnologische System der betroffenen Person zu einem späteren Zeitpunkt gelöscht, formatiert oder neu installiert, muss durch die betroffene Person eine erneute Installation des Browser-Add-Ons erfolgen, um Google Analytics zu deaktivieren. Sofern das Browser-Add-On durch die betroffene Person oder einer anderen Person, die ihrem Machtbereich zuzurechnen ist, deinstalliert oder deaktiviert wird, besteht die Möglichkeit der Neuinstallation oder der erneuten Aktivierung des Browser-Add-Ons.</p><p>Weitere Informationen und die geltenden Datenschutzbestimmungen von Google können unter <a href=\"https://www.google.de/intl/de/policies/privacy/\" target=\"_blank\">www.google.de/intl/de/policies/privacy/</a> und unter <a href=\"http://www.google.com/analytics/terms/de.html\" target=\"_blank\">www.google.com/analytics/terms/de.html</a> abgerufen werden. Google Analytics wird unter diesem Link <a href=\"https://www.google.com/intl/de_de/analytics/\" target=\"_blank\">www.google.com/intl/de_de/analytics/</a> genauer erläutert.<br> &nbsp;</p><h4>13. Rechtsgrundlage der Verarbeitung</h4><p>Art. 6 I lit. a DS-GVO dient unserem Unternehmen als Rechtsgrundlage für Verarbeitungsvorgänge, bei denen wir eine Einwilligung für einen bestimmten Verarbeitungszweck einholen. Ist die Verarbeitung personenbezogener Daten zur Erfüllung eines Vertrags, dessen Vertragspartei die betroffene Person ist, erforderlich, wie dies beispielsweise bei Verarbeitungsvorgängen der Fall ist, die für eine Lieferung von Waren oder die Erbringung einer sonstigen Leistung oder Gegenleistung notwendig sind, so beruht die Verarbeitung auf Art. 6 I lit. b DS-GVO. Gleiches gilt für solche Verarbeitungsvorgänge die zur Durchführung vorvertraglicher Maßnahmen erforderlich sind, etwa in Fällen von Anfragen zur unseren Produkten oder Leistungen. Unterliegt unser Unternehmen einer rechtlichen Verpflichtung durch welche eine Verarbeitung von personenbezogenen Daten erforderlich wird, wie beispielsweise zur Erfüllung steuerlicher Pflichten, so basiert die Verarbeitung auf Art. 6 I lit. c DS-GVO. In seltenen Fällen könnte die Verarbeitung von personenbezogenen Daten erforderlich werden, um lebenswichtige Interessen der betroffenen Person oder einer anderen natürlichen Person zu schützen. Dies wäre beispielsweise der Fall, wenn ein Besucher in unserem Betrieb verletzt werden würde und daraufhin sein Name, sein Alter, seine Krankenkassendaten oder sonstige lebenswichtige Informationen an einen Arzt, ein Krankenhaus oder sonstige Dritte weitergegeben werden müssten. Dann würde die Verarbeitung auf Art. 6 I lit. d DS-GVO beruhen. Letztlich könnten Verarbeitungsvorgänge auf Art. 6 I lit. f DS-GVO beruhen. Auf dieser Rechtsgrundlage basieren Verarbeitungsvorgänge, die von keiner der vorgenannten Rechtsgrundlagen erfasst werden, wenn die Verarbeitung zur Wahrung eines berechtigten Interesses unseres Unternehmens oder eines Dritten erforderlich ist, sofern die Interessen, Grundrechte und Grundfreiheiten des Betroffenen nicht überwiegen. Solche Verarbeitungsvorgänge sind uns insbesondere deshalb gestattet, weil sie durch den Europäischen Gesetzgeber besonders erwähnt wurden. Er vertrat insoweit die Auffassung, dass ein berechtigtes Interesse anzunehmen sein könnte, wenn die betroffene Person ein Kunde des Verantwortlichen ist (Erwägungsgrund 47 Satz 2 DS-GVO).<br> &nbsp;</p><h4>14. Berechtigte Interessen an der Verarbeitung, die von dem Verantwortlichen oder einem Dritten verfolgt werden</h4><p>Basiert die Verarbeitung personenbezogener Daten auf Artikel 6 I lit. f DS-GVO ist unser berechtigtes Interesse die Durchführung unserer Geschäftstätigkeit zugunsten des Wohlergehens all unserer Mitarbeiter und unserer Anteilseigner.<br> &nbsp;</p><h4>15. Dauer, für die die personenbezogenen Daten gespeichert werden</h4><p>Das Kriterium für die Dauer der Speicherung von personenbezogenen Daten ist die jeweilige gesetzliche Aufbewahrungsfrist. Nach Ablauf der Frist werden die entsprechenden Daten routinemäßig gelöscht, sofern sie nicht mehr zur Vertragserfüllung oder Vertragsanbahnung erforderlich sind.<br> &nbsp;</p><h4>16. Gesetzliche oder vertragliche Vorschriften zur Bereitstellung der personenbezogenen Daten; Erforderlichkeit für den Vertragsabschluss; Verpflichtung der betroffenen Person, die personenbezogenen Daten bereitzustellen; mögliche Folgen der Nichtbereitstellung</h4><p>Wir klären Sie darüber auf, dass die Bereitstellung personenbezogener Daten zum Teil gesetzlich vorgeschrieben ist (z.B. Steuervorschriften) oder sich auch aus vertraglichen Regelungen (z.B. Angaben zum Vertragspartner) ergeben kann. Mitunter kann es zu einem Vertragsschluss erforderlich sein, dass eine betroffene Person uns personenbezogene Daten zur Verfügung stellt, die in der Folge durch uns verarbeitet werden müssen. Die betroffene Person ist beispielsweise verpflichtet uns personenbezogene Daten bereitzustellen, wenn unser Unternehmen mit ihr einen Vertrag abschließt. Eine Nichtbereitstellung der personenbezogenen Daten hätte zur Folge, dass der Vertrag mit dem Betroffenen nicht geschlossen werden könnte. Vor einer Bereitstellung personenbezogener Daten durch den Betroffenen muss sich der Betroffene an unseren Datenschutzbeauftragten wenden. Unser Datenschutzbeauftragter klärt den Betroffenen einzelfallbezogen darüber auf, ob die Bereitstellung der personenbezogenen Daten gesetzlich oder vertraglich vorgeschrieben oder für den Vertragsabschluss erforderlich ist, ob eine Verpflichtung besteht, die personenbezogenen Daten bereitzustellen, und welche Folgen die Nichtbereitstellung der personenbezogenen Daten hätte.<br> &nbsp;</p><h4>17. Bestehen einer automatisierten Entscheidungsfindung</h4><p>Als verantwortungsbewusstes Unternehmen verzichten wir auf eine automatische Entscheidungsfindung oder ein Profiling.<br> &nbsp;</p><p>&nbsp;</p><p>Diese Datenschutzerklärung wurde durch den Datenschutzerklärungs-Generator von den Datenschutz Nürnberg in Kooperation mit der RC GmbH, die gebrauchte Notebooks wiederverwertet und den Filesharing Rechtsanwälten von WBS-LAW erstellt.</p><p>&nbsp;</p></div>"

/***/ }),

/***/ "./src/app/datenschutz/datenschutz.component.ts":
/*!******************************************************!*\
  !*** ./src/app/datenschutz/datenschutz.component.ts ***!
  \******************************************************/
/*! exports provided: DatenschutzComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DatenschutzComponent", function() { return DatenschutzComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var DatenschutzComponent = /** @class */ (function () {
    function DatenschutzComponent() {
    }
    DatenschutzComponent.prototype.ngOnInit = function () {
    };
    DatenschutzComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-datenschutz',
            template: __webpack_require__(/*! ./datenschutz.component.html */ "./src/app/datenschutz/datenschutz.component.html"),
            styles: [__webpack_require__(/*! ./datenschutz.component.css */ "./src/app/datenschutz/datenschutz.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], DatenschutzComponent);
    return DatenschutzComponent;
}());



/***/ }),

/***/ "./src/app/eingabe/eingabe.component.css":
/*!***********************************************!*\
  !*** ./src/app/eingabe/eingabe.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2VpbmdhYmUvZWluZ2FiZS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/eingabe/eingabe.component.html":
/*!************************************************!*\
  !*** ./src/app/eingabe/eingabe.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "    \r\n    <mat-horizontal-stepper (selectionChange)=\"saveEverything()\" class=\"container jumbotron\" #stepper>\r\n        <ng-template matStepLabel matStepperIcon=\"done\">\r\n            <mat-icon>domain</mat-icon>\r\n          Kontaktdaten\r\n        </ng-template>\r\n              <mat-step [stepControl]=\"zeroFormGroup\">\r\n                  <ng-template matStepLabel>\r\n                      <mat-icon>play_arrow</mat-icon>\r\n                      <span class=\"align-middle\"> Start</span>\r\n                  </ng-template>\r\n                  <!-- STEP 0 -->\r\n                  <!-- INTRO -->\r\n                  <form [formGroup]=\"zeroFormGroup\">                      \r\n                        <!-- so wird den Children die Events saveKunde & saveAdressen zugänglich gemacht -->\r\n                        <app-intro></app-intro>\r\n                    <div class=\"d-flex justify-content-center p-4\">\r\n                      <button class=\"btn btn-success mx-3 align-center\"  matStepperNext *ngIf=\"zeroFormGroup.valid\">weiter</button>\r\n                    </div>\r\n                  </form>\r\n                </mat-step>\r\n                <!-- STEP 1 -->\r\n                <!-- Kontaktdaten -->\r\n                <mat-step [stepControl]=\"firstFormGroup\">\r\n                    <ng-template matStepLabel>\r\n                        <mat-icon>perm_contact_calendar</mat-icon>\r\n                        <span class=\"align-middle\"> Kontakt</span>\r\n                    </ng-template>\r\n\r\n                  <form [formGroup]=\"firstFormGroup\">                      \r\n                        <!-- so wird den Children die Events saveKunde & saveAdressen zugänglich gemacht -->\r\n                        <app-kundeneingabe [saveKunde]=\"saveKunde\" [saveAdressen]=\"saveAdressen\" [pruefeEingabe]=\"pruefeEingabe\"></app-kundeneingabe>\r\n                    <div class=\"d-flex justify-content-center p-4\">\r\n                      <button class=\"btn btn-success mx-3 align-center\"  matStepperNext *ngIf=\"firstFormGroup.valid\">weiter</button>\r\n                    </div>\r\n                  </form>\r\n                </mat-step>\r\n                <!-- STEP 2 -->\r\n                <!-- Betriebsdaten -->\r\n                <mat-step [stepControl]=\"secondFormGroup\">\r\n                  <form [formGroup]=\"secondFormGroup\">\r\n                      <ng-template matStepLabel matStepperIcon=\"done\">\r\n                          <mat-icon>hotel</mat-icon>\r\n                          <span class=\"align-middle\"> Betrieb</span>                        \r\n                      </ng-template>\r\n                        <!-- Save Events im HTML:-->\r\n                        <app-betriebsdaten [saveBetriebsdaten]=\"saveBetriebsdaten\"  [pruefeEingabe]=\"pruefeEingabe\"></app-betriebsdaten>                   \r\n                    <div class=\"d-flex justify-content-center p-4\">\r\n                      <button class=\"btn btn-success mx-3 align-center\" matStepperPrevious>zurück</button>\r\n                      <button class=\"btn btn-success mx-3 align-center\" matStepperNext>weiter</button>\r\n                    </div>\r\n                  </form>\r\n                </mat-step>\r\n                <!-- STEP 3 -->\r\n                <!-- Wasser -->\r\n                <mat-step [stepControl]=\"thirdFormGroup\">\r\n                        <form [formGroup]=\"thirdFormGroup\">\r\n                            <ng-template matStepLabel matStepperIcon=\"done\">\r\n                                <mat-icon>waves</mat-icon>\r\n                                <span class=\"align-middle\"> Wasser</span>\r\n                            </ng-template>\r\n                              <!-- Save Events im HTML:-->\r\n                              <app-wasser [saveWasser]=\"saveWasser\" [pruefeEingabe]=\"pruefeEingabe\"></app-wasser>                   \r\n                          <div class=\"d-flex justify-content-center p-4\">\r\n                            <button class=\"btn btn-success mx-3 align-center\" matStepperPrevious>zurück</button>\r\n                            <button class=\"btn btn-success mx-3 align-center\" matStepperNext>weiter</button>\r\n                          </div>\r\n                        </form>\r\n                </mat-step>    \r\n                <!-- STEP 4 -->\r\n                <!-- Strom -->\r\n                <mat-step [stepControl]=\"fourthFormGroup\">\r\n                        <form [formGroup]=\"fourthFormGroup\">\r\n                            <ng-template matStepLabel matStepperIcon=\"done\">\r\n                                <mat-icon>offline_bolt</mat-icon>\r\n                                <span class=\"align-middle\"> Strom</span>\r\n                            </ng-template>\r\n                              <!-- Save Events im HTML:-->\r\n                              <app-strom [saveStrom]=\"saveStrom\" [pruefeEingabe]=\"pruefeEingabe\"></app-strom>                   \r\n                          <div class=\"d-flex justify-content-center p-4\">\r\n                            <button class=\"btn btn-success mx-3 align-center\" matStepperPrevious>zurück</button>\r\n                            <button class=\"btn btn-success mx-3 align-center\" matStepperNext>weiter</button>\r\n                          </div>\r\n                        </form>\r\n                </mat-step>    \r\n                <!-- STEP 5 -->\r\n                <!-- Heizung und Anlagen -->\r\n                <mat-step [stepControl]=\"fifthFormGroup\">\r\n                        <form [formGroup]=\"fifthFormGroup\">\r\n                            <ng-template matStepLabel matStepperIcon=\"done\">\r\n                                <mat-icon>whatshot</mat-icon>\r\n                                <span class=\"align-middle\"> Heiz-/Anlagen</span>\r\n                            </ng-template>\r\n                              <!-- Save Events im HTML:-->\r\n                              <app-strom-heizung [saveStromHeizung]=\"saveStromHeizung\" [pruefeEingabe]=\"pruefeEingabe\"></app-strom-heizung>                   \r\n                          <div class=\"d-flex justify-content-center p-4\">\r\n                            <button class=\"btn btn-success mx-3 align-center\" matStepperPrevious>zurück</button>\r\n                            <button class=\"btn btn-success mx-3 align-center\" matStepperNext>weiter</button>\r\n                          </div>\r\n                        </form>\r\n                </mat-step>    \r\n                <!-- STEP 6 -->\r\n                <!-- Abfälle -->\r\n                 <mat-step [stepControl]=\"sixthFormGroup\">\r\n                        <form [formGroup]=\"sixthFormGroup\">\r\n                            <ng-template matStepLabel matStepperIcon=\"done\">\r\n                                <mat-icon>delete</mat-icon>\r\n                                <span class=\"align-middle\"> Abfall</span>\r\n                            </ng-template>\r\n                              <!-- Save Events im HTML:-->\r\n                              <app-restabfall [saveRestabfalltonnen]=\"saveRestabfalltonnen\" [pruefeEingabe]=\"pruefeEingabe\"></app-restabfall>                   \r\n                          <div class=\"d-flex justify-content-center p-4\">\r\n                            <button class=\"btn btn-success mx-3 align-center\" matStepperPrevious>zurück</button>\r\n                            <button class=\"btn btn-success mx-3 align-center\" matStepperNext>weiter</button>\r\n                          </div>\r\n                        </form>\r\n                </mat-step>\r\n                <!-- STEP 7 -->\r\n                <!-- Eingabecheck -->\r\n                <mat-step [stepControl]=\"seventhFormGroup\">\r\n                    <form [formGroup]=\"seventhFormGroup\">\r\n                        <ng-template matStepLabel matStepperIcon=\"done\">\r\n                            <mat-icon>cloud_upload</mat-icon>\r\n                            <span class=\"align-middle\"> Abgabe</span>\r\n                        </ng-template>\r\n                        <!-- Save Events im HTML:-->\r\n                        <app-pruefung [pruefeEingabe]=\"pruefeEingabe\"></app-pruefung>                   \r\n                    <div class=\"d-flex justify-content-center p-4\">\r\n                      <button class=\"btn btn-success mx-3 align-center\" matStepperPrevious>zurück</button>\r\n                      <button class=\"btn btn-success mx-3 align-center\" matStepperNext>weiter</button>\r\n                    </div>\r\n                  </form>\r\n                </mat-step>\r\n        </mat-horizontal-stepper>\r\n<app-footer></app-footer>\r\n  "

/***/ }),

/***/ "./src/app/eingabe/eingabe.component.ts":
/*!**********************************************!*\
  !*** ./src/app/eingabe/eingabe.component.ts ***!
  \**********************************************/
/*! exports provided: EingabeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EingabeComponent", function() { return EingabeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material_stepper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/stepper */ "./node_modules/@angular/material/esm5/stepper.es5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_data_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../_services/data.service */ "./src/app/_services/data.service.ts");
/* harmony import */ var _services_pruefservice_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../_services/pruefservice.service */ "./src/app/_services/pruefservice.service.ts");







var EingabeComponent = /** @class */ (function () {
    function EingabeComponent(_formBuilder, Dataservice, Pruefservice) {
        this._formBuilder = _formBuilder;
        this.Dataservice = Dataservice;
        this.Pruefservice = Pruefservice;
        //wenn man auf next klickt, haut die Eingabekomponente die Events raus:
        this.saveKunde = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.saveAdressen = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.saveBetriebsdaten = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.saveStromHeizung = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.saveWasser = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.saveStrom = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.saveRestabfalltonnen = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.abgabeQuickcheckUmwelt = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.pruefeEingabe = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.zeroFormGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({});
        this.firstFormGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({});
        this.secondFormGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({});
        this.thirdFormGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({});
        this.fourthFormGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({});
        this.fifthFormGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({});
        this.sixthFormGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({});
        this.seventhFormGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({});
        this.abgabeFormGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({});
    }
    EingabeComponent.prototype.saveEverything = function () {
        console.log("SaveEverything: StepperIndex:", this.stepper.selectedIndex);
        console.log("pruefung: ResetPruefService");
        this.Pruefservice.ResetPruefungService();
        this.pruefeEingabe.emit();
        switch (this.stepper.selectedIndex) {
            //Intro
            case 0: {
                break;
            }
            //Kontaktdaten: Rechnungsadresse & Co 
            case 1: {
                console.log("Eingabe: emit saveKunde & saveAdressen; case 1");
                this.saveKunde.emit();
                this.saveAdressen.emit();
                break;
            }
            //Betriebsdaten
            case 2: {
                console.log("Eingabe: emit saveBetriebsdaten->Einrichtung usw.");
                this.saveBetriebsdaten.emit();
                break;
            }
            //Wasser
            case 3: {
                console.log("Eingabe: emit saveWasser");
                this.saveWasser.emit();
                break;
            }
            //Strom
            case 4: {
                console.log("Eingabe: emit saveStrom");
                this.saveStrom.emit();
                break;
            }
            //Heizung und Anlagen (strom-Heizung)
            case 5: {
                console.log("Eingabe: emit saveStromHeizung");
                this.saveStromHeizung.emit();
                break;
            }
            //Restabfall
            case 6: {
                console.log("Eingabe: emit saveRestabfalltonnen");
                this.saveRestabfalltonnen.emit();
                break;
            }
            //Pruefseite  
            case 7: {
                break;
            }
        }
    };
    EingabeComponent.prototype.ngOnInit = function () {
        this.firstFormGroup = this._formBuilder.group({});
        this.secondFormGroup = this._formBuilder.group({});
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], EingabeComponent.prototype, "saveKunde", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], EingabeComponent.prototype, "saveAdressen", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], EingabeComponent.prototype, "saveBetriebsdaten", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], EingabeComponent.prototype, "saveStromHeizung", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], EingabeComponent.prototype, "saveWasser", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], EingabeComponent.prototype, "saveStrom", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], EingabeComponent.prototype, "saveRestabfalltonnen", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], EingabeComponent.prototype, "abgabeQuickcheckUmwelt", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], EingabeComponent.prototype, "pruefeEingabe", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('stepper'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material_stepper__WEBPACK_IMPORTED_MODULE_2__["MatStepper"])
    ], EingabeComponent.prototype, "stepper", void 0);
    EingabeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-eingabe',
            template: __webpack_require__(/*! ./eingabe.component.html */ "./src/app/eingabe/eingabe.component.html"),
            styles: [__webpack_require__(/*! ./eingabe.component.css */ "./src/app/eingabe/eingabe.component.css")]
        }),
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root',
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"], _services_data_service__WEBPACK_IMPORTED_MODULE_4__["DataService"], _services_pruefservice_service__WEBPACK_IMPORTED_MODULE_5__["PruefService"]])
    ], EingabeComponent);
    return EingabeComponent;
}());



/***/ }),

/***/ "./src/app/footer/footer.component.css":
/*!*********************************************!*\
  !*** ./src/app/footer/footer.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Zvb3Rlci9mb290ZXIuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/footer/footer.component.html":
/*!**********************************************!*\
  !*** ./src/app/footer/footer.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- EXPLORE SECTION -->\r\n<section id=\"explore-section\" class=\"bg-light text-muted py-5\">\r\n    <div class=\"container\">\r\n      <div class=\"row\">      \r\n        <div class=\"col-md-6\">\r\n            <h3>Partner:</h3> \r\n          <div class=\"d-flex flex-column p-3\">\r\n              <img src=\"/assets/img/logo_Dehoga.png\" style=\"max-width:40%\" class=\"float-left\">\r\n          </div>\r\n          <div class=\"d-flex flex-column p-3\">\r\n              <img src=\"/assets/img/Bett+Bike_Logo_farbig_(4c).png\" style=\"max-width:40%\" class=\"float-left\">\r\n          </div>\r\n          <div class=\"d-flex flex-column p-3\">\r\n              <img src=\"/assets/img/Qualitaetsgastgeber-allgemein.jpg\" style=\"max-width:40%\" class=\"float-left\">\r\n          </div>\r\n  \r\n        </div>\r\n        <div class=\"col-md-6\">\r\n          <h3>Durchgeführt von:</h3>\r\n          <p></p>\r\n          <img src=\"/assets/img/logo_claim.jpg\" style=\"max-width:40%\" class=\"\">\r\n          <div class=\"py-4\">\r\n              <p>Viabono GmbH </p>\r\n              <p> Hauptstr. 240 </p>\r\n              <p>51503 Rösrath-Hoffnungsthal </p>\r\n              <p>Tel: 02205 – 9198350</p>\r\n              <p>Fax: 02205 – 9198355</p>\r\n              <p>E-Mail: info​[at]​viabono.de</p>\r\n              <p>Geschäftsführer|Inhaltlich Verantwortlicher: Helge Beißert-Riegel </p>\r\n              <p>Handelsregisternummer: HRB 47486</p>\r\n              <p>Amtsgericht Köln</p>\r\n              <p>Umsatzsteuer-Identifikationsnummer: DE248938948</p>\r\n              <p>Copyright Foto: Berghotel Rehlegg</p>\r\n              <p></p>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </section>\r\n  \r\n  <!-- FOOTER -->\r\n  <footer id=\"main-footer\" class=\"bg-dark\">\r\n    <div class=\"container\">\r\n      <div class=\"row\">\r\n        <div class=\"col py-2 bg-success\">\r\n          <p class=\"align-middle\">Copyright &copy;\r\n            <span id=\"year\">2018</span>\r\n          </p>\r\n        </div>\r\n        <div class=\"col py-2 bg-success\">           \r\n          <a href=\"/datenschutz\" class=\"btn btn-link\">Datenschutzerklärung</a>\r\n          <a href=\"/impressum\" class=\"btn btn-link\">Impressum</a>\r\n          <button class=\"btn btn-link \">Kontakt</button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </footer>\r\n    \r\n  \r\n  <script src=\"http://code.jquery.com/jquery-3.3.1.min.js\" integrity=\"sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=\"\r\n    crossorigin=\"anonymous\"></script>\r\n  <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js\" integrity=\"sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49\"\r\n    crossorigin=\"anonymous\"></script>\r\n  <script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js\" integrity=\"sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T\"\r\n    crossorigin=\"anonymous\"></script>"

/***/ }),

/***/ "./src/app/footer/footer.component.ts":
/*!********************************************!*\
  !*** ./src/app/footer/footer.component.ts ***!
  \********************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var FooterComponent = /** @class */ (function () {
    function FooterComponent() {
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__(/*! ./footer.component.html */ "./src/app/footer/footer.component.html"),
            styles: [__webpack_require__(/*! ./footer.component.css */ "./src/app/footer/footer.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "./src/app/home/home.component.html":
/*!******************************************!*\
  !*** ./src/app/home/home.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"col-md-6 col-md-offset-3\">\r\n    <h1>Hi {{currentUser.firstName}}!</h1>\r\n    <p>You're logged in with Angular 2/5 and Angular CLI!!</p>\r\n    <h3>All registered users:</h3>\r\n    <ul>\r\n        <li *ngFor=\"let user of users\">\r\n            {{user.username}} ({{user.firstName}} {{user.lastName}})\r\n            - <a (click)=\"deleteUser(user.id)\">Delete</a>\r\n        </li>\r\n    </ul>\r\n    <p><a [routerLink]=\"['/login']\">Logout</a></p>\r\n</div>"

/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_index__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../_services/index */ "./src/app/_services/index.ts");



var HomeComponent = /** @class */ (function () {
    function HomeComponent(userService) {
        this.userService = userService;
        this.users = [];
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }
    HomeComponent.prototype.ngOnInit = function () {
        this.loadAllUsers();
    };
    HomeComponent.prototype.deleteUser = function (id) {
        // this.userService.delete(id).subscribe(() => { this.loadAllUsers() });
    };
    HomeComponent.prototype.loadAllUsers = function () {
        // this.userService.getAll().subscribe(users => { this.users = users; });
    };
    HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/home/home.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_index__WEBPACK_IMPORTED_MODULE_2__["UserService"]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/home/index.ts":
/*!*******************************!*\
  !*** ./src/app/home/index.ts ***!
  \*******************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _home_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./home.component */ "./src/app/home/home.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return _home_component__WEBPACK_IMPORTED_MODULE_0__["HomeComponent"]; });




/***/ }),

/***/ "./src/app/impressum/impressum.component.css":
/*!***************************************************!*\
  !*** ./src/app/impressum/impressum.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ltcHJlc3N1bS9pbXByZXNzdW0uY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/impressum/impressum.component.html":
/*!****************************************************!*\
  !*** ./src/app/impressum/impressum.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"col-sm-12\" id=\"impressum\">\r\n    <app-footer></app-footer>\r\n</div>"

/***/ }),

/***/ "./src/app/impressum/impressum.component.ts":
/*!**************************************************!*\
  !*** ./src/app/impressum/impressum.component.ts ***!
  \**************************************************/
/*! exports provided: ImpressumComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImpressumComponent", function() { return ImpressumComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ImpressumComponent = /** @class */ (function () {
    function ImpressumComponent() {
    }
    ImpressumComponent.prototype.ngOnInit = function () {
    };
    ImpressumComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-impressum',
            template: __webpack_require__(/*! ./impressum.component.html */ "./src/app/impressum/impressum.component.html"),
            styles: [__webpack_require__(/*! ./impressum.component.css */ "./src/app/impressum/impressum.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ImpressumComponent);
    return ImpressumComponent;
}());



/***/ }),

/***/ "./src/app/intro/intro.component.css":
/*!*******************************************!*\
  !*** ./src/app/intro/intro.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ludHJvL2ludHJvLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/intro/intro.component.html":
/*!********************************************!*\
  !*** ./src/app/intro/intro.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h4>Herzlich willkommen zum Quick Check Umwelt</h4>\r\n\r\n<p>Wir freuen uns, dass Sie sich dazu entschieden haben, den Quick Check Umwelt zur Ermittlung Ihrer Umwelt-/ Klima- bzw. Verbrauchskennzahlen einzusetzen.</p>\r\n\r\n<p>Neben den betriebsspezifische Verbrauchskennzahlen, welche aus den von Ihnen getätigten Eingaben errechnet werden, zeigt Ihnen der Quick Check Umwelt, die Kennzahlen vergleichbarer Beherbergungsbetriebe auf.</p>\r\n\r\n<p>Bitte tragen Sie Ihre betriebsspezifische Daten in die nachfolgenden Felder ein. Durch den Eingabecheck am Ende der Befragung erhalten Sie eine Rückmeldung, welche Angaben ggf. noch hinzugefügt oder korrigiert werden müssen.</p>"

/***/ }),

/***/ "./src/app/intro/intro.component.ts":
/*!******************************************!*\
  !*** ./src/app/intro/intro.component.ts ***!
  \******************************************/
/*! exports provided: IntroComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IntroComponent", function() { return IntroComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var IntroComponent = /** @class */ (function () {
    function IntroComponent() {
    }
    IntroComponent.prototype.ngOnInit = function () {
    };
    IntroComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-intro',
            template: __webpack_require__(/*! ./intro.component.html */ "./src/app/intro/intro.component.html"),
            styles: [__webpack_require__(/*! ./intro.component.css */ "./src/app/intro/intro.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], IntroComponent);
    return IntroComponent;
}());



/***/ }),

/***/ "./src/app/kundeneingabe/kundeneingabe.component.css":
/*!***********************************************************!*\
  !*** ./src/app/kundeneingabe/kundeneingabe.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2t1bmRlbmVpbmdhYmUva3VuZGVuZWluZ2FiZS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/kundeneingabe/kundeneingabe.component.html":
/*!************************************************************!*\
  !*** ./src/app/kundeneingabe/kundeneingabe.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n  <h4 class=\"d-block mx-auto\">Bitte geben Sie Ihre Kontaktdaten ein:</h4>      \r\n    <div class=\"row\">     \r\n        <div class=\"col-lg-4 card my-2 py-2\">       \r\n            <h6 class=\"card-title\">Firmenadresse:</h6>     \r\n            <form class=\"form card-body\" [formGroup]=\"FirmenAddressFormGroup\">\r\n                    <mat-form-field class=\"full-width\">\r\n                            <input [errorStateMatcher]=\"matcher\" matInput required minlength=3 placeholder=\"Firmenname\" formControlName=\"name\" [formControl]=\"FirmenAddressFormGroup.controls.name\">\r\n                            <mat-icon *ngIf=\"FirmenAddressFormGroup.controls.name.valid\" class=\"success-icon\" matSuffix >done</mat-icon>\r\n                    </mat-form-field>\r\n                    <mat-form-field  class=\"full-width\">\r\n                            <input [errorStateMatcher]=\"matcher\" matInput required minlength=4 placeholder=\"Straße\" formControlName=\"strasse\" [formControl]=\"FirmenAddressFormGroup.controls.strasse\">\r\n                            <mat-icon *ngIf=\"FirmenAddressFormGroup.controls.strasse.valid\" class=\"success-icon\" matSuffix >done</mat-icon>\r\n                    </mat-form-field>\r\n                    <mat-form-field class=\"full-width\">\r\n                            <input [errorStateMatcher]=\"matcher\" matInput  placeholder=\"Adresszusatz\" formControlName=\"adresszusatz\" [formControl]=\"FirmenAddressFormGroup.controls.adresszusatz\">\r\n                            <mat-icon *ngIf=\"FirmenAddressFormGroup.controls.adresszusatz.touched\" class=\"success-icon\" matSuffix >done</mat-icon>\r\n                    </mat-form-field>\r\n                    <mat-form-field  class=\"full-width\">\r\n                            <input [errorStateMatcher]=\"matcher\" matInput required placeholder=\"Postleitzahl\" minlength=\"4\" maxlength=\"6\" type=\"number\"  formControlName=\"postleitzahl\" [formControl]=\"FirmenAddressFormGroup.controls.postleitzahl\">\r\n                            <mat-icon *ngIf=\"FirmenAddressFormGroup.controls.postleitzahl.valid\" class=\"success-icon\" matSuffix >done</mat-icon>\r\n                    </mat-form-field>\r\n                    <mat-form-field class=\"full-width\">\r\n                            <input [errorStateMatcher]=\"matcher\" matInput required placeholder=\"Ort\"  minlength=\"2\" formControlName=\"ort\" [formControl]=\"FirmenAddressFormGroup.controls.ort\">\r\n                            <mat-icon *ngIf=\"FirmenAddressFormGroup.controls.ort.valid\" class=\"success-icon\" matSuffix >done</mat-icon>\r\n                    </mat-form-field>\r\n                    <mat-form-field class=\"full-width\">\r\n                            <input [errorStateMatcher]=\"matcher\" matInput placeholder=\"Land\"  minlength=\"4\" formControlName=\"land\" [formControl]=\"FirmenAddressFormGroup.controls.land\">\r\n                            <mat-icon *ngIf=\"FirmenAddressFormGroup.controls.land.valid\" class=\"success-icon\" matSuffix >done</mat-icon>\r\n                    </mat-form-field>   \r\n                    <mat-form-field class=\"full-width\">\r\n                                <input [errorStateMatcher]=\"matcher\" matInput placeholder=\"UStID - Umsatzsteueridentifikationsnummer\"  minlength=\"4\" formControlName=\"UStID\" [formControl]=\"FirmenAddressFormGroup.controls.UStID\">\r\n                                <mat-icon *ngIf=\"FirmenAddressFormGroup.controls.UStID.valid\" class=\"success-icon\" matSuffix >done</mat-icon>\r\n                     </mat-form-field>     \r\n            </form>\r\n        </div>   \r\n            <div class=\"col-lg-4 card my-2 py-2\">\r\n                <h6 class=\"card-title\">Kontaktdaten Ansprechpartner:</h6> \r\n                <h6 class=\"fontweightnormal\">Bitte geben Sie hier eine E-Mail Adresse an, an die auch die Rechnung sowie die Deteilauswertung geschickt werden soll.</h6>\r\n                <form class=\"form card-body\" [formGroup]=\"kontaktdatenFormGroup\">                     \r\n                    <mat-form-field  class=\"full-width\">\r\n                        <input [errorStateMatcher]=\"matcher\" matInput required minlength=\"3\" placeholder=\"Name Ihres Betriebs/Standortes\" formControlName=\"name_des_betriebes\" [formControl]=\"kontaktdatenFormGroup.controls.name_des_betriebes\">\r\n                        <mat-icon *ngIf=\"kontaktdatenFormGroup.controls.name_des_betriebes.valid\" class=\"success-icon\" matSuffix >done</mat-icon>\r\n                    </mat-form-field>                        \r\n                    <mat-form-field class=\"full-width\">\r\n                            <mat-select [(value)]=\"gender\" placeholder=\"Bitte wählen Sie eine Ansprache\">\r\n                                <mat-option value=\"Frau\">Frau</mat-option>\r\n                                <mat-option value=\"Herr\">Herr</mat-option>\r\n                            </mat-select>\r\n                    </mat-form-field>\r\n                    <mat-form-field class=\"full-width\">\r\n                        <input [errorStateMatcher]=\"matcher\" matInput minlength=\"3\" required  placeholder=\"Name des Ansprechpartners\" formControlName=\"ansprechpartner\" [formControl]=\"kontaktdatenFormGroup.controls.ansprechpartner\">\r\n                        <mat-icon *ngIf=\"kontaktdatenFormGroup.controls.ansprechpartner.valid\" class=\"success-icon\" matSuffix >done</mat-icon>\r\n                    </mat-form-field>\r\n\r\n                    <mat-form-field class=\"full-width\">\r\n                            <input [errorStateMatcher]=\"matcher\" matInput minlength=\"3\" placeholder=\"E-Mail Anpsprechpartner\" [formControl]=\"kontaktdatenFormGroup.controls.ansprechpartner_email\"\r\n                            [errorStateMatcher]=\"matcher\" required>\r\n                            <mat-hint *ngIf=\"kontaktdatenFormGroup.controls.ansprechpartner_email.hasError('email') && !kontaktdatenFormGroup.controls.ansprechpartner_email.hasError('required')\">\r\n                            Bitte geben Sie ein richtige Emailadresse an!\r\n                            </mat-hint>\r\n                            <mat-icon *ngIf=\"kontaktdatenFormGroup.controls.ansprechpartner_email.valid\" class=\"success-icon\" matSuffix >done</mat-icon>\r\n                    </mat-form-field>\r\n                    <mat-form-field class=\"full-width\">\r\n                        <input [errorStateMatcher]=\"matcher\" matInput minlength=\"3\" required type=\"tel\" placeholder=\"Telefonnummer des Ansprechpartners\"  [formControl]=\"kontaktdatenFormGroup.controls.ansprechpartner_telefon\">\r\n                        <mat-icon *ngIf=\"kontaktdatenFormGroup.controls.ansprechpartner_telefon.valid\" class=\"success-icon\" matSuffix >done</mat-icon>\r\n                    </mat-form-field>\r\n                    <mat-form-field class=\"full-width\">\r\n                    <input [errorStateMatcher]=\"matcher\" matInput minlength=\"3\" type=\"tel\" placeholder=\"Handynummer des Ansprechpartners\" formControlName=\"ansprechpartner_mobil\" [formControl]=\"kontaktdatenFormGroup.controls.ansprechpartner_mobil\">\r\n                       \r\n                    </mat-form-field>\r\n                    <mat-form-field class=\"full-width\">\r\n                        <input [errorStateMatcher]=\"matcher\" matInput placeholder=\"ggf.Notizen\" formControlName=\"notiz\" [formControl]=\"kontaktdatenFormGroup.controls.notiz\">\r\n                    </mat-form-field>\r\n                </form>\r\n            </div>\r\n            <div class=\"col-lg-4 card my-2 py-2\">            \r\n                <form class=\"form\" [formGroup]=\"RechnungsAddressFormGroup\">            \r\n                <h6 class=\"card-title\" *ngIf=\"RechnungsAddressFormGroup.controls.checkedFormControl_Rechnungsadresse.value\">Rechnungsadresse:</h6>\r\n                <div class=\"card-body\" *ngIf=\"RechnungsAddressFormGroup.controls.checkedFormControl_Rechnungsadresse.value\">\r\n                        <mat-form-field class=\"full-width\">\r\n                                <input [errorStateMatcher]=\"matcher\" matInput required minlength=\"3\" placeholder=\"Firmenname\" formControlName=\"name\" [formControl]=\"RechnungsAddressFormGroup.controls.name\">\r\n                                <mat-icon *ngIf=\"RechnungsAddressFormGroup.controls.name.valid\" class=\"success-icon\" matSuffix >done</mat-icon>\r\n                        </mat-form-field>\r\n                        <mat-form-field  class=\"full-width\">\r\n                                <input [errorStateMatcher]=\"matcher\" matInput required minlength=\"3\" placeholder=\"Straße\" formControlName=\"strasse\" [formControl]=\"RechnungsAddressFormGroup.controls.strasse\">\r\n                                <mat-icon *ngIf=\"RechnungsAddressFormGroup.controls.strasse.valid\" class=\"success-icon\" matSuffix >done</mat-icon>\r\n                        </mat-form-field>\r\n                        <mat-form-field class=\"full-width\">\r\n                                <input [errorStateMatcher]=\"matcher\" matInput placeholder=\"Adresszusatz\" formControlName=\"adresszusatz\" [formControl]=\"RechnungsAddressFormGroup.controls.adresszusatz\">\r\n                                <mat-icon *ngIf=\"RechnungsAddressFormGroup.controls.adresszusatz.touched\" class=\"success-icon\" matSuffix >done</mat-icon>\r\n                        </mat-form-field>\r\n                        <mat-form-field  class=\"full-width\">\r\n                                <input [errorStateMatcher]=\"matcher\" matInput required type=\"number\" placeholder=\"Postleitzahl\" minlength=\"4\" maxlength=\"6\" formControlName=\"postleitzahl\" [formControl]=\"RechnungsAddressFormGroup.controls.postleitzahl\">\r\n                                <mat-icon *ngIf=\"RechnungsAddressFormGroup.controls.postleitzahl.valid\" class=\"success-icon\" matSuffix >done</mat-icon>\r\n                        </mat-form-field>\r\n                        <mat-form-field class=\"full-width\">\r\n                                <input [errorStateMatcher]=\"matcher\" matInput required minlength=\"3\" placeholder=\"Ort\" formControlName=\"ort\" [formControl]=\"RechnungsAddressFormGroup.controls.ort\">\r\n                                <mat-icon *ngIf=\"RechnungsAddressFormGroup.controls.ort.valid\" class=\"success-icon\" matSuffix >done</mat-icon>\r\n                        </mat-form-field>\r\n                        <mat-form-field class=\"full-width\">\r\n                                <input [errorStateMatcher]=\"matcher\" matInput placeholder=\"Land\" minlength=\"3\" formControlName=\"land\" [formControl]=\"RechnungsAddressFormGroup.controls.land\">\r\n                                <mat-icon *ngIf=\"RechnungsAddressFormGroup.controls.land.valid\" class=\"success-icon\" matSuffix >done</mat-icon>\r\n                        </mat-form-field>   \r\n                        <mat-form-field class=\"full-width\">\r\n                                <input [errorStateMatcher]=\"matcher\" matInput placeholder=\"UStID - Umsatzsteueridentifikationsnummer\" minlength=\"3\" formControlName=\"UStID\" [formControl]=\"RechnungsAddressFormGroup.controls.UStID\">\r\n                                <mat-icon *ngIf=\"RechnungsAddressFormGroup.controls.UStID.valid\" class=\"success-icon\" matSuffix >done</mat-icon>\r\n                        </mat-form-field>     \r\n                </div>\r\n                <h6>Ist die Rechnungsadresse gleich der Firmenadresse?</h6>\r\n                <mat-slide-toggle #Rechnungsadresse aria-label type=\"checkbox\" [checked]=\"RechnungsAddressFormGroup.controls.checkedFormControl_Rechnungsadresse.value\" formControlName=\"checkedFormControl_Rechnungsadresse\" [formControl]=\"RechnungsAddressFormGroup.controls.checkedFormControl_Rechnungsadresse\">\r\n                <h6 class=\"mt-2 align-middle\">{{RechnungsAddressFormGroup.controls.checkedFormControl_Rechnungsadresse.value ? 'Nein' : 'Ja'}}</h6></mat-slide-toggle>\r\n            </form>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/kundeneingabe/kundeneingabe.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/kundeneingabe/kundeneingabe.component.ts ***!
  \**********************************************************/
/*! exports provided: MyErrorStateMatcher, KundeneingabeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyErrorStateMatcher", function() { return MyErrorStateMatcher; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "KundeneingabeComponent", function() { return KundeneingabeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_data_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../_services/data.service */ "./src/app/_services/data.service.ts");
/* harmony import */ var _services_pruefservice_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../_services/pruefservice.service */ "./src/app/_services/pruefservice.service.ts");
/* harmony import */ var _models_index__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../_models/index */ "./src/app/_models/index.ts");
/* harmony import */ var _guards_index__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../_guards/index */ "./src/app/_guards/index.ts");










/** custom Error when invalid control is dirty, touched, or submitted. */
var MyErrorStateMatcher = /** @class */ (function () {
    function MyErrorStateMatcher() {
    }
    MyErrorStateMatcher.prototype.isErrorState = function (control, form) {
        var isSubmitted = form && form.submitted;
        return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    };
    return MyErrorStateMatcher;
}());

var KundeneingabeComponent = /** @class */ (function () {
    function KundeneingabeComponent(Dataservice, Pruefservice, authGuardService) {
        this.Dataservice = Dataservice;
        this.Pruefservice = Pruefservice;
        this.authGuardService = authGuardService;
        this.matcher = new MyErrorStateMatcher();
        this.gender = "";
        this.KundeInComponente = new _models_index__WEBPACK_IMPORTED_MODULE_6__["Kunde"]();
        this.AdressenInComponente = new _models_index__WEBPACK_IMPORTED_MODULE_6__["Adressen"]();
        //KundenFormgroup Deklarieren
        this.kontaktdatenFormGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            kunden_id: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            //Kundenobjekt:
            name_des_betriebes: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            kunden_nummer: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', []),
            ansprechpartner: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            ansprechpartner_email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email]),
            ansprechpartner_mobil: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', []),
            ansprechpartner_telefon: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            notiz: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', []),
        });
        this.kontaktdatenNamenArray = new Array();
        this.kontaktdatenNamenArray.push("Name des Betriebs");
        this.kontaktdatenNamenArray.push("KundenNummer");
        this.kontaktdatenNamenArray.push("Ansprechpartner");
        this.kontaktdatenNamenArray.push("Ansprechpartner Email");
        this.kontaktdatenNamenArray.push("Ansprechpartner Mobil");
        this.kontaktdatenNamenArray.push("Ansprechpartner Telefon");
        this.kontaktdatenNamenArray.push("Notiz");
        this.kontaktdatenFormArray = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormArray"]([
            new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
            new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
            new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
            new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
            new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]()
        ]);
        //Firmenadresse deklarieren
        this.FirmenAddressFormGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            strasse: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            adresszusatz: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', []),
            postleitzahl: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            ort: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            land: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            UStID: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
        });
        this.FirmenAdressNamenArray = new Array();
        this.FirmenAdressNamenArray.push("Name");
        this.FirmenAdressNamenArray.push("Strasse");
        this.FirmenAdressNamenArray.push("Adresszusatz");
        this.FirmenAdressNamenArray.push("Postleitzahl");
        this.FirmenAdressNamenArray.push("Ort");
        this.FirmenAdressNamenArray.push("Land");
        this.FirmenAdressNamenArray.push("UStID");
        this.FirmenAdressFormArray = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormArray"]([
            new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
            new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
            new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
            new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
            new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
            new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
            new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]() //6    
        ]);
        //Rechnungsadress deklarieren
        this.RechnungsAddressFormGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            checkedFormControl_Rechnungsadresse: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            strasse: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            adresszusatz: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].nullValidator]),
            postleitzahl: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(6), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(4)]),
            ort: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            land: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            UStID: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
        });
        this.RechnungsAdressNamenArray = new Array();
        this.RechnungsAdressNamenArray.push("Rechnungsaddresse ist gleich Firmenaddresse");
        this.RechnungsAdressNamenArray.push("Name");
        this.RechnungsAdressNamenArray.push("Strasse");
        this.RechnungsAdressNamenArray.push("Adresszusatz");
        this.RechnungsAdressNamenArray.push("Postleitzahl");
        this.RechnungsAdressNamenArray.push("Ort");
        this.RechnungsAdressNamenArray.push("land");
        this.RechnungsAdressNamenArray.push("UStID");
        this.RechnungsAdressFormArray = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormArray"]([
            new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
            new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
            new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
            new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
            new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
            new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
            new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
            new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]() //7    
        ]);
        this.matcher = new MyErrorStateMatcher();
    }
    KundeneingabeComponent.prototype.ngOnInit = function () {
        var _this = this;
        //subscribe auf den Kunden und 
        this.Dataservice.getKunde()
            //schreib den Scheiß ins Formular
            .subscribe(function (response) {
            _this.KundeInComponente = response;
            //Kunden im Dataservice speichern
            _this.Dataservice.updateKunde(_this.KundeInComponente);
            console.log("getKunde FUNKTIONIERT!", _this.KundeInComponente);
            //Kunden befüllen:
            _this.kontaktdatenFormGroup.controls.name_des_betriebes.setValue(_this.KundeInComponente.name_des_betriebes);
            _this.kontaktdatenFormGroup.controls.ansprechpartner.setValue(_this.KundeInComponente.ansprechpartner);
            _this.kontaktdatenFormGroup.controls.ansprechpartner_email.setValue(_this.KundeInComponente.ansprechpartner_email);
            //pruefung auf null,...leider....Fuck Typescript!
            if (_this.KundeInComponente.ansprechpartner_mobil != null) {
                _this.kontaktdatenFormGroup.controls.ansprechpartner_mobil.setValue(_this.KundeInComponente.ansprechpartner_mobil);
            }
            if (_this.KundeInComponente.ansprechpartner_telefon != null) {
                _this.kontaktdatenFormGroup.controls.ansprechpartner_telefon.setValue(_this.KundeInComponente.ansprechpartner_telefon);
            }
            if (_this.KundeInComponente.notiz != null) {
                _this.kontaktdatenFormGroup.controls.notiz.setValue(_this.KundeInComponente.notiz);
                console.log(_this.KundeInComponente.notiz);
            }
        }, function (error) { return console.log(error); });
        //get Adressen
        this.Dataservice.getAdressen()
            .subscribe(function (response) {
            //speichere State in DIESER Componente!
            _this.AdressenInComponente = response;
            //im Dataservice speichern
            _this.Dataservice.updateFirmenAdresse(_this.AdressenInComponente.Firmenadresse);
            _this.Dataservice.updateRechnungsAdresse(_this.AdressenInComponente.Rechnungsadresse);
            console.log("getAdresse FUNKTIONIERT!", _this.AdressenInComponente);
            console.log("Rechnungsadresse:", _this.AdressenInComponente.Rechnungsadresse);
            //FirmenAdresse befüllen:
            if (_this.AdressenInComponente.Firmenadresse != null) {
                _this.FirmenAddressFormGroup.controls.strasse.setValue(_this.AdressenInComponente.Firmenadresse.strasse);
                _this.FirmenAddressFormGroup.controls.ort.setValue(_this.AdressenInComponente.Firmenadresse.ort);
                _this.FirmenAddressFormGroup.controls.postleitzahl.setValue(_this.AdressenInComponente.Firmenadresse.postleitzahl);
                _this.FirmenAddressFormGroup.controls.land.setValue(_this.AdressenInComponente.Firmenadresse.land);
                _this.FirmenAddressFormGroup.controls.UStID.setValue(_this.AdressenInComponente.Firmenadresse.UStID);
                //pruefung auf null,...leider....Fuck Typescript!
                if (_this.AdressenInComponente.Firmenadresse != null) {
                    _this.FirmenAddressFormGroup.controls.name.setValue(_this.AdressenInComponente.Firmenadresse.name);
                    _this.FirmenAddressFormGroup.controls.adresszusatz.setValue(_this.AdressenInComponente.Firmenadresse.adresszusatz);
                }
            }
            //RechnungsAdresse befüllen:
            if (_this.AdressenInComponente.Rechnungsadresse != null) {
                _this._SlideToggle_Rechnungsaddresse.toggle();
                setTimeout(function () {
                    _this.RechnungsAddressFormGroup.controls.strasse.setValue(_this.AdressenInComponente.Rechnungsadresse.strasse);
                    _this.RechnungsAddressFormGroup.controls.ort.setValue(_this.AdressenInComponente.Rechnungsadresse.ort);
                    _this.RechnungsAddressFormGroup.controls.postleitzahl.setValue(_this.AdressenInComponente.Rechnungsadresse.postleitzahl);
                    _this.RechnungsAddressFormGroup.controls.land.setValue(_this.AdressenInComponente.Rechnungsadresse.land);
                    _this.RechnungsAddressFormGroup.controls.UStID.setValue(_this.AdressenInComponente.Rechnungsadresse.UStID);
                }, 500);
                //pruefung auf null,...leider....Fuck Typescript!
                if (_this.AdressenInComponente.Firmenadresse.name != null) {
                    _this.RechnungsAddressFormGroup.controls.name.setValue(_this.AdressenInComponente.Rechnungsadresse.name);
                }
                if (_this.AdressenInComponente.Firmenadresse.adresszusatz != null) {
                    _this.RechnungsAddressFormGroup.controls.adresszusatz.setValue(_this.AdressenInComponente.Rechnungsadresse.adresszusatz);
                }
            }
        }, function (error) { return console.log(error); });
        //get Umweltchecks
        this.Dataservice.getUmweltChecks()
            .subscribe(function (response) {
            //speichere den Umweltcheck weg:
            _this.Dataservice.updateUmweltchecks(response);
            console.log("Http-Response in Kundeneingabe:", _this.Dataservice.Umweltchecks);
        }, function (error) {
            console.log(error);
            if (error.status == 405 || 410 || 302) {
                _this.authGuardService.deleteCurrentUser();
            }
        });
        //Listen to saveKundeEvent vom Parentcomponent: eingabe.component => SAVE local Data!
        this.saveKunde.subscribe(function () {
            //speichere den geupdaten lokal ab! 
            _this.Dataservice.saveKunde();
        }, function (error) {
            if (error.status == 405 || 410 || 302) {
                _this.authGuardService.deleteCurrentUser();
            }
        }, function (complete) { console.log("Kunde erfolgereich vom Frontent versendet"); });
        //Listen to save AdressenEvent vom Parentcomponent: eingabe.component
        //speichere die geupdaten lokal ab!
        this.saveAdressen.subscribe(function () {
            _this.Dataservice.saveFirmenAdresse();
            if (_this._SlideToggle_Rechnungsaddresse.checked) {
                console.log("Save Rechnungsadresse");
                _this.Dataservice.saveRechnungsAdresse();
            }
            else {
                console.log("Delete Rechnungsaddresse");
                _this.Dataservice.deleteRechnungsAdresse();
            }
        }, function (error) {
            console.log(error);
            if (error.status == 405 || 410 || 302) {
                _this.authGuardService.deleteCurrentUser();
            }
        }, function (complete) { console.log("Kunde erfolgereich vom Frontent versendet"); });
        this.pruefeEingabe.subscribe(function () {
            _this.pruefeKundeneingabe(), console.log("PrüfeKundeineingabe()");
        });
        //Listen to save Umweltcheck vom Parentcomponent: eingabe.component
        //speichere die geupdaten lokal ab!
        setTimeout(function () { _this.onChanges(); }, 3000);
    };
    KundeneingabeComponent.prototype.ngAfterViewInit = function () {
    };
    KundeneingabeComponent.prototype.onChanges = function () {
        //this.name.valueChanges.subscribe(val=>{console.log("name valueChange läuft")})
        var _this = this;
        this.kontaktdatenFormGroup.valueChanges
            .subscribe(function (val) {
            var Kunde2 = new _models_index__WEBPACK_IMPORTED_MODULE_6__["Kunde"]();
            //unbedingt alte IDs mitnehmen!
            Kunde2.kunden_id = _this.Dataservice.KundenBhobj.getValue().kunden_id;
            Kunde2.firmen_adresse_id = _this.Dataservice.KundenBhobj.getValue().firmen_adresse_id;
            Kunde2.rechnungs_adresse_id = _this.Dataservice.KundenBhobj.getValue().rechnungs_adresse_id;
            Kunde2.name_des_betriebes = _this.kontaktdatenFormGroup.controls.name_des_betriebes.value;
            Kunde2.ansprechpartner = _this.kontaktdatenFormGroup.controls.ansprechpartner.value;
            Kunde2.ansprechpartner_email = _this.kontaktdatenFormGroup.controls.ansprechpartner_email.value;
            Kunde2.ansprechpartner_mobil = _this.kontaktdatenFormGroup.controls.ansprechpartner_mobil.value;
            Kunde2.ansprechpartner_telefon = _this.kontaktdatenFormGroup.controls.ansprechpartner_telefon.value;
            Kunde2.notiz = _this.kontaktdatenFormGroup.controls.notiz.value;
            console.log("Values Changed kontaktdatenFormGroup => works", val);
            _this.Dataservice.updateKunde(Kunde2);
        });
        this.FirmenAddressFormGroup.valueChanges.subscribe(function (val) {
            var Adressen2 = new _models_index__WEBPACK_IMPORTED_MODULE_6__["Adressen"]();
            //Adressen2.Firmenadresse.adress_id = this.AdressenInComponente.Firmenadresse.adress_id
            //Adressen2.Rechnungsadresse.adress_id = this.AdressenInComponente.Rechnungsadresse.adress_id
            //Firmenadresse
            Adressen2.Firmenadresse.name = _this.FirmenAddressFormGroup.controls.name.value;
            Adressen2.Firmenadresse.adresszusatz = _this.FirmenAddressFormGroup.controls.adresszusatz.value;
            Adressen2.Firmenadresse.strasse = _this.FirmenAddressFormGroup.controls.strasse.value;
            Adressen2.Firmenadresse.ort = _this.FirmenAddressFormGroup.controls.ort.value;
            Adressen2.Firmenadresse.postleitzahl = String(_this.FirmenAddressFormGroup.controls.postleitzahl.value);
            Adressen2.Firmenadresse.land = _this.FirmenAddressFormGroup.controls.land.value;
            Adressen2.Firmenadresse.typ = "Firmenadresse";
            Adressen2.Firmenadresse.UStID = _this.FirmenAddressFormGroup.controls.UStID.value;
            console.log("Values Changed FirmenAddressFormGroup => works", val);
            _this.Dataservice.updateFirmenAdresse(Adressen2.Firmenadresse);
        });
        this.RechnungsAddressFormGroup.valueChanges.subscribe(function (val) {
            var Adressen1 = new _models_index__WEBPACK_IMPORTED_MODULE_6__["Adressen"]();
            //Rechnungsadresse
            Adressen1.Rechnungsadresse.name = _this.RechnungsAddressFormGroup.controls.name.value;
            Adressen1.Rechnungsadresse.adresszusatz = _this.RechnungsAddressFormGroup.controls.adresszusatz.value;
            Adressen1.Rechnungsadresse.strasse = _this.RechnungsAddressFormGroup.controls.strasse.value;
            Adressen1.Rechnungsadresse.ort = _this.RechnungsAddressFormGroup.controls.ort.value;
            Adressen1.Rechnungsadresse.postleitzahl = String(_this.RechnungsAddressFormGroup.controls.postleitzahl.value);
            Adressen1.Rechnungsadresse.land = _this.RechnungsAddressFormGroup.controls.land.value;
            Adressen1.Rechnungsadresse.typ = "Rechnungsadresse";
            Adressen1.Rechnungsadresse.UStID = _this.RechnungsAddressFormGroup.controls.UStID.value;
            _this.Dataservice.updateRechnungsAdresse(Adressen1.Rechnungsadresse);
        });
    };
    //dies Funktion packt die FormArray mit den zugehörigen Namen (=Fehlermeldungen)
    //in die Arrays und updated diese manuell (fuck this shit, warum nicht automatisch?)
    KundeneingabeComponent.prototype.pruefeKundeneingabe = function () {
        //Array zusammenkloppen: "Name", "Error"
        if (this.kontaktdatenFormGroup.controls.name_des_betriebes.invalid) {
            this.Pruefservice.pushNameErrorArray("Name des Betriebes", this.kontaktdatenFormGroup.controls.name_des_betriebes.errors);
        }
        if (this.kontaktdatenFormGroup.controls.kunden_nummer.invalid) {
            this.Pruefservice.pushNameErrorArray("Kundennummer", this.kontaktdatenFormGroup.controls.kunden_nummer.errors);
        }
        if (this.kontaktdatenFormGroup.controls.ansprechpartner.invalid) {
            this.Pruefservice.pushNameErrorArray("Name des Ansprechpartners", this.kontaktdatenFormGroup.controls.ansprechpartner.errors);
        }
        if (this.kontaktdatenFormGroup.controls.ansprechpartner_email.invalid) {
            this.Pruefservice.pushNameErrorArray("Ansprechpartner Email", this.kontaktdatenFormGroup.controls.ansprechpartner_email.errors);
        }
        if (this.kontaktdatenFormGroup.controls.ansprechpartner_mobil.invalid) {
            this.Pruefservice.pushNameErrorArray("Ansprechpartner Mobil", this.kontaktdatenFormGroup.controls.ansprechpartner_mobil.errors);
        }
        if (this.kontaktdatenFormGroup.controls.ansprechpartner_telefon.invalid) {
            this.Pruefservice.pushNameErrorArray("Ansprechpartner Telefon", this.kontaktdatenFormGroup.controls.ansprechpartner_telefon.errors);
        }
        if (this.kontaktdatenFormGroup.controls.notiz.invalid) {
            this.Pruefservice.pushNameErrorArray("Notiz", this.kontaktdatenFormGroup.controls.notiz.errors);
        }
        this.Pruefservice.pushInvalidFormgroup("Kontaktdaten", this.kontaktdatenFormGroup);
        /*
        name_des_betriebes: new FormControl('',[Validators.required]), //0
          kunden_nummer: new FormControl('',[]),                         //1
          ansprechpartner: new FormControl('',[Validators.required]),    //2
          ansprechpartner_email : new FormControl('', [Validators.required, Validators.email]), //3
          ansprechpartner_mobil : new FormControl('', []),               //4
          ansprechpartner_telefon : new FormControl('', [Validators.required]),    //5
          notiz: new Form //6
        */
        if (this.FirmenAddressFormGroup.controls.name.invalid) {
            this.Pruefservice.pushNameErrorArray("Name", this.FirmenAddressFormGroup.controls.name.errors);
        }
        if (this.FirmenAddressFormGroup.controls.strasse.invalid) {
            this.Pruefservice.pushNameErrorArray("Strasse", this.FirmenAddressFormGroup.controls.strasse.errors);
        }
        if (this.FirmenAddressFormGroup.controls.adresszusatz.invalid) {
            this.Pruefservice.pushNameErrorArray("Adresszusatz", this.FirmenAddressFormGroup.controls.adresszusatz.errors);
        }
        if (this.FirmenAddressFormGroup.controls.postleitzahl.invalid) {
            this.Pruefservice.pushNameErrorArray("Postleitzahl", this.FirmenAddressFormGroup.controls.postleitzahl.errors);
        }
        if (this.FirmenAddressFormGroup.controls.ort.invalid) {
            this.Pruefservice.pushNameErrorArray("Ort", this.FirmenAddressFormGroup.controls.ort.errors);
        }
        if (this.FirmenAddressFormGroup.controls.land.invalid) {
            this.Pruefservice.pushNameErrorArray("Land", this.FirmenAddressFormGroup.controls.land.errors);
        }
        if (this.FirmenAddressFormGroup.controls.UStID.invalid) {
            this.Pruefservice.pushNameErrorArray("UStID: Umsatzsteueridentifikationsnummer", this.FirmenAddressFormGroup.controls.UStID.errors);
        }
        this.Pruefservice.pushInvalidFormgroup("Firmenadresse", this.FirmenAddressFormGroup);
        /*      name: new FormControl('',[Validators.required]),    //0
           strasse: new FormControl('',[Validators.required]), //1
           adresszusatz: new FormControl('',[]),               //2
           postleitzahl: new FormControl('',[Validators.required]), //3
           ort: new FormControl('',[Validators.required]),     //4
           land : new FormControl('', []),                     //5
           UStID: new FormControl('', [Validators.required]),  //6 */
        if (this._SlideToggle_Rechnungsaddresse.checked) {
            if (this.RechnungsAddressFormGroup.controls.checkedFormControl_Rechnungsadresse.invalid) {
                this.Pruefservice.pushNameErrorArray("Rechnungsadresse ist gleich Firmenadresse", this.RechnungsAddressFormGroup.controls.checkedFormControl_Rechnungsadresse.errors);
            }
            if (this.RechnungsAddressFormGroup.controls.name.invalid) {
                this.Pruefservice.pushNameErrorArray("Name", this.RechnungsAddressFormGroup.controls.name.errors);
            }
            if (this.RechnungsAddressFormGroup.controls.strasse.invalid) {
                this.Pruefservice.pushNameErrorArray("Strasse", this.RechnungsAddressFormGroup.controls.strasse.errors);
            }
            if (this.RechnungsAddressFormGroup.controls.adresszusatz.invalid) {
                this.Pruefservice.pushNameErrorArray("Adresszusatz", this.RechnungsAddressFormGroup.controls.adresszusatz.errors);
            }
            if (this.RechnungsAddressFormGroup.controls.postleitzahl.invalid) {
                this.Pruefservice.pushNameErrorArray("Postleitzahl", this.RechnungsAddressFormGroup.controls.postleitzahl.errors);
            }
            if (this.RechnungsAddressFormGroup.controls.ort.invalid) {
                this.Pruefservice.pushNameErrorArray("Ort", this.RechnungsAddressFormGroup.controls.ort.errors);
            }
            if (this.RechnungsAddressFormGroup.controls.land.invalid) {
                this.Pruefservice.pushNameErrorArray("Land", this.RechnungsAddressFormGroup.controls.land.errors);
            }
            if (this.RechnungsAddressFormGroup.controls.UStID.invalid) {
                this.Pruefservice.pushNameErrorArray("UStID - Umsatzsteueridentifikationnummer", this.RechnungsAddressFormGroup.controls.UStID.errors);
            }
            this.Pruefservice.pushInvalidFormgroup("Rechnungsadresse", this.RechnungsAddressFormGroup);
        }
        /*
              checkedFormControl_Rechnungsadresse : new FormControl('',[Validators.required]), //0
            name: new FormControl('',[Validators.required]),                                 //1
            strasse: new FormControl('',[Validators.required]),                              //2
            adresszusatz: new FormControl('',[Validators.nullValidator]),                    //3
            postleitzahl: new FormControl('',[Validators.required]),                         //4
            ort: new FormControl('',[Validators.required]),                                  //5
            land : new FormControl('', [Validators.required]),                               //6
            UStID: new FormControl('', [Validators.required]),                               //7
         */
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], KundeneingabeComponent.prototype, "saveKunde", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], KundeneingabeComponent.prototype, "saveAdressen", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], KundeneingabeComponent.prototype, "pruefeEingabe", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('Rechnungsadresse'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSlideToggle"])
    ], KundeneingabeComponent.prototype, "_SlideToggle_Rechnungsaddresse", void 0);
    KundeneingabeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-kundeneingabe',
            template: __webpack_require__(/*! ./kundeneingabe.component.html */ "./src/app/kundeneingabe/kundeneingabe.component.html"),
            styles: [__webpack_require__(/*! ./kundeneingabe.component.css */ "./src/app/kundeneingabe/kundeneingabe.component.css")]
        }),
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root',
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_data_service__WEBPACK_IMPORTED_MODULE_4__["DataService"], _services_pruefservice_service__WEBPACK_IMPORTED_MODULE_5__["PruefService"], _guards_index__WEBPACK_IMPORTED_MODULE_7__["AuthGuard"]])
    ], KundeneingabeComponent);
    return KundeneingabeComponent;
}());



/***/ }),

/***/ "./src/app/landingpage/landingpage.component.css":
/*!*******************************************************!*\
  !*** ./src/app/landingpage/landingpage.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xhbmRpbmdwYWdlL2xhbmRpbmdwYWdlLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/landingpage/landingpage.component.html":
/*!********************************************************!*\
  !*** ./src/app/landingpage/landingpage.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- HOME SECTION -->\r\n<header id=\"home-section\">\r\n    <div class=\"home-inner container\">\r\n      <div class=\"row\">\r\n        <div class=\"col-lg-9 col-md-12 col-sm-12 rounded py-1\" style=\"background-color:rgba(255,255,255,0.85);\">\r\n            <ngb-tabset>\r\n                <ngb-tab title=\"QU\">\r\n                  <ng-template ngbTabContent>\r\n                        <img class=\"mt-3\" src=\"/assets/img/Umweltcheck_Logo_transparent.png\" style=\"width:50%; margin-left:23%\">\r\n\r\n                        <h3 class=\"display-6 text-center mt-3\">\r\n                            Die günstige und unkomplizierte Ermittlung betrieblicher Umwelt- und Klimakennzahlen\r\n                        </h3>\r\n                        <div class=\"d-flex\">\r\n                          <div class=\"p-2 align-self-center align-middle \">              \r\n                            <fa class=\"fas\" name=\"check\" size=\"3x\"></fa>\r\n                          </div>\r\n                          <div class=\"p-2 align-self-center align-middle\">\r\n                              Vergleichen Sie die Nachhaltigekeit Ihres Betriebes mit anderen Betrieben Ihrer Branche \r\n                          </div>\r\n                        </div>\r\n                        <div class=\"d-flex\">\r\n                                <div class=\"p-2 align-self-center align-middle \">              \r\n                                  <fa class=\"fas\" name=\"check\" size=\"3x\"></fa>\r\n                                </div>\r\n                                <div class=\"p-2 align-self-center align-middle\">\r\n                                    Bestimmen Sie unkompliziert die relevanten Klimakennzahlen für Ihreren Betrieb\r\n                                </div>\r\n                              </div>\r\n                        <div class=\"d-flex\">\r\n                          <div class=\"p-2 align-self-center align-middle\">\r\n                              <fa class=\"fas\" name=\"check\" size=\"3x\"></fa>\r\n                          </div>\r\n                          <div class=\"p-2 align-self-center align-middle\">\r\n                              Betriebe der Kategorien Hotels, Pensionen, Ferienwohnungen und Ferienhäuser können teilnehmen\r\n                          </div>\r\n                        </div>\r\n                      <div class=\"d-flex\">\r\n                          <div class=\"p-2 align-self-center align-middle\">\r\n                              <fa class=\"fas\" name=\"check\" size=\"3x\"></fa>\r\n                          </div>\r\n                          <div class=\"p-2 align-self-center align-middle\">\r\n                            99,- Euro zzgl. MwSt. pro Betrieb für einen Quick Check Umwelt  \r\n                          </div>\r\n                      </div>\r\n\r\n                  </ng-template>\r\n                </ngb-tab>\r\n                <ngb-tab id=\"Vorteile\">\r\n                  <ng-template ngbTabTitle>Was ist der Quick Check Umwelt?</ng-template>\r\n                  <ng-template ngbTabContent>\r\n                        <img class=\"mt-3\" src=\"/assets/img/Umweltcheck_Logo_transparent.png\" style=\"width:50%; margin-left:23%\">\r\n\r\n                        <div class=\"card-head h5 p-2 \">\r\n                                Was ist der Quick Check Umwelt?\r\n                                </div>\r\n                                <div class=\"card-body\">\r\n                                  Der Quick Check Umwelt ist ein Online-Tool zur erstmaligen Ermittlung der betrieblichen Umwelt- und Klimafreundlichkeit von Berherbergungsbetrieben der Kategorien Hotel, Pension, Ferienwohnung und Ferienhaus.<br>\r\n                                  Mithilfe eines digitalen Online-Bogens werden relevante Kennzahlen in den Bereichen Strom, Heizung, Wasser und Abfall erfasst. Die Auswertung erfolgt anhand einer systematischen Auflistung der betrieblichen Verbräuche in Relation zu denen vergleichbarer Betriebe.<br>\r\n                                  Teilnehmende Betriebe erhalten entsprechend schnell und kostengünstig wertige Ergebnisse, ohne umfassende Kriterien- bzw. Erhebungsbögen ausfüllen zu müssen und Belege einzureichen.             \r\n                        </div>\r\n                    </ng-template>\r\n                </ngb-tab>\r\n                <ngb-tab title=\"Wer prüft den Quick Check Umwelt?\">\r\n                  <ng-template ngbTabContent>\r\n                        <img class=\"mt-3\" src=\"/assets/img/Umweltcheck_Logo_transparent.png\" style=\"width:50%; margin-left:23%\">\r\n                        <div class=\"card-head h5 p-2 \">\r\n                                Wer prüft den Quick Check Umwelt?\r\n                            </div>\r\n                            <div class=\"card-body\">                  \r\n                              Der Quick Check Umwelt wird angeboten und durchgeführt von Viabono, der unabhängigen und anerkannten\r\n                              Fachorganisation für Umwelt- und Nachhaltigkeitszertifizierungen. Kommunikativ unterstützende Partner sind\r\n                              u. a. Qualitätsgastgeber Wanderbares Deutschland, Bett+Bike Deutschland sowie der DEHOGA Bundesverband.\r\n                            </div>     \r\n                   </ng-template>\r\n                </ngb-tab>\r\n\r\n              <ngb-tab title=\"Teilnahme\" id=\"Teilnahme\">\r\n                    <ng-template ngbTabContent>\r\n                            <img class=\"mt-3\" src=\"/assets/img/Umweltcheck_Logo_transparent.png\" style=\"width:50%; margin-left:23%\">\r\n                            <div class=\"card-head h5 p-2 \">\r\n                                    Wer kann teilnehmen?\r\n                            </div>  \r\n                            <div class=\"card-body\">                  \r\n                                  Der Quick Check Umwelt richtet sich an Beherbergungsbetriebe der Kategorien Hotels, Pensionen, Ferienwohnungen und Ferienhäuser.\r\n                            </div>\r\n                    </ng-template>\r\n                  </ngb-tab>\r\n\r\n              <ngb-tab title=\"Bereiche\">\r\n                        <ng-template ngbTabContent>\r\n                                <img class=\"mt-3\" src=\"/assets/img/Umweltcheck_Logo_transparent.png\" style=\"width:50%; margin-left:23%\">\r\n                                <div class=\"card-head h5 p-2 \">\r\n                                        Welche Bereiche umfasst der Quick Check Umwelt?\r\n                                </div>\r\n                                <div class=\"card-body\">                  \r\n                                        Die Berechnung umfasst die Bereiche Strom, Heizung,\r\n                                        Wasser und Abfall.\r\n                                </div>\r\n                        </ng-template>\r\n                      </ngb-tab>\r\n\r\n              <ngb-tab title=\"Gebühren\" id=\"Gebühren\">\r\n                    <ng-template ngbTabContent>\r\n                            <img class=\"mt-3\" src=\"/assets/img/Umweltcheck_Logo_transparent.png\" style=\"width:50%; margin-left:23%\">\r\n                            <div class=\"card-head h5 p-2 \" >\r\n                                    Wie hoch sind die Gebühren?\r\n                                </div>\r\n                                <div class=\"card-body\">                  \r\n                                  Für die Durchführung des Quick Check Umwelt wird ein\r\n                                  Betrag von 99,- Euro zzgl. MwSt. erhoben.\r\n                                </div>  \r\n                    </ng-template>\r\n               </ngb-tab>\r\n            </ngb-tabset>                      \r\n        </div>               \r\n\r\n        <div class=\"col-lg-3 col-md-12 col-sm-12\">\r\n          <div class=\"card bg-success text-center card-form\" style=\"height:100%\">\r\n            <div class=\"card-head ml-3 mr-3\"><h3 class=\"mt-3 p-4 rounded\" style=\"background-color:white\">Melden Sie sich heute noch an</h3>\r\n            </div>\r\n            <div class=\"card-body align-middle\">\r\n              <app-register></app-register>\r\n            </div>\r\n          </div>\r\n        </div>      \r\n    </div>\r\n  </div>\r\n</header>\r\n\r\n<!-- EXPLORE HEAD -->\r\n<section id=\"explore-head-section\">\r\n  <div class=\"container rounded\">\r\n    <div class=\"row\"> \r\n      <h1 class=\"m-auto p-4 display-5\" style=\"color:rgba(0,0,0,0.8)\">Erfahren Sie mehr:</h1> \r\n    </div>\r\n    <div class=\"row\">      \r\n    </div>\r\n    <div class=\"row\">\r\n        <div class=\"col-sm-12 col-md-6 col-lg-6 pl-4 py-1\">\r\n            <div class=\"card border-success\">  \r\n                <div class=\"card-head h5 p-2 \">\r\n                Was ist der Quick Check Umwelt?\r\n                </div>\r\n                <div class=\"card-body\">\r\n                  Der Quick Check Umwelt ist ein Online-Tool zur erstmaligen Ermittlung der betrieblichen Umwelt- und Klimafreundlichkeit von Berherbergungsbetrieben der Kategorien Hotel, Pension, Ferienwohnung und Ferienhaus.<br>\r\n                  Mithilfe eines digitalen Online-Bogens werden relevante Kennzahlen in den Bereichen Strom, Heizung, Wasser und Abfall erfasst. Die Auswertung erfolgt anhand einer systematischen Auflistung der betrieblichen Verbräuche in Relation zu denen vergleichbarer Betriebe.<br>\r\n                  Teilnehmende Betriebe erhalten entsprechend schnell und kostengünstig wertige Ergebnisse, ohne umfassende Kriterien- bzw. Erhebungsbögen ausfüllen zu müssen und Belege einzureichen.             \r\n                </div>\r\n              \r\n            </div> \r\n          </div>\r\n          <div class=\"col-sm-12 col-md-6 col-lg-6 py-1\">\r\n              <div class=\"card border-success\">  \r\n                  <div class=\"card-head h5 p-2 \">\r\n                      Wer prüft den Quick Check Umwelt?\r\n                  </div>\r\n                  <div class=\"card-body\">                  \r\n                    Der Quick Check Umwelt wird angeboten und durchgeführt von Viabono, der unabhängigen und anerkannten\r\n                    Fachorganisation für Umwelt- und Nachhaltigkeitszertifizierungen. Kommunikativ unterstützende Partner sind\r\n                    u. a. Qualitätsgastgeber Wanderbares Deutschland, Bett+Bike Deutschland sowie der DEHOGA Bundesverband.\r\n                  </div>           \r\n              </div> \r\n          </div>\r\n      <div class=\"col py-1\">\r\n          <div class=\"card border-success\">  \r\n              <div class=\"card-head h5 p-2 \">\r\n                  Wer kann teilnehmen?\r\n              </div>  \r\n              <div class=\"card-body\">                  \r\n                Der Quick Check Umwelt richtet sich an Beherbergungsbetriebe der Kategorien Hotels, Pensionen, Ferienwohnungen und Ferienhäuser.\r\n            </div>\r\n            \r\n          </div> \r\n      </div>\r\n      <div class=\"col py-1\">\r\n          <div class=\"card border-success\">  \r\n              <div class=\"card-head h5 p-2 \">\r\n                  Welche Bereiche umfasst der Quick Check Umwelt?\r\n              </div>\r\n              <div class=\"card-body\">                  \r\n                  Die Berechnung umfasst die Bereiche Strom, Heizung,\r\n                  Wasser und Abfall.\r\n              </div>\r\n            \r\n          </div> \r\n      </div>\r\n      <div id=\"kosten\" class=\"col py-1 pr-4\">\r\n          <div class=\"card border-success\">  \r\n              <div class=\"card-head h5 p-2 \" >\r\n                  Wie hoch sind die Gebühren?\r\n              </div>\r\n              <div class=\"card-body\">                  \r\n                Für die Durchführung des Quick Check Umwelt wird ein\r\n                Betrag von 99,- Euro zzgl. MwSt. erhoben.\r\n              </div>\r\n            \r\n          </div> \r\n      </div>\r\n  </div>\r\n  <div class=\"row\">\r\n    <a href=\"#\" class=\"btn btn-success col\">Jetzt teilnehmen</a>\r\n  </div>\r\n  </div>\r\n</section>\r\n\r\n<app-footer></app-footer>"

/***/ }),

/***/ "./src/app/landingpage/landingpage.component.ts":
/*!******************************************************!*\
  !*** ./src/app/landingpage/landingpage.component.ts ***!
  \******************************************************/
/*! exports provided: LandingpageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LandingpageComponent", function() { return LandingpageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var LandingpageComponent = /** @class */ (function () {
    function LandingpageComponent() {
    }
    LandingpageComponent.prototype.ngOnInit = function () {
    };
    LandingpageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-landingpage',
            template: __webpack_require__(/*! ./landingpage.component.html */ "./src/app/landingpage/landingpage.component.html"),
            styles: [__webpack_require__(/*! ./landingpage.component.css */ "./src/app/landingpage/landingpage.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], LandingpageComponent);
    return LandingpageComponent;
}());



/***/ }),

/***/ "./src/app/login/index.ts":
/*!********************************!*\
  !*** ./src/app/login/index.ts ***!
  \********************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _login_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./login.component */ "./src/app/login/login.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return _login_component__WEBPACK_IMPORTED_MODULE_0__["LoginComponent"]; });




/***/ }),

/***/ "./src/app/login/login.component.html":
/*!********************************************!*\
  !*** ./src/app/login/login.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"col-md-4 col-md-offset-4 login p-3 rounded\">\r\n    <h2>Login</h2>\r\n    <form name=\"form\" (ngSubmit)=\"f.form.valid && login()\" #f=\"ngForm\" novalidate>\r\n        <div class=\"form-group\" [ngClass]=\"{ 'has-error': f.submitted && !username.valid }\">\r\n            <label for=\"username\">Email-Adresse:</label>\r\n            <input type=\"text\" class=\"form-control\" name=\"username\" [(ngModel)]=\"model.username\" #username=\"ngModel\" required />\r\n            <div *ngIf=\"f.submitted && !username.valid\" class=\"help-block\">Username is required</div>\r\n        </div>\r\n        <div class=\"form-group\" [ngClass]=\"{ 'has-error': f.submitted && !password.valid }\">\r\n            <label for=\"password\">Password</label>\r\n            <input type=\"password\" class=\"form-control\" name=\"password\" [(ngModel)]=\"model.password\" #password=\"ngModel\" required />\r\n            <div *ngIf=\"f.submitted && !password.valid\" class=\"help-block\">Password is required</div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <button [disabled]=\"loading\" class=\"btn btn-lg btn-success\">Login</button>\r\n            <img *ngIf=\"loading\" src=\"data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==\" />\r\n            <a [routerLink]=\"['/register']\" class=\"btn btn-link\">Register</a>\r\n        </div>\r\n    </form>\r\n</div>\r\n\r\n<app-footer></app-footer>\r\n"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../_services/data.service */ "./src/app/_services/data.service.ts");
/* harmony import */ var _services_index__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../_services/index */ "./src/app/_services/index.ts");





var LoginComponent = /** @class */ (function () {
    function LoginComponent(route, router, authenticationService, alertService, DataService) {
        this.route = route;
        this.router = router;
        this.authenticationService = authenticationService;
        this.alertService = alertService;
        this.DataService = DataService;
        this.model = {};
        this.loading = false;
    }
    LoginComponent.prototype.ngOnInit = function () {
        // get return url from route parameters or default to '/'
        this.returnUrl = "/eingabe";
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/eingabe';
    };
    LoginComponent.prototype.login = function () {
        var _this = this;
        this.loading = true;
        this.authenticationService.login(this.model.username, this.model.password)
            .subscribe(function (data) {
            _this.loading = false;
            _this.router.navigate([_this.returnUrl]);
        }, function (error) {
            _this.alertService.error(error);
            _this.loading = false;
        });
    };
    LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/login/login.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _services_index__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"],
            _services_index__WEBPACK_IMPORTED_MODULE_4__["AlertService"],
            _services_data_service__WEBPACK_IMPORTED_MODULE_3__["DataService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/name-editor/name-editor.component.css":
/*!*******************************************************!*\
  !*** ./src/app/name-editor/name-editor.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL25hbWUtZWRpdG9yL25hbWUtZWRpdG9yLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/name-editor/name-editor.component.html":
/*!********************************************************!*\
  !*** ./src/app/name-editor/name-editor.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  name-editor works!\n</p>\n"

/***/ }),

/***/ "./src/app/name-editor/name-editor.component.ts":
/*!******************************************************!*\
  !*** ./src/app/name-editor/name-editor.component.ts ***!
  \******************************************************/
/*! exports provided: NameEditorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NameEditorComponent", function() { return NameEditorComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var NameEditorComponent = /** @class */ (function () {
    function NameEditorComponent() {
    }
    NameEditorComponent.prototype.ngOnInit = function () {
    };
    NameEditorComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-name-editor',
            template: __webpack_require__(/*! ./name-editor.component.html */ "./src/app/name-editor/name-editor.component.html"),
            styles: [__webpack_require__(/*! ./name-editor.component.css */ "./src/app/name-editor/name-editor.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], NameEditorComponent);
    return NameEditorComponent;
}());



/***/ }),

/***/ "./src/app/navigation/navigation.component.css":
/*!*****************************************************!*\
  !*** ./src/app/navigation/navigation.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL25hdmlnYXRpb24vbmF2aWdhdGlvbi5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/navigation/navigation.component.html":
/*!******************************************************!*\
  !*** ./src/app/navigation/navigation.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- main app container -->\r\n<!--The content below is only a placeholder and can be replaced.-->\r\n<nav class=\"navbar navbar-expand-sm bg-success navbar-dark fixed-top \" style=\"color:black\" id=\"main-nav\">\r\n    <div class=\"container\">\r\n      <a href=\"index.html\" class=\"navbar-brand\">quickcheckumwelt.de</a>\r\n      <button class=\"navbar-toggler\" data-toggle=\"collapse\" data-target=\"#navbarCollapse\">\r\n        <span class=\"navbar-toggler-icon\"></span>\r\n      </button>\r\n      <div class=\"collapse navbar-collapse mx-auto\" id=\"navbarCollapse\">\r\n        <ul class=\"navbar-nav ml-auto\">\r\n          <li class=\"nav-item\">\r\n              <a href=\"/eingabe\" class=\"nav-link\">Daten eingeben</a>\r\n            </li>\r\n          <li class=\"nav-item\">\r\n              <a href=\"#explore-section\" class=\"nav-link\">Kontakt</a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n              <a class=\"nav-link btn btn-light btn-sm\" href=\"/login\" style=\"color:rgba(0,0,0,0.8)\">Login</a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n              <a class=\"nav-link\" href=\"/logout\" (click)=\"logout()\">Logout</a>\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </div>\r\n  </nav>\r\n\r\n  \r\n"

/***/ }),

/***/ "./src/app/navigation/navigation.component.ts":
/*!****************************************************!*\
  !*** ./src/app/navigation/navigation.component.ts ***!
  \****************************************************/
/*! exports provided: NavigationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavigationComponent", function() { return NavigationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_index__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../_services/index */ "./src/app/_services/index.ts");



var NavigationComponent = /** @class */ (function () {
    function NavigationComponent(authenticationService) {
        this.authenticationService = authenticationService;
    }
    NavigationComponent.prototype.ngOnInit = function () {
    };
    NavigationComponent.prototype.logout = function () {
        this.authenticationService.logout();
    };
    NavigationComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-navigation',
            template: __webpack_require__(/*! ./navigation.component.html */ "./src/app/navigation/navigation.component.html"),
            styles: [__webpack_require__(/*! ./navigation.component.css */ "./src/app/navigation/navigation.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_index__WEBPACK_IMPORTED_MODULE_2__["AuthenticationService"]])
    ], NavigationComponent);
    return NavigationComponent;
}());



/***/ }),

/***/ "./src/app/pruefung/pruefung.component.css":
/*!*************************************************!*\
  !*** ./src/app/pruefung/pruefung.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BydWVmdW5nL3BydWVmdW5nLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/pruefung/pruefung.component.html":
/*!**************************************************!*\
  !*** ./src/app/pruefung/pruefung.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\r\n  <h2>Prüfung Ihrer Eingabe</h2>\r\n  <div *ngIf=\"disabled\" style=\"font-size:12px\">bitte korrigieren Sie folgende Angaben</div>\r\n  <div *ngIf=\"disabled==false\" style=\"font-size:12px\">Ihre Eingabe ist korrekt. Sie können die Quickcheck-Umwelt jetzt abgeben:</div>\r\n \r\n    <div class=\"col-lg-3 col-md-3 col-sm-12 p-3 rounded\" *ngFor=\"let FormGroupName of FormGroupNamen;let i=index;\" class=\"card\">\r\n        <h6>{{FormGroupName}}</h6>\r\n        <div *ngFor=\"let Error of Errors[i];let a=index;\" class=\"alert alert-danger\" role=\"alert\">{{Error}}</div>\r\n    </div>\r\n  \r\n  <div class=\"card my-4\">\r\n    <Button class=\"alert btn btn-info mx-auto my-4 mx-auto\" [disabled]=\"abgabeKnopfGesperrt\" (click)=\"abgabe()\">QuickcheckUmwelt JETZT abgeben</Button>\r\n    <div *ngIf=\"valueAGBacceptance==false\" class=\"alert alert-warning\">Bitte akzeptieren Sie die AGB und bestätigen den Kauf.</div>\r\n\r\n    <mat-radio-group class=\"px-4\">\r\n        <mat-radio-button [disabled]=\"disabled\" [value]=\"valueAGBacceptance\" (click)=\"accept()\"><span>Quick Check Umwelt  jetzt kostenpflichtig für 99 Euro zzgl. MwSt. berechnen lassen. Sie erhalten mit dem Quick Check Umwelt zusammen eine Rechnung.</span>,</mat-radio-button>\r\n    </mat-radio-group>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/pruefung/pruefung.component.ts":
/*!************************************************!*\
  !*** ./src/app/pruefung/pruefung.component.ts ***!
  \************************************************/
/*! exports provided: PruefungComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PruefungComponent", function() { return PruefungComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_pruefservice_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../_services/pruefservice.service */ "./src/app/_services/pruefservice.service.ts");
/* harmony import */ var _services_data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../_services/data.service */ "./src/app/_services/data.service.ts");




var PruefungComponent = /** @class */ (function () {
    function PruefungComponent(Pruefservice, DataService) {
        this.Pruefservice = Pruefservice;
        this.DataService = DataService;
        this.valueAGBacceptance = false;
        this.abgabeKnopfGesperrt = true;
        this.disabled = true;
        this.FormGroupNamen = new Array();
        this.Errors = new Array;
        this.value = 1;
    }
    PruefungComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.Pruefservice.InvalidFormGroupOberservable.subscribe(function () {
            _this.updateErrors();
        });
    };
    PruefungComponent.prototype.updateErrors = function () {
        //was brauch ich denn?
        //dasselbe, was im Pruefservice schon steht...die Groups und die Controls als Arrays 
        //zum durchlaufen:
        console.log("pruefcomponent: udpateErros/refresh Erros");
        this.FormGroupNamen.length = 0;
        this.Errors.length = 0;
        var i = 0;
        for (var _i = 0, _a = this.Pruefservice.InvalidFormGroups; _i < _a.length; _i++) {
            var InvalidFormgroup = _a[_i];
            this.FormGroupNamen.push(InvalidFormgroup.name);
            this.Errors[i] = new Array();
            this.Errors[i] = InvalidFormgroup.FormControlNames;
            i++;
        }
        //wenn keine Fehler mehr da sind, dann stelle disabled auf false
        //der User kann jetzt abschicken, wenn er akzeptiert hat
        if (this.FormGroupNamen.length == 0) {
            //man zustimmen
            this.disabled = false;
        }
        else {
            this.disabled = true;
        }
    };
    PruefungComponent.prototype.abgabe = function () {
        if (this.valueAGBacceptance) {
            this.DataService.abgabeQuickcheckUmwelt();
        }
    };
    PruefungComponent.prototype.accept = function () {
        this.valueAGBacceptance = !(this.valueAGBacceptance);
        //wenn es keine Fehler gibt, dann stelle an:
        if (this.FormGroupNamen.length == 0 && this.valueAGBacceptance) {
            this.abgabeKnopfGesperrt = !(this.abgabeKnopfGesperrt);
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], PruefungComponent.prototype, "pruefeEingabe", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], PruefungComponent.prototype, "pruefungQuickcheckUmwelt", void 0);
    PruefungComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-pruefung',
            template: __webpack_require__(/*! ./pruefung.component.html */ "./src/app/pruefung/pruefung.component.html"),
            styles: [__webpack_require__(/*! ./pruefung.component.css */ "./src/app/pruefung/pruefung.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_pruefservice_service__WEBPACK_IMPORTED_MODULE_2__["PruefService"], _services_data_service__WEBPACK_IMPORTED_MODULE_3__["DataService"]])
    ], PruefungComponent);
    return PruefungComponent;
}());



/***/ }),

/***/ "./src/app/register/index.ts":
/*!***********************************!*\
  !*** ./src/app/register/index.ts ***!
  \***********************************/
/*! exports provided: RegisterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _register_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./register.component */ "./src/app/register/register.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "RegisterComponent", function() { return _register_component__WEBPACK_IMPORTED_MODULE_0__["RegisterComponent"]; });




/***/ }),

/***/ "./src/app/register/register.component.html":
/*!**************************************************!*\
  !*** ./src/app/register/register.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\"></div>\r\n<div class=\"col-md-12 rounded\">\r\n    <form name=\"form\" (ngSubmit)=\"f.form.valid && register()\" #f=\"ngForm\" novalidate>\r\n        <div class=\"form-group\" [ngClass]=\"{ 'has-error': f.submitted && !username.valid }\">\r\n            <label for=\"firstName\">Vorname</label>\r\n            <input type=\"text\" class=\"form-control\" name=\"firstName\" [(ngModel)]=\"model.firstName\" #firstName=\"ngModel\" required />\r\n            <div *ngIf=\"f.submitted && !firstName.valid\" class=\"help-block\">Vorname wird benötigt</div>\r\n        </div>\r\n        <div class=\"form-group\" [ngClass]=\"{ 'has-error': f.submitted && !username.valid }\">\r\n            <label for=\"lastName\">Nachname</label>\r\n            <input type=\"text\" class=\"form-control\" name=\"lastName\" [(ngModel)]=\"model.lastName\" #lastName=\"ngModel\" required />\r\n            <div *ngIf=\"f.submitted && !lastName.valid\" class=\"help-block\">Nachname wird benötigt</div>\r\n        </div>\r\n        <div class=\"form-group\" [ngClass]=\"{ 'has-error': f.submitted && !username.valid }\">\r\n            <label for=\"username\">E-Mail-Adresse</label>\r\n            <input matinput type=\"email\" minlength=\"5\" class=\"form-control\" name=\"username\" [(ngModel)]=\"model.username\" #username=\"ngModel\" required />\r\n            <div *ngIf=\"f.submitted && !username.valid\" class=\"help-block\">Bitte geben Sie eine gültige Email-Adresse ein!</div>\r\n        </div>\r\n        <div class=\"form-group\" [ngClass]=\"{ 'has-error': f.submitted && !password.valid }\">\r\n            <label for=\"password\">Password</label>\r\n            <input type=\"password\" class=\"form-control\" name=\"password\" [(ngModel)]=\"model.password\" #password=\"ngModel\" required />\r\n            <div *ngIf=\"f.submitted && !password.valid\" class=\"help-block\">Password is required</div>\r\n        </div>\r\n        <div class=\"form-group \">\r\n            <button [disabled]=\"loading\" class=\"btn btn-lg btn-light\">Jetzt anmelden</button>\r\n            <img *ngIf=\"loading\" src=\"data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==\" />\r\n            <!--<a [routerLink]=\"['/login']\" class=\"btn btn-link\">Cancel</a>-->\r\n        </div>\r\n    </form>\r\n</div>"

/***/ }),

/***/ "./src/app/register/register.component.ts":
/*!************************************************!*\
  !*** ./src/app/register/register.component.ts ***!
  \************************************************/
/*! exports provided: RegisterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterComponent", function() { return RegisterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_index__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../_services/index */ "./src/app/_services/index.ts");




var RegisterComponent = /** @class */ (function () {
    function RegisterComponent(router, userService, alertService) {
        this.router = router;
        this.userService = userService;
        this.alertService = alertService;
        this.model = {};
        this.loading = false;
    }
    RegisterComponent.prototype.register = function () {
        var _this = this;
        this.loading = true;
        this.userService.create(this.model)
            .subscribe(function (data) {
            _this.alertService.success('Registration successful', true);
            _this.router.navigate(['/login']);
        }, function (error) {
            if (error.status == 409) {
                error = "Diese Emailaddresse ist schon vergeben. Bitte wählen Sie eine andere.";
            }
            _this.alertService.error(error);
            _this.loading = false;
        });
    };
    RegisterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-register',
            template: __webpack_require__(/*! ./register.component.html */ "./src/app/register/register.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _services_index__WEBPACK_IMPORTED_MODULE_3__["UserService"],
            _services_index__WEBPACK_IMPORTED_MODULE_3__["AlertService"]])
    ], RegisterComponent);
    return RegisterComponent;
}());



/***/ }),

/***/ "./src/app/restabfall/restabfall.component.css":
/*!*****************************************************!*\
  !*** ./src/app/restabfall/restabfall.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Jlc3RhYmZhbGwvcmVzdGFiZmFsbC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/restabfall/restabfall.component.html":
/*!******************************************************!*\
  !*** ./src/app/restabfall/restabfall.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n  <div class=\"row\">\r\n    <form [formGroup]=\"RestabfalltonneFormGroup\">\r\n      <div formArrayName=\"RestabfalltonnenArray\">\r\n        <div class=\"card p-3\">\r\n           <h6 class=\"card-title\">Ist Ihnen die jährliche Abfallmenge in Tonnen bekannt?</h6>\r\n           <mat-slide-toggle class=\"\" #abfallmenge>{{_SlideToggle_abfallmengeBekannt.checked ?'Ja' : 'Nein'}}</mat-slide-toggle>\r\n        \r\n           <mat-form-field class=\"\" *ngIf=\"_SlideToggle_abfallmengeBekannt.checked\" class=\"full-width\">\r\n              <mat-label>Abfallmenge in kg pro Jahr:</mat-label>  \r\n              <input matInput type=\"number\" min=\"1\" minlength=\"1\" required [formControl]=\"abfallmenge_in_kg\">\r\n              <div matSuffix>t</div>\r\n              <div matPrefix></div>\r\n           </mat-form-field>\r\n        </div>   \r\n\r\n        <div *ngIf=\"_SlideToggle_abfallmengeBekannt.checked==false\" class=\"card p-3\">\r\n          <h6 class=\"card-title\" >Wie viele Restabfalltonnen nutzen Sie?</h6>\r\n          <div *ngIf=\"_SlideToggle_abfallmengeBekannt .checked==false\" >\r\n            <h6 class=\"fontweightnormal\">Achtung: Falls Ihre Restabfalltonne(n) auf Abruf geleert wird/werden, addieren Sie bitte das Gesamtvolumen für das gesamte Jahr und wählen Sie als Leerungsintervall „1 x im Jahr“ </h6> \r\n            <div *ngFor=\"let volumen of RestabfalltonnenArray.controls; let i=index\">\r\n            \r\n              <h6 class=\"mt-4 align-middle\" *ngIf=\"i %2 != 1\"><mat-icon>delete</mat-icon> Restabfalltonne {{(i+2)/2}}:</h6>\r\n              <h6 class=\"fontweightnormal\" *ngIf=\"i %2 != 0\">Leerungsinterervall:</h6>\r\n              <select *ngIf=\"i %2 != 0\" [formControlName]=\"i\">\r\n                  <option value=\"0\">--Bitte Auswählen--</option>\r\n                  <option *ngFor=\"let Leerungsinvervall of Leerungsintervalle; let g=index;\" [value]=\"Leerungsinvervall.AnzahlProJahr\">\r\n                      {{Leerungsinvervall.Text}}\r\n                  </option>\r\n              </select>\r\n              <label>      \r\n                <h6 class=\"h6small\" *ngIf=\"i %2 != 1\">Volumen in Litern:</h6>\r\n                <input required min=\"0\" minlength=\"1\" *ngIf=\"i %2 != 1\" type=\"number\" aria-placeholder=\"Liter\" [formControlName]=\"i\">        \r\n              </label>\r\n            </div>\r\n          </div>\r\n          <div class=\"my-4\"></div>\r\n          <button class=\"btn btn-success\" (click)=\"addRestabfalltonneFormGroups()\">Restabfalltonne hinzufügen</button>\r\n          <button class=\"btn btn-success\" (click)=\"removeRestabfalltonneFormGroups(1)\">Eine Restabfalltonne löschen</button>\r\n        </div>\r\n      </div>\r\n    </form>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/restabfall/restabfall.component.ts":
/*!****************************************************!*\
  !*** ./src/app/restabfall/restabfall.component.ts ***!
  \****************************************************/
/*! exports provided: RestabfallComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RestabfallComponent", function() { return RestabfallComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_index__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../_models/index */ "./src/app/_models/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/core */ "./node_modules/@angular/material/esm5/core.es5.js");
/* harmony import */ var _services_data_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../_services/data.service */ "./src/app/_services/data.service.ts");
/* harmony import */ var _services_pruefservice_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../_services/pruefservice.service */ "./src/app/_services/pruefservice.service.ts");
/* harmony import */ var _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/slide-toggle */ "./node_modules/@angular/material/esm5/slide-toggle.es5.js");









var RestabfallComponent = /** @class */ (function () {
    function RestabfallComponent(Dataservice, Pruefservice) {
        this.Dataservice = Dataservice;
        this.Pruefservice = Pruefservice;
        this.RestabfalltonneFormGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            RestabfalltonnenArray: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormArray"]([
                new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]),
                new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]),
            ]),
        });
        this.abfallmenge_in_kg = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
        this.BHKWRohstoff = "";
        this.counter = 1;
        this.counterString = '0';
        this.Leerungsintervalle = [{ Text: "1 x in der Woche", AnzahlProJahr: 52 }];
        this.Leerungsintervalle.push({ Text: "täglich", AnzahlProJahr: 365 });
        this.Leerungsintervalle.push({ Text: "2 x in der Woche", AnzahlProJahr: 104 });
        this.Leerungsintervalle.push({ Text: "3 x in der Woche", AnzahlProJahr: 156 });
        this.Leerungsintervalle.push({ Text: "4 x in der Woche", AnzahlProJahr: 208 });
        this.Leerungsintervalle.push({ Text: "5 x in der Woche", AnzahlProJahr: 73 });
        this.Leerungsintervalle.push({ Text: "6 x in der Woche", AnzahlProJahr: 312 });
        this.Leerungsintervalle.push({ Text: "alle zwei Wochen", AnzahlProJahr: 26 });
        this.Leerungsintervalle.push({ Text: "alle drei Wochen", AnzahlProJahr: 17.33 });
        this.Leerungsintervalle.push({ Text: "1 x im Monat", AnzahlProJahr: 12 });
        this.Leerungsintervalle.push({ Text: "11 x im Jahr", AnzahlProJahr: 11 });
        this.Leerungsintervalle.push({ Text: "10 x im Jahr", AnzahlProJahr: 10 });
        this.Leerungsintervalle.push({ Text: "9 x im Jahr", AnzahlProJahr: 9 });
        this.Leerungsintervalle.push({ Text: "8 x im Jahr", AnzahlProJahr: 8 });
        this.Leerungsintervalle.push({ Text: "7 x im Jahr", AnzahlProJahr: 7 });
        this.Leerungsintervalle.push({ Text: "6 x im Jahr", AnzahlProJahr: 6 });
        this.Leerungsintervalle.push({ Text: "5 x im Jahr", AnzahlProJahr: 5 });
        this.Leerungsintervalle.push({ Text: "4 x im Jahr", AnzahlProJahr: 4 });
        this.Leerungsintervalle.push({ Text: "3 x im Jahr", AnzahlProJahr: 3 });
        this.Leerungsintervalle.push({ Text: "2 x im Jahr", AnzahlProJahr: 2 });
        this.Leerungsintervalle.push({ Text: "1 x im Jahr", AnzahlProJahr: 1 });
        this.matcher = new _angular_material_core__WEBPACK_IMPORTED_MODULE_4__["ErrorStateMatcher"]();
    }
    Object.defineProperty(RestabfallComponent.prototype, "RestabfalltonnenArray", {
        get: function () {
            return this.RestabfalltonneFormGroup.get('RestabfalltonnenArray');
        },
        enumerable: true,
        configurable: true
    });
    RestabfallComponent.prototype.addRestabfalltonneFormGroups = function () {
        if (this.RestabfalltonnenArray.length <= 10) {
            this.counter++;
            this.counterString = String(this.counter);
            this.RestabfalltonnenArray.push(new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]));
            this.RestabfalltonnenArray.push(new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]));
        }
    };
    RestabfallComponent.prototype.addRestabfalltonneFormControlWithValue = function (AnzahlDerLeerungen, Volumen) {
        if (this.RestabfalltonnenArray.length <= 10) {
            this.RestabfalltonnenArray.push(new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](Volumen, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]));
            this.RestabfalltonnenArray.push(new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](AnzahlDerLeerungen, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]));
        }
    };
    RestabfallComponent.prototype.removeRestabfalltonneFormGroups = function (i) {
        if (this.RestabfalltonnenArray.length > 0) {
            this.RestabfalltonnenArray.removeAt(this.RestabfalltonnenArray.length - 1);
            this.RestabfalltonnenArray.removeAt(this.RestabfalltonnenArray.length - 1);
        }
    };
    RestabfallComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.Dataservice.UmweltchecksBhobj.subscribe(
        //Problem: wird einmal aufgerufnen beim Konstruktor und einmal, wenn die Datengeupdated werden.
        function (Umweltchecks) {
            console.log("RestabfallOnInit:", Umweltchecks);
            if (Umweltchecks.Umweltchecks) {
                _this.pullDatainRestabfalltonnenInRestabfallComponente();
            }
        }, function (error) { console.log("ERROR in Restabfallcomponente.", error); });
        //Listen to saveRestabfalltonnen-Event vom Parentcomponent: eingabe.component => SAVE local Data!
        this.saveRestabfalltonnen.subscribe(function () {
            console.log("Restabfalltonnen: saveRestabfalltonnen");
            //lokal: schreibe die Werte in die Tonnen, dann in den Dataservice -
            _this.saveAbfalltonnen();
            //absenden ans Backend! 
            _this.Dataservice.saveRestabfalltonnen();
        }, function (err) { console.log(err); }, function (complete) { console.log("Restabfalltonnen erfolgereich vom Frontent versendet"); });
        this.pruefeEingabe.subscribe(function () {
            setTimeout(function () {
                _this.pruefeEingabeStromRestabfallKomponente();
            }, 0);
        });
    };
    //Abspeichern der Tonnen im Dataservice:
    RestabfallComponent.prototype.saveAbfalltonnen = function () {
        //wenn die Menge bekannt ist, speicher die Menge, sonst die Tonnen
        //Dann die Einrichtung
        if (this._SlideToggle_abfallmengeBekannt.checked) {
            this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.abfall_in_kg_pro_jahr = Number(this.abfallmenge_in_kg.value);
            this.Dataservice.deleteRestabfalltonnenInDS();
            this.Dataservice.UmweltchecksBhobj.next(this.Dataservice.Umweltchecks);
            this.Dataservice.saveEinrichtung();
        }
        else {
            //setze den AbfallWert auf 0, falls da vorher was stand:
            this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.abfall_in_kg_pro_jahr = 0;
            this.Dataservice.UmweltchecksBhobj.next(this.Dataservice.Umweltchecks);
            this.Dataservice.saveEinrichtung();
            //es gibt manuelle Tonnen:  
            //Plan:
            //1. alle löschen:
            this.Dataservice.deleteRestabfalltonnenInDS();
            //2. jeden Datensatz zu einer Tonne bauen
            var volumen = 0;
            var AnzahlDerLeerungen = 0;
            var counter = 0;
            console.log("Restabfalltonnen: saveAbfalltonnen Controls:", this.RestabfalltonnenArray.controls);
            for (var _i = 0, _a = this.RestabfalltonnenArray.controls; _i < _a.length; _i++) {
                var control = _a[_i];
                //1.Volumen:
                if (counter % 2 == 0) {
                    var newRestabfalltonne = new _models_index__WEBPACK_IMPORTED_MODULE_2__["Restabfalltonne"]();
                    volumen = control.value;
                }
                //2. Leerungen:
                if (counter % 2 == 1) {
                    AnzahlDerLeerungen = control.value;
                    newRestabfalltonne.einrichtungs_id = 0;
                    newRestabfalltonne.einrichtungs_id = this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].einrichtungs_id;
                    newRestabfalltonne.volumen_in_litern = Number(volumen);
                    newRestabfalltonne.leerungen_pro_jahr = Number(AnzahlDerLeerungen);
                    console.log("newRestabfalltonne:", newRestabfalltonne);
                    //3. Tonnen puschen
                    this.Dataservice.addRestabfalltonne(newRestabfalltonne);
                }
                counter++;
            }
            this.Dataservice.UmweltchecksBhobj.next(this.Dataservice.Umweltchecks);
        }
    };
    RestabfallComponent.prototype.pullDatainRestabfalltonnenInRestabfallComponente = function () {
        var _this = this;
        //For Each Restabfalltonne, -> add() und fülle die Daten rein ;-)
        if (this.Dataservice.Umweltchecks.Umweltchecks.length > 0) {
            if (this.umweltcheckSubscription) {
                this.umweltcheckSubscription.unsubscribe();
            }
            var counter = 0;
            console.log("RestabfalltonnenArray", this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.Restabfalltonnen);
            console.log("RestabfalltonnenControls:", this.RestabfalltonnenArray.controls);
            for (var _i = 0, _a = this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.Restabfalltonnen; _i < _a.length; _i++) {
                var tonne = _a[_i];
                console.log("CountervariableRestabfalltonnen:", counter);
                //FEHLER: ich verstehe nicht, warum hier tonne nicht definiert sein kann?!
                //es gibt keine "leeren" Tonnen im Array und der counter zählt auch nicht drüber
                if (tonne) {
                    //lege ne Neue Tonne an:
                    if (counter > 1) {
                        this.addRestabfalltonneFormControlWithValue(tonne.volumen_in_litern, tonne.leerungen_pro_jahr);
                    }
                    this.RestabfalltonnenArray.controls[counter].setValue(tonne.volumen_in_litern);
                    this.RestabfalltonnenArray.controls[counter + 1].setValue(tonne.leerungen_pro_jahr);
                }
                counter = counter + 2;
            }
            //Abfalltoggle betätigen:
            if (this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.abfall_in_kg_pro_jahr > 0) {
                this._SlideToggle_abfallmengeBekannt.toggle();
                setTimeout(function () {
                    console.log("Restabfall: Setze abfall_in_kg_pro_jahr", _this.Dataservice.Umweltchecks.Umweltchecks[_this.Dataservice.NummerUmweltcheck].Einrichtung.abfall_in_kg_pro_jahr);
                    _this.abfall_in_kg_pro_jahr = _this.Dataservice.Umweltchecks.Umweltchecks[_this.Dataservice.NummerUmweltcheck].Einrichtung.abfall_in_kg_pro_jahr;
                    _this.abfallmenge_in_kg.setValue(Number(_this.abfall_in_kg_pro_jahr));
                }, 250);
            }
        }
    };
    RestabfallComponent.prototype.pruefeEingabeStromRestabfallKomponente = function () {
        //jede Heizung hat nur ein Feld
        console.log("pruef pruefeEingabeStromRestabfallKomponente");
        //Toggle getoggled?
        if (this._SlideToggle_abfallmengeBekannt.checked) {
            if (this.abfallmenge_in_kg.value == "" || this.abfall_in_kg_pro_jahr < 1) {
                this.Pruefservice.pushNameErrorArray("Abfallmenge in kg pro Jahr ist bekannt: Abfallmenge muss > 0 sein", this.RestabfalltonneFormGroup.errors);
            }
        }
        else {
            //toggle nicht gechecked
            //gehe die Tonnen durch
            var i = 1;
            //Restabfalltonne hat zwei Felder pro Tonne
            for (var _i = 0, _a = this.RestabfalltonnenArray.controls; _i < _a.length; _i++) {
                var control = _a[_i];
                //immer die ungerade Nehmen: (wir verzichten auf eine Fehler mehr)
                if (control.invalid) {
                    if (i % 2 == 1) {
                        this.Pruefservice.pushNameErrorArray("Restabfalltonne " + String(i) + ": Volumen in Litern", control.errors);
                    }
                    if (i % 2 == 0) {
                        this.Pruefservice.pushNameErrorArray("Restabfalltonne " + String(i - 1) + ": Leerungsintervall", control.errors);
                    }
                }
                i++;
            }
        }
        this.Pruefservice.pushInvalidFormgroup("Restabfälle", this.RestabfalltonneFormGroup);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], RestabfallComponent.prototype, "saveRestabfalltonnen", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], RestabfallComponent.prototype, "pruefeEingabe", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('abfallmenge'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_7__["MatSlideToggle"])
    ], RestabfallComponent.prototype, "_SlideToggle_abfallmengeBekannt", void 0);
    RestabfallComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-restabfall',
            template: __webpack_require__(/*! ./restabfall.component.html */ "./src/app/restabfall/restabfall.component.html"),
            styles: [__webpack_require__(/*! ./restabfall.component.css */ "./src/app/restabfall/restabfall.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_data_service__WEBPACK_IMPORTED_MODULE_5__["DataService"], _services_pruefservice_service__WEBPACK_IMPORTED_MODULE_6__["PruefService"]])
    ], RestabfallComponent);
    return RestabfallComponent;
}());



/***/ }),

/***/ "./src/app/strom-heizung/strom-heizung.component.css":
/*!***********************************************************!*\
  !*** ./src/app/strom-heizung/strom-heizung.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3N0cm9tLWhlaXp1bmcvc3Ryb20taGVpenVuZy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/strom-heizung/strom-heizung.component.html":
/*!************************************************************!*\
  !*** ./src/app/strom-heizung/strom-heizung.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\"> \r\n    <div class=\"row\">\r\n            <div class=\"col\">\r\n              <div class=\"form card p-2\">\r\n                    <div>                            \r\n                        <h6 class=\"card-title\">Bitte fügen Sie eine beliebige Anzahl Heizungen hinzu. Wählen Sie erst ein Heizmittel aus und klicken Sie danach auf Heizung hinzufügen.</h6>\r\n                        <mat-form-field class=\"full-width\">\r\n                            <mat-select placeholder=\"Bitte wählen Sie ein Heizmittel aus\" [formControl]=\"HeizmittelSelector\"> <!--[value]=\"heizmittelFormArray.controls.HeizmittelSelector.value\"-->\r\n                                <mat-option *ngFor=\"let heizmittelvalue of Heizmittelvalues.Heizmittelvalues; let i = index\" [value]=\"heizmittelvalue\" >\r\n                                    {{heizmittelvalue}}\r\n                                </mat-option>\r\n                           </mat-select>                                                                                \r\n                        </mat-form-field>\r\n                        <div  *ngIf=\"HeizartSelectError\" class=\"text-danger\" >Bitte wählen Sie ein Heizmittel aus!</div>       \r\n                        <div class=\"d-flex justify-content-center\">                     \r\n                           <button class=\"btn btn-success\" (click)=\"addHeizung()\">Heizung hinzufügen</button><button class=\"btn btn-success\" (click)=\"removeHeizung()\">Heizung entfernen</button>\r\n                        </div>\r\n                    </div>\r\n            </div>\r\n                    <form [formGroup]=\"heizmittelFormGroup\" class=\"card p-2\">\r\n                        <h6 class=\"card-title\">Heizungen:</h6>\r\n                        <h6 class=\"alert-info fontweightnormal p-1\">Angaben dazu, wie viel Heizenergie verbraucht wurde, finden Sie in Ihrer Heizkostenabrechnung oder der Rechnung des Energielieferanten.</h6>\r\n                        <div>\r\n                            <div *ngFor=\"let heizmittelFormControl of heizmittelFormArray.controls; let i=index\">\r\n                            <h6 *ngIf=\"HeizungsNamenArray[i]\">{{i+1}}. {{HeizungsNamenArray[i]}}:</h6>   \r\n                                <mat-form-field *ngIf=\"HeizungsNamenArray[i]\" class=\"full-width\">                                            \r\n                                            <mat-label>{{HeizungsNamenArray[i].name}}</mat-label>                                       \r\n                                            <input [errorStateMatcher]=\"matcher\" matInput type=\"number\" min=\"0\" minlength=\"1\" required [formControlName]=\"i\" [formControl]=\"heizmittelFormControl\">\r\n                                            <div matSuffix>{{Heizungen[i].einheit}}</div>\r\n                                </mat-form-field>\r\n                            </div>\r\n                        </div>\r\n                    </form>\r\n            </div>\r\n\r\n            <div class=\"col\">\r\n              <div class=\"card p-2\">\r\n                  <!-- BHWK TOGGLE -->\r\n                  <h6 class=\"card-title\">Betreiben Sie Blockheizkraftwerke?</h6>\r\n                  <mat-slide-toggle #bhkw [formControl]=\"BHWKtoggleFormControl\">{{BHWKtoggleFormControl.value ?'Ja' : 'Nein'}}</mat-slide-toggle>\r\n                  <!-- BHKW SELECTOR-->\r\n                  <div *ngIf=\"BHWKtoggleFormControl.value\">\r\n                    <h6 class=\"card-title\">Bitte fügen Sie eine beliebige Anzahl BHKW hinzu. Wählen Sie erst ein Betriebsmittel aus und klicken Sie danach auf BHKW hinzufügen.</h6>\r\n                    <mat-form-field class=\"full-width\">\r\n                        <mat-select placeholder=\"Bitte wählen Sie ein Betriebsmittel aus\" [formControl]=\"BHKWSelector\"> \r\n                            <mat-option *ngFor=\"let BHKWvalue of BHWKBetriebsmittelValues.BHKWvalues; let i = index\" [value]=\"BHKWvalue\" >\r\n                                {{BHKWvalue}}\r\n                            </mat-option>\r\n                        </mat-select>                                                                                \r\n                    </mat-form-field>\r\n                    <div *ngIf=\"BHWKBetriebsmittelSelectError\" class=\"text-danger\" >Bitte wählen Sie ein Betriebsmittel aus!</div>  \r\n                    <div  class=\"d-flex justify-content-center\">                          \r\n                        <button class=\"btn btn-success\" (click)=\"addBHKW()\">BHKW hinzufügen</button><button class=\"btn btn-success\"(click)=\"removeBHKW()\">BHKW löschen</button>\r\n                    </div>\r\n                </div>\r\n\r\n              </div>\r\n              <form  class=\"form card p-2\" [formGroup]=\"BHKWFormGroup\">\r\n                  <!--BHKWS for Loop-->\r\n                  <div *ngIf=\"BHWKtoggleFormControl.value\">\r\n                        <form [formGroup]=\"BHKWAnlagenFormGroup\">\r\n                                <div formArrayName=\"BHKWFormArray\">\r\n                                    <div *ngFor=\"let BHKWFormControl of BHKWAnlagenFormGroup.controls.BHKWFormArray.controls; let i=index\">\r\n                                    <h6 *ngIf=\"i %2 != 1\">{{(i+1)/2+0.5}}. {{BHKWNamenArray[i]}}</h6>\r\n                                        \r\n                                        <mat-form-field *ngIf=\"i %2 != 1\" class=\"full-width\">\r\n                                                    <mat-label>BHWK Eigenverbrauch Strom:</mat-label>  \r\n                                                    <input [errorStateMatcher]=\"matcher\" matInput type=\"number\" minlength=\"1\" required [formControlName]=\"i\" [formControl]=\"BHKWFormControl\">\r\n                                                    <div matSuffix>kWh</div>\r\n                                                    <div matPrefix></div>\r\n                                        </mat-form-field> \r\n                                        <mat-form-field *ngIf=\"i %2 != 0\"class=\"full-width\">\r\n                                                    <mat-label>BHWK Eingespeister Strom:</mat-label>\r\n                                                    <input [errorStateMatcher]=\"matcher\" matInput type=\"number\" minlength=\"1\" required [formControlName]=\"i\" [formControl]=\"BHKWFormControl \">\r\n                                                    <div matSuffix>kWh</div>\r\n                                                    <div matPrefix></div>\r\n                                        </mat-form-field> \r\n                                    </div>\r\n                                </div>\r\n                            </form>\r\n                  </div>\r\n              </form>\r\n            \r\n              <form class=\"form card p-2\" [formGroup]=\"PhotovoltaikFormGroup\">\r\n                  <h6 class=\"card-title\">Haben Sie eine Photovoltaikanlage?</h6>\r\n                  <mat-slide-toggle #photo formControlName=\"PhotovoltaikToggleFormControl\" [formControl]=\"PhotovoltaikFormGroup.controls.PhotovoltaikToggleFormControl\">{{PhotovoltaikFormGroup.controls.PhotovoltaikToggleFormControl.value ?'Ja' : 'Nein'}}</mat-slide-toggle>\r\n                  <div *ngIf=\"PhotovoltaikFormGroup.controls.PhotovoltaikToggleFormControl.value\">\r\n                    <h6>Eigenverbrauch Strom:</h6>\r\n                    <mat-form-field class=\"full-width\">\r\n                                <input [errorStateMatcher]=\"matcher\" matInput type=\"number\" minlength=\"1\" required formControlName=\"PhotovoltaikEigenverbrauchFormControl\" [formControl]=\"PhotovoltaikFormGroup.controls.PhotovoltaikEigenverbrauchFormControl\">\r\n                                <div matSuffix>kWh</div>\r\n                                <div matPrefix></div>\r\n                    </mat-form-field> \r\n                    <h6>Eingespeister Strom:</h6>\r\n                    <mat-form-field class=\"full-width\">\r\n                                <input [errorStateMatcher]=\"matcher\" matInput type=\"number\" minlength=\"1\" required formControlName=\"PhotovoltaikEingespeistFormControl\" [formControl]=\"PhotovoltaikFormGroup.controls.PhotovoltaikEingespeistFormControl\">\r\n                                <div matSuffix>kWh</div>\r\n                                <div matPrefix></div>\r\n                    </mat-form-field> \r\n                  </div>\r\n                </form>\r\n            </div>    \r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/strom-heizung/strom-heizung.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/strom-heizung/strom-heizung.component.ts ***!
  \**********************************************************/
/*! exports provided: StromHeizungComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StromHeizungComponent", function() { return StromHeizungComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_index__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../_models/index */ "./src/app/_models/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/core */ "./node_modules/@angular/material/esm5/core.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_data_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../_services/data.service */ "./src/app/_services/data.service.ts");
/* harmony import */ var _services_pruefservice_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../_services/pruefservice.service */ "./src/app/_services/pruefservice.service.ts");









var StromHeizungComponent = /** @class */ (function () {
    function StromHeizungComponent(Dataservice, Pruefservice) {
        this.Dataservice = Dataservice;
        this.Pruefservice = Pruefservice;
        //HEIZUNG:
        this.Heizungen = [];
        this.Heizmittelnamen = new _models_index__WEBPACK_IMPORTED_MODULE_2__["Heizmittelnamen"]();
        this.HeizmittelSelector = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', []);
        this.HeizungsNamenArray = [];
        this.HeizartSelectError = false;
        this.heizmittelFormArray = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormArray"]([]);
        this.heizmittelFormGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            heizmittelFormArray: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormArray"]([
            //new FormControl('',[Validators.required]),
            ]),
        });
        //BHKW:
        this.BHWKBetriebsmittelValues = new _models_index__WEBPACK_IMPORTED_MODULE_2__["BHKWvalues"]();
        this.BHKWSelector = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', []);
        this.BHWKBetriebsmittelSelectError = false;
        this.BHKWFormArray = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormArray"]([]);
        this.BHKWs = [];
        this.BHKWNamenArray = [];
        this.BHKWRohstoff = "notSet";
        this.BHKWAnlagenFormGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            BHKWFormArray: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormArray"]([])
        });
        console.log("BHKW-FormGroup:", this.BHKWAnlagenFormGroup);
        this.BHKWFormGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            BHWKtoggleFormControl: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', []),
        });
        this.BHWKtoggleFormControl = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', []),
            //Photovoltaik:
            this.PhotovoltaikFormGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
                PhotovoltaikToggleFormControl: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
                PhotovoltaikEigenverbrauchFormControl: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]),
                PhotovoltaikEingespeistFormControl: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]),
            });
        this.matcher = new _angular_material_core__WEBPACK_IMPORTED_MODULE_4__["ErrorStateMatcher"]();
    }
    StromHeizungComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log("HeizmittelFormGroup:", this.heizmittelFormGroup);
        this.heizungsCounter = 0;
        this.Dataservice.UmweltchecksBhobj.subscribe(
        //Problem: wird einmal aufgerufnen beim Konstruktor und einmal, wenn die Datengeupdated werden.
        function (Umweltchecks) {
            console.log("BETRIEBSDATENCOMPONENTE:", Umweltchecks);
            if (Umweltchecks.Umweltchecks) {
                _this.pullDataInStromHeizungComponente();
            }
        }, function (error) { console.log("ERROR in Betriebsdatencomponente.", error); });
        //Listen to saveRestabfalltonnen-Event vom Parentcomponent: eingabe.component => SAVE local Data!
        this.saveStromHeizung.subscribe(function () {
            console.log("Anlagen: saveAnlagen");
            //lokal: schreibe die Werte in die Tonnen, dann in den Dataservice -
            _this.saveAnlagen();
        }, function (err) { console.log(err); }, function (complete) { console.log("Anlagen erfolgereich vom Frontent versendet"); });
        this.pruefeEingabe.subscribe(function () {
            console.log("pruefe StromHeizung");
            _this.pruefeEingabeStromHeizungKomponente();
        });
        this.Heizmittelvalues = new _models_index__WEBPACK_IMPORTED_MODULE_2__["Heizmittelvalue"]();
        console.log("Heizmittelvalues", this.Heizmittelvalues.Heizmittelvalues);
    };
    StromHeizungComponent.prototype.addHeizung = function () {
        console.log("ADDHEIZUNG", this.HeizmittelSelector.value);
        if ((this.Heizungen.length <= 20) && (this.HeizmittelSelector.value != null)) {
            this.heizungsCounter++;
            this.HeizartSelectError = false;
            var NewHeizung = new _models_index__WEBPACK_IMPORTED_MODULE_2__["Anlage"]();
            NewHeizung.typ = this.HeizmittelSelector.value;
            NewHeizung.name = NewHeizung.typ.replace("ae", "ä");
            NewHeizung.name = NewHeizung.typ.replace("oe", "ö");
            NewHeizung.name = NewHeizung.typ.replace("ue", "ü");
            if (NewHeizung.typ.includes("kWh")) {
                //console.log("HEIZ Anlagentyp:",anlage.typ,"Heizmittelvalues:",Heizungsvalues.Heizmittelvalues[i],"Heizmittelname:",HeizungsNamen.Heizmittelnamen[i])
                NewHeizung.einheit = "kWh";
            }
            if (NewHeizung.typ.includes("Kg")) {
                //console.log("HEIZ Anlagentyp:",anlage.typ,"Heizmittelvalues:",Heizungsvalues.Heizmittelvalues[i],"Heizmittelname:",HeizungsNamen.Heizmittelnamen[i])
                NewHeizung.einheit = "kg";
            }
            if (NewHeizung.typ.includes("Liter")) {
                //console.log("HEIZ Anlagentyp:",anlage.typ,"Heizmittelvalues:",Heizungsvalues.Heizmittelvalues[i],"Heizmittelname:",HeizungsNamen.Heizmittelnamen[i])
                NewHeizung.einheit = "Liter";
            }
            if (NewHeizung.typ.includes("in t")) {
                //console.log("HEIZ Anlagentyp:",anlage.typ,"Heizmittelvalues:",Heizungsvalues.Heizmittelvalues[i],"Heizmittelname:",HeizungsNamen.Heizmittelnamen[i])
                NewHeizung.einheit = "t";
            }
            this.Heizungen.push(NewHeizung);
            this.HeizungsNamenArray.push(String(NewHeizung.name));
            this.heizmittelFormArray.controls.push(new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]));
            this.heizmittelFormGroup.controls.heizmittelFormArray = this.heizmittelFormArray;
            console.log("HeizmittelControls", this.heizmittelFormGroup.controls.heizmittelFormArray);
            console.log("Heizungen:", this.Heizungen);
        }
        else {
            this.HeizartSelectError = true;
        }
        this.HeizmittelSelector.reset();
    };
    StromHeizungComponent.prototype.removeHeizung = function () {
        if (this.Heizungen.length > 0) {
            this.HeizungsNamenArray.pop();
            this.heizmittelFormArray.removeAt(this.heizmittelFormArray.length - 1);
            this.heizmittelFormGroup.controls.heizmittelFormArray = this.heizmittelFormArray;
            console.log("Heizung löschen:", this.heizmittelFormArray.length);
            this.Heizungen.pop();
        }
    };
    StromHeizungComponent.prototype.addBHKW = function () {
        console.log("BHKW-Selektorvalue:", this.BHKWSelector.value);
        if ((this.BHKWs.length <= 20) && ((this.BHKWSelector.value != null) && (this.BHKWSelector.value != ""))) {
            this.BHKWNamenArray.push(this.BHKWSelector.value);
            this.BHKWNamenArray.push(this.BHKWSelector.value);
            //zwei Reinpuschen: 1. Eigenverbrauchter Strom; 2. Eingespeister Strom:
            this.BHKWFormArray.push(new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]));
            this.BHKWFormArray.push(new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]));
            this.BHKWAnlagenFormGroup.controls.BHKWFormArray = this.BHKWFormArray;
            var NewBHKW = new _models_index__WEBPACK_IMPORTED_MODULE_2__["Anlage"]();
            NewBHKW.name = this.BHKWSelector.value;
            NewBHKW.typ = this.BHKWSelector.value;
            this.BHKWs.push(NewBHKW);
            this.BHKWFormGroup.controls.BHKWFormArray = this.BHKWFormArray;
            console.log("BHKW-addBHKW():", this.BHKWs);
        }
        this.BHKWSelector.reset();
    };
    StromHeizungComponent.prototype.removeBHKW = function () {
        this.BHKWFormArray.removeAt(this.BHKWFormArray.length - 1);
        this.BHKWFormArray.removeAt(this.BHKWFormArray.length - 1);
        this.BHKWFormGroup.controls.BHKWFormArray = this.BHKWFormArray;
        this.BHKWs.pop();
    };
    StromHeizungComponent.prototype.pullDataInStromHeizungComponente = function () {
        var _this = this;
        if (this.Dataservice.Umweltchecks.Umweltchecks.length > 0) {
            if (this.umweltcheckSubscription) {
                this.umweltcheckSubscription.unsubscribe();
            }
            //HEIZUNGEN alle Werte reinschmeißen aus dem Dataservice:
            this.Heizungen = this.Dataservice.getAnlagenAusAnlangenArray('Heizung');
            console.log("Heizungen anzuzeigende:", this.Heizungen);
            //2. pushe das heizmittelFormGroup.controls.heizmittelFormArray.controls mit values
            for (var _i = 0, _a = this.Heizungen; _i < _a.length; _i++) {
                var Heizung = _a[_i];
                this.heizmittelFormArray.push(new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](Heizung.rohstoff_verbrauch, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]));
                this.heizmittelFormGroup.controls.heizmittelFormArray = this.heizmittelFormArray;
                this.HeizungsNamenArray.push(Heizung.name);
            }
            console.log("HeizmittelFormArray : ", this.heizmittelFormArray);
            //BHKW get alle Werte:
            this.BHKWs = this.Dataservice.getAnlagenAusAnlangenArray('BHKW');
            console.log("BHKWs: ", this.BHKWs);
            //iteriert durch alle BHWK und toggelt, wenn es Werte gibt
            //und setzt den Wert vom letzten
            var i = 0;
            var toggled = false;
            if (this.BHKWs.length > 0) {
                for (var _b = 0, _c = this.BHKWs; _b < _c.length; _b++) {
                    var BHKW = _c[_b];
                    if (((BHKW.strom_eigenverbrauch > 0) || (BHKW.strom_netzeinspeisung) > 0) && toggled == false) {
                        this._SlideToggle_bhkw.toggle();
                        toggled = true;
                    }
                    if (((BHKW.strom_eigenverbrauch > 0) || (BHKW.strom_netzeinspeisung) > 0)) {
                        //Werte setzen nach 1 Sek:c
                        //eine Sekunde warten, weil ng-if das erst in die DOM laden muss #AngularFail
                        //Erstelle neues BHKW:
                        setTimeout(function (BHKW) {
                            console.log("BHKW LADEN:", i, ": ", BHKW);
                            _this.BHKWNamenArray.push(BHKW.name);
                            _this.BHKWNamenArray.push(BHKW.name);
                            _this.BHKWFormArray.push(new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](BHKW.strom_eigenverbrauch, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]));
                            _this.BHKWFormArray.push(new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](BHKW.strom_netzeinspeisung, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]));
                            _this.BHKWAnlagenFormGroup.controls.BHKWFormArray = _this.BHKWFormArray;
                            //console.log("BHKWs - Set: :",this.BHKWAnlagenFormGroup.controls.BHKWFormArray)
                        }, 500, BHKW);
                    }
                    i++;
                }
            }
            //Photovoltaik-Toggle:
            var Photovoltaik = this.Dataservice.getAnlagenAusAnlangenArray('Photovoltaik');
            console.log("PHOTOVOLTAIK#####: ", Photovoltaik);
            //iteriert durch alle BHWK und toggelt, wenn es Werte gibt
            if ((Photovoltaik.length > 0) && (Photovoltaik[0].strom_netzeinspeisung || Photovoltaik[0].strom_eigenverbrauch > 0)) {
                this._SlideToggle_photo.toggle();
                //Werte setzen nach 1 Sek:c
                //eine Sekunde warten, weil ng-if das erst in die DOM laden muss #AngularFail
                setTimeout(function (Photovoltaik) {
                    _this.PhotovoltaikFormGroup.controls.PhotovoltaikEigenverbrauchFormControl.setValue(Photovoltaik.strom_eigenverbrauch);
                    _this.PhotovoltaikFormGroup.controls.PhotovoltaikEingespeistFormControl.setValue(Photovoltaik.strom_netzeinspeisung);
                }, 500, Photovoltaik[0]);
            }
            //Photovoltaik-toggle
        }
    };
    //schreibt die Werte aus den Eingabeforms in die Anlageobjekte und speichert die ab:
    StromHeizungComponent.prototype.saveAnlagen = function () {
        //alle Anlagen löschen
        this.Dataservice.deleteAnlagen();
        var anlagen = [];
        //alle Heizungen in Dataservice puschen:
        this.Dataservice.addAnlagen(this.Heizungen);
        //Heizung: einmal alle Heizungen im Heizungsarray durchgehen und die Werte zuweisen:j
        var i = 0;
        for (var _i = 0, _a = this.heizmittelFormArray.controls; _i < _a.length; _i++) {
            var HeizmittelControl = _a[_i];
            if (HeizmittelControl.value > 0) {
                this.Heizungen[i].rohstoff_verbrauch = Number(HeizmittelControl.value);
            }
            this.Heizungen[i].strom_eigenverbrauch = 0;
            this.Heizungen[i].strom_netzeinspeisung = 0;
            i++;
        }
        anlagen = anlagen.concat(this.Heizungen);
        //BHKW: 
        //wenn toggle gechecked, speicher den shit:
        if (this._SlideToggle_bhkw.checked) {
            i = 0;
            var BHKWindex = 0;
            //einmal alle BHKW durchgehen und value saven
            for (var _b = 0, _c = this.BHKWFormArray.controls; _b < _c.length; _b++) {
                var BHKWControl = _c[_b];
                if (Number(BHKWControl.value) > 0) {
                    //Problem: hier sind 2 Formcontrols für 1 BHWK zuständig
                    //also immer durch 2 rechnen:
                    if (i % 2 == 1) {
                        this.BHKWs[BHKWindex].strom_netzeinspeisung = Number(BHKWControl.value);
                    }
                    if (i % 2 == 0) {
                        BHKWindex = i / 2;
                        this.BHKWs[i / 2].strom_eigenverbrauch = Number(BHKWControl.value);
                    }
                    //für: 0
                    if (i == 0) {
                        BHKWindex = 0;
                        this.BHKWs[i].strom_netzeinspeisung = Number(BHKWControl.value);
                    }
                    //nullvalues setzten:
                    this.BHKWs[BHKWindex].rohstoff_verbrauch = 0;
                }
                i++;
            }
        }
        anlagen = anlagen.concat(this.BHKWs);
        //Photovoltaikanlage
        //toggle gechecked:
        if (this._SlideToggle_photo.checked) {
            var anlage = new _models_index__WEBPACK_IMPORTED_MODULE_2__["Anlage"]();
            anlage.typ = "Photovoltaik";
            anlage.strom_netzeinspeisung = Number(this.PhotovoltaikFormGroup.controls.PhotovoltaikEingespeistFormControl.value);
            anlage.strom_eigenverbrauch = Number(this.PhotovoltaikFormGroup.controls.PhotovoltaikEigenverbrauchFormControl.value);
            anlage.rohstoff_verbrauch = 0;
            anlagen.push(anlage);
        }
        console.log("ANLAGEN Strom-Heizung-Componente:", anlagen);
        //Daten im Dataservice speichern: 
        this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.Anlagen = anlagen;
        this.Dataservice.UmweltchecksBhobj.next(this.Dataservice.Umweltchecks);
        //Dataservice wegschicken an Backend: 
        this.Dataservice.saveAnlagen();
    };
    StromHeizungComponent.prototype.pruefeEingabeStromHeizungKomponente = function () {
        //jede Heizung hat nur ein Feld
        var i = 0;
        for (var _i = 0, _a = this.heizmittelFormArray.controls; _i < _a.length; _i++) {
            var control = _a[_i];
            if (control.invalid) {
                this.Pruefservice.pushNameErrorArray(this.Heizungen[i].name, control.errors);
            }
            i++;
        }
        this.Pruefservice.pushInvalidFormgroup("Heizungen", this.heizmittelFormGroup);
        if (this._SlideToggle_bhkw.checked) {
            //BHKW hat zwei Felder pro BHKW
            i = 1;
            for (var _b = 0, _c = this.BHKWFormArray.controls; _b < _c.length; _b++) {
                var control2 = _c[_b];
                //immer die ungerade Nehmen: (wir verzichten auf eine Fehler mehr)
                if (control2.invalid) {
                    if (i % 2 == 1) {
                        this.Pruefservice.pushNameErrorArray("BHKW " + String(i - 1) + ": Eigenverbrauch", control2.errors);
                    }
                    if (i % 2 == 0) {
                        this.Pruefservice.pushNameErrorArray("BHKW " + String(i - 2) + ": eingespeißt", control2.errors);
                    }
                }
                i++;
            }
            this.Pruefservice.pushInvalidFormgroup("BHKW", this.BHKWFormGroup);
        }
        if (this._SlideToggle_photo.checked) {
            if (this.PhotovoltaikFormGroup.controls.PhotovoltaikEigenverbrauchFormControl.invalid) {
                this.Pruefservice.pushNameErrorArray("Photovoltaik Strom Eigenverbrauch", this.PhotovoltaikFormGroup.controls.PhotovoltaikEigenverbrauchFormControl.errors);
            }
            if (this.PhotovoltaikFormGroup.controls.PhotovoltaikEingespeistFormControl.invalid) {
                this.Pruefservice.pushNameErrorArray("Photovoltaik Strom eingespeißt", this.PhotovoltaikFormGroup.controls.PhotovoltaikEingespeistFormControl.errors);
            }
            this.Pruefservice.pushInvalidFormgroup("Photovoltaikanlage", this.PhotovoltaikFormGroup);
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], StromHeizungComponent.prototype, "saveStromHeizung", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], StromHeizungComponent.prototype, "pruefeEingabe", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('bhkw'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSlideToggle"])
    ], StromHeizungComponent.prototype, "_SlideToggle_bhkw", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('photo'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSlideToggle"])
    ], StromHeizungComponent.prototype, "_SlideToggle_photo", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('selectDropdownHeizung'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSelect"])
    ], StromHeizungComponent.prototype, "_selectDropdown", void 0);
    StromHeizungComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-strom-heizung',
            template: __webpack_require__(/*! ./strom-heizung.component.html */ "./src/app/strom-heizung/strom-heizung.component.html"),
            styles: [__webpack_require__(/*! ./strom-heizung.component.css */ "./src/app/strom-heizung/strom-heizung.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_data_service__WEBPACK_IMPORTED_MODULE_6__["DataService"], _services_pruefservice_service__WEBPACK_IMPORTED_MODULE_7__["PruefService"]])
    ], StromHeizungComponent);
    return StromHeizungComponent;
}());



/***/ }),

/***/ "./src/app/strom/strom.component.css":
/*!*******************************************!*\
  !*** ./src/app/strom/strom.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3N0cm9tL3N0cm9tLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/strom/strom.component.html":
/*!********************************************!*\
  !*** ./src/app/strom/strom.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\"> \r\n  <div class=\"row\">        \r\n    <div class=\"col-md-12 col-sm-12\">\r\n        <form class=\"form card p-2\" [formGroup]=\"StromFormGroup\"> \r\n            <h6>Wie viel kWh Strom verbrauchen Sie im Jahr?</h6>\r\n            <mat-form-field class=\"full-width\">\r\n                        <input [errorStateMatcher]=\"matcher\" matInput type=\"number\" min=\"0\" minlength=\"1\" required placeholder=\"Stromverbrauch in kWh pro Jahr\" formControlName=\"stromverbrauch_in_kwh\" [formControl]=\"StromFormGroup.controls.stromverbrauch_in_kwh\">\r\n                        <span matSuffix>kWh</span>\r\n                        <mat-icon *ngIf=\"StromFormGroup.controls.stromverbrauch_in_kwh.valid\" class=\"success-icon\" matSuffix >done</mat-icon>\r\n            </mat-form-field>\r\n            <h6>Wie viel Prozent Ökostrom beziehen Sie?</h6>\r\n            <h6 class=\"fontweightnormal\">Den Anteil des Ökostroms entnehmen Sie der Stromkennzeichnung, die in der Regel als Anlage Ihrer Abrechnung beigefügt ist.</h6>        \r\n        \r\n            <mat-form-field class=\"full-width\">\r\n                        <input [errorStateMatcher]=\"matcher\" matInput type=\"number\" min=\"0\" max=\"100\" minlength=\"1\" required placeholder=\"% Ökostrom\" formControlName=\"oekostromverbrauch_in_kwh\" [formControl]=\"StromFormGroup.controls.oekostromverbrauch_in_kwh\">\r\n                        <span matSuffix>%</span>\r\n                        <mat-hint>also: {{StromFormGroup.controls.oekostromverbrauch_in_kwh.value * StromFormGroup.controls.stromverbrauch_in_kwh.value/100 }} kWh</mat-hint>\r\n                        <mat-icon *ngIf=\"StromFormGroup.controls.oekostromverbrauch_in_kwh.valid\" class=\"success-icon\" matSuffix >done</mat-icon>\r\n            </mat-form-field>\r\n        </form>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/strom/strom.component.ts":
/*!******************************************!*\
  !*** ./src/app/strom/strom.component.ts ***!
  \******************************************/
/*! exports provided: StromComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StromComponent", function() { return StromComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_pruefservice_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../_services/pruefservice.service */ "./src/app/_services/pruefservice.service.ts");
/* harmony import */ var _services_data_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../_services/data.service */ "./src/app/_services/data.service.ts");
/* harmony import */ var _angular_material_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/core */ "./node_modules/@angular/material/esm5/core.es5.js");






var StromComponent = /** @class */ (function () {
    function StromComponent(Dataservice, Pruefservice) {
        this.Dataservice = Dataservice;
        this.Pruefservice = Pruefservice;
        this.matcher = new _angular_material_core__WEBPACK_IMPORTED_MODULE_5__["ErrorStateMatcher"]();
        this.StromFormGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            stromverbrauch_in_kwh: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            oekostromverbrauch_in_kwh: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].min(0), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].max(100), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(1)]),
        });
    }
    StromComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.Dataservice.UmweltchecksBhobj.subscribe(
        //Problem: wird einmal aufgerufnen beim Konstruktor und einmal, wenn die Datengeupdated werden.
        function (Umweltchecks) {
            console.log("Strom-COMPONENTE:", Umweltchecks);
            if (Umweltchecks.Umweltchecks) {
                _this.pullDataInStromComponente();
            }
        }, function (error) { console.log("ERROR in Stromcomponente.", error); });
        this.saveStrom.subscribe(function () {
            _this.saveStromInDataservice();
        }, function (err) { console.log(err); }, function (complete) { console.log("Strom erfolgereich vom Frontent versendet"); });
        this.pruefeEingabe.subscribe(function () {
            _this.pruefeStromComponente();
        });
    };
    StromComponent.prototype.pullDataInStromComponente = function () {
        if (this.Dataservice.Umweltchecks.Umweltchecks.length > 0) {
            if (this.umweltcheckSubscription) {
                this.umweltcheckSubscription.unsubscribe();
            }
            this.StromFormGroup.controls.stromverbrauch_in_kwh.setValue(Number(this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.stromverbrauch_in_kwh) + Number(this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.oekostromverbrauch_in_kwh));
            this.StromFormGroup.controls.oekostromverbrauch_in_kwh.setValue((Number(this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.oekostromverbrauch_in_kwh)) / (Number(this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.stromverbrauch_in_kwh) + Number(this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.oekostromverbrauch_in_kwh)) * 100);
        }
    };
    StromComponent.prototype.saveStromInDataservice = function () {
        if (this.umweltcheckSubscription) {
            this.umweltcheckSubscription.unsubscribe();
        }
        if (this.Dataservice.Umweltchecks.Umweltchecks.length > 0) {
            //UPDATE Einrichtung:
            //im Backend wird immer zusamment mit einem Umweltcheck eine Einrichtung angelegt
            //deswegen können wir die Einrichtung immer Updaten und müssen Sie nicht neu anlegen
            this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.oekostromverbrauch_in_kwh = Number(this.StromFormGroup.controls.oekostromverbrauch_in_kwh.value) * Number(this.StromFormGroup.controls.stromverbrauch_in_kwh.value) / 100;
            this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.stromverbrauch_in_kwh = Number(this.StromFormGroup.controls.stromverbrauch_in_kwh.value) - this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.oekostromverbrauch_in_kwh;
        }
    };
    StromComponent.prototype.pruefeStromComponente = function () {
        if (this.StromFormGroup.controls.stromverbrauch_in_kwh.invalid)
            this.Pruefservice.pushNameErrorArray("Stromverbrauch in kWh", this.StromFormGroup.controls.stromverbrauch_in_kwh.errors);
        if (this.StromFormGroup.controls.oekostromverbrauch_in_kwh.invalid)
            this.Pruefservice.pushNameErrorArray("Ökostromverbrauch in %", this.StromFormGroup.controls.oekostromverbrauch_in_kwh.errors);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], StromComponent.prototype, "saveStrom", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], StromComponent.prototype, "pruefeEingabe", void 0);
    StromComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-strom',
            template: __webpack_require__(/*! ./strom.component.html */ "./src/app/strom/strom.component.html"),
            styles: [__webpack_require__(/*! ./strom.component.css */ "./src/app/strom/strom.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_data_service__WEBPACK_IMPORTED_MODULE_4__["DataService"], _services_pruefservice_service__WEBPACK_IMPORTED_MODULE_3__["PruefService"]])
    ], StromComponent);
    return StromComponent;
}());



/***/ }),

/***/ "./src/app/test/test.component.css":
/*!*****************************************!*\
  !*** ./src/app/test/test.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Rlc3QvdGVzdC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/test/test.component.html":
/*!******************************************!*\
  !*** ./src/app/test/test.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"example-container\">\n    <mat-form-field>\n      <input matInput placeholder=\"Input\">\n    </mat-form-field>\n  \n    <mat-form-field>\n      <textarea matInput placeholder=\"Textarea\"></textarea>\n    </mat-form-field>\n  \n    <mat-form-field>\n      <mat-select placeholder=\"Select\">\n        <mat-option value=\"option\">Option</mat-option>\n      </mat-select>\n    </mat-form-field>\n  </div>"

/***/ }),

/***/ "./src/app/test/test.component.ts":
/*!****************************************!*\
  !*** ./src/app/test/test.component.ts ***!
  \****************************************/
/*! exports provided: TestComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestComponent", function() { return TestComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


//import {InputErrorStateMatcherExample} from './input-error-state-matcher-example';
var TestComponent = /** @class */ (function () {
    function TestComponent() {
    }
    TestComponent.prototype.ngOnInit = function () {
    };
    TestComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-test',
            template: __webpack_require__(/*! ./test.component.html */ "./src/app/test/test.component.html"),
            styles: [__webpack_require__(/*! ./test.component.css */ "./src/app/test/test.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], TestComponent);
    return TestComponent;
}());



/***/ }),

/***/ "./src/app/wasser/wasser.component.css":
/*!*********************************************!*\
  !*** ./src/app/wasser/wasser.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3dhc3Nlci93YXNzZXIuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/wasser/wasser.component.html":
/*!**********************************************!*\
  !*** ./src/app/wasser/wasser.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\"> \r\n  <div class=\"row\">        \r\n    <div class=\"col-md-12 col-sm-12\">\r\n    <form class=\"form card p-2\" [formGroup]=\"WasserFormGroup\">  \r\n        <h6>Wie viel m³ Wasser verbrauchen Sie im Jahr?</h6>\r\n        <mat-form-field  class=\"full-width\">\r\n                    <input [errorStateMatcher]=\"matcher\" matInput type=\"number\" min=\"0\" minlength=\"1\" required placeholder=\"Wasserverbrauch in m³ pro Jahr\" formControlName=\"wasserverbrauch_in_m3\" [formControl]=\"WasserFormGroup.controls.wasserverbrauch_in_m3\">\r\n                    <span matSuffix>m³</span>\r\n                    <mat-icon *ngIf=\"WasserFormGroup.controls.wasserverbrauch_in_m3.valid\" class=\"success-icon\" matSuffix >done</mat-icon>\r\n        </mat-form-field>\r\n    </form>\r\n    </div>\r\n    <div class=\"col-md-12 col-sm-12\">\r\n        <form class=\"form card p-2\"  [formGroup]=\"wellnessbereichFormGroup\">\r\n            <h6>Haben Sie einen Wellnessbereich?</h6>\r\n            <mat-slide-toggle #w [color]=\"color\"  formControlName=\"checkedFormControl\" type=\"checkbox\" [checked]=\"wellnessbereichFormGroup.controls.checkedFormControl.value\" [formControl]=\"wellnessbereichFormGroup.controls.checkedFormControl\"> {{wellnessbereichFormGroup.controls.checkedFormControl.value ? 'Ja' : 'Nein'}}</mat-slide-toggle>\r\n            <div *ngIf=\"wellnessbereichFormGroup.controls.checkedFormControl.value\">\r\n                <h6>Wie groß ist der Wellnessbereich in m²?</h6>\r\n                <mat-form-field class=\"full-width\">\r\n                            <input [errorStateMatcher]=\"matcher\" matInput type=\"number\" min=\"0\" minlength=\"1\" required placeholder=\"Gesamtfläche Wellnessbereich in m²\" formControlName=\"groesse_in_m2\" [formControl]=\"wellnessbereichFormGroup.controls.groesse_in_m2\">\r\n                            <span matSuffix>m²</span>\r\n                            <mat-icon *ngIf=\"wellnessbereichFormGroup.controls.groesse_in_m2.valid\" class=\"success-icon\" matSuffix >done</mat-icon>                                \r\n                </mat-form-field>\r\n\r\n                <h6>Welches Volumen hat das Schwimmbad? <br> Bei mehreren Schwimmbädern geben Sie bitte das Gesamtvolumen an.</h6>\r\n                <mat-form-field class=\"full-width\">\r\n                            <input [errorStateMatcher]=\"matcher\" matInput type=\"number\" min=\"0\" minlength=\"1\" placeholder=\"Volumen Schwimmbad in m³\" formControlName=\"volumen_schwimmbad_in_m3\" [formControl]=\"wellnessbereichFormGroup.controls.volumen_schwimmbad_in_m3\">\r\n                            <span matSuffix>m³</span>                          \r\n                </mat-form-field>\r\n                \r\n                <h6>Wie viele Saunen hat Ihr Wellnessbereich?</h6>\r\n                <mat-form-field class=\"full-width\">\r\n                            <input [errorStateMatcher]=\"matcher\" matInput type=\"number\" min=\"0\" minlength=\"1\" required placeholder=\"Anzahl der Saunen insgesamt\" formControlName=\"anzahl_der_saunen\" [formControl]=\"wellnessbereichFormGroup.controls.anzahl_der_saunen\">\r\n                            <mat-icon *ngIf=\"wellnessbereichFormGroup.controls.anzahl_der_saunen.valid\" class=\"success-icon\" matSuffix >done</mat-icon>\r\n                </mat-form-field>\r\n\r\n                <h6>Wie viele Wochen im Jahr ist Ihr Wellnessbereich geöffnet?</h6>\r\n                <mat-form-field class=\"full-width\">\r\n                            <mat-select [value]=\"wellnessbereichFormGroup.controls.Oeffnungswochen_pro_jahr.value\" placeholder=\"Öffnungswochen des Wellnessbereichs pro Jahr\" [formControl]=\"wellnessbereichFormGroup.controls.Oeffnungswochen_pro_jahr\"> \r\n                                <mat-option *ngFor=\"let kalenderwoche of kalenderwochen; let i = index\" [value]=\"i+1\" >\r\n                                    {{i+1}}\r\n                                </mat-option>\r\n                            </mat-select>\r\n                </mat-form-field>\r\n            </div>                          \r\n        </form>\r\n    </div>  \r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/wasser/wasser.component.ts":
/*!********************************************!*\
  !*** ./src/app/wasser/wasser.component.ts ***!
  \********************************************/
/*! exports provided: WasserComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WasserComponent", function() { return WasserComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _models_wellnessbereich__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../_models/wellnessbereich */ "./src/app/_models/wellnessbereich.ts");
/* harmony import */ var _services_pruefservice_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../_services/pruefservice.service */ "./src/app/_services/pruefservice.service.ts");
/* harmony import */ var _services_data_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../_services/data.service */ "./src/app/_services/data.service.ts");
/* harmony import */ var _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/slide-toggle */ "./node_modules/@angular/material/esm5/slide-toggle.es5.js");
/* harmony import */ var _angular_material_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/core */ "./node_modules/@angular/material/esm5/core.es5.js");








var WasserComponent = /** @class */ (function () {
    function WasserComponent(Dataservice, Pruefservice) {
        this.Dataservice = Dataservice;
        this.Pruefservice = Pruefservice;
        this.matcher = new _angular_material_core__WEBPACK_IMPORTED_MODULE_7__["ErrorStateMatcher"]();
        this.Wellnessbereich = new _models_wellnessbereich__WEBPACK_IMPORTED_MODULE_3__["Wellnessbereich"]();
        this.kalenderwochen = Array(52).map(function (x, i) { return i; });
        this.WasserFormGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            wasserverbrauch_in_m3: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])
        });
        this.wellnessbereichFormGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            checkedFormControl: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
            groesse_in_m2: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            anzahl_der_saunen: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            volumen_schwimmbad_in_m3: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            Oeffnungswochen_pro_jahr: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])
        });
    }
    WasserComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.Dataservice.UmweltchecksBhobj.subscribe(
        //Problem: wird einmal aufgerufnen beim Konstruktor und einmal, wenn die Datengeupdated werden.
        function (Umweltchecks) {
            console.log("Wasser-COMPONENTE:", Umweltchecks);
            if (Umweltchecks.Umweltchecks) {
                _this.pullDatainWasserComponente();
            }
        }, function (error) { console.log("ERROR in Wassercomponente.", error); });
        //Listen to saveBetriebsdaten-Event vom Parentcomponent: eingabe.component => SAVE local Data!
        this.saveWasser.subscribe(function () {
            //lokal: schreibe die Werte in die Tonnen, dann in den Dataservice -
            _this.saveWasserInDataservice();
        }, function (err) { console.log(err); }, function (complete) { console.log("Wasser erfolgereich vom Frontent versendet"); });
        this.pruefeEingabe.subscribe(function () {
            _this.pruefEingabeDerWasserComponente();
        });
    };
    WasserComponent.prototype.pullDatainWasserComponente = function () {
        var _this = this;
        this.WasserFormGroup.controls.wasserverbrauch_in_m3.setValue(this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.wasserverbrauch_in_m3);
        //die Einrichtung hat einen Wellnessbereich, wenn Werte exisitieren:
        if (this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.Wellnessbereich && this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.Wellnessbereich.groesse_in_m2 > 0) {
            //Toggle umstellen
            this._SlideToggle_wellness.toggle();
            //eine Sekunde warten, weil ng-if das erst in die DOM laden muss #AngularFail
            setTimeout(function () {
                _this.wellnessbereichFormGroup.controls.groesse_in_m2.setValue(_this.Dataservice.Umweltchecks.Umweltchecks[_this.Dataservice.NummerUmweltcheck].Einrichtung.Wellnessbereich.groesse_in_m2);
                _this.wellnessbereichFormGroup.controls.volumen_schwimmbad_in_m3.setValue(_this.Dataservice.Umweltchecks.Umweltchecks[_this.Dataservice.NummerUmweltcheck].Einrichtung.Wellnessbereich.volumen_schwimmbad_in_m3);
                _this.wellnessbereichFormGroup.controls.anzahl_der_saunen.setValue(_this.Dataservice.Umweltchecks.Umweltchecks[_this.Dataservice.NummerUmweltcheck].Einrichtung.Wellnessbereich.anzahl_der_saunen);
                _this.wellnessbereichFormGroup.controls.Oeffnungswochen_pro_jahr.setValue(_this.Dataservice.Umweltchecks.Umweltchecks[_this.Dataservice.NummerUmweltcheck].Einrichtung.Wellnessbereich.Oeffnungswochen_pro_jahr);
                console.log("BETRIEBSDATEN: Oeffnungswochen pro Jahr:", _this.Dataservice.Umweltchecks.Umweltchecks[_this.Dataservice.NummerUmweltcheck].Einrichtung.Wellnessbereich.Oeffnungswochen_pro_jahr);
                _this.selected_oeffnungswochen = _this.Dataservice.Umweltchecks.Umweltchecks[_this.Dataservice.NummerUmweltcheck].Einrichtung.Wellnessbereich.Oeffnungswochen_pro_jahr;
                //this.getOeffnungswochen_pro_jahr(this.wellnessbereichFormGroup.controls.Oeffnungswochen_pro_jahr.value)
                // warum nimmt er nicht den richtigen Value?
            }, 500);
        }
    };
    WasserComponent.prototype.saveWasserInDataservice = function () {
        if (this.umweltcheckSubscription) {
            this.umweltcheckSubscription.unsubscribe();
        }
        //test
        this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.wasserverbrauch_in_m3 = Number(this.WasserFormGroup.controls.wasserverbrauch_in_m3.value);
        if (this._SlideToggle_wellness.checked) { //toggle Wellnessbereich checked
            //die Einrichtung hat einen Wellnessbereich, wenn Werte exisitieren:
            //update alle Felder Wellnessbereich oder lege an ,if null:
            //Wenn der getoggle ist: update, wenn nicht, lösche die WellnessbereichID im Parent?
            //UPDATE Wellnessbereich: 
            if (this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.Wellnessbereich) {
                this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.Wellnessbereich.groesse_in_m2 = Number(this.wellnessbereichFormGroup.controls.groesse_in_m2.value);
                this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.Wellnessbereich.volumen_schwimmbad_in_m3 = Number(this.wellnessbereichFormGroup.controls.volumen_schwimmbad_in_m3.value);
                this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.Wellnessbereich.anzahl_der_saunen = Number(this.wellnessbereichFormGroup.controls.anzahl_der_saunen.value);
                this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.Wellnessbereich.Oeffnungswochen_pro_jahr = Number(this.wellnessbereichFormGroup.controls.Oeffnungswochen_pro_jahr.value);
                this.Dataservice.saveWellnessbereich();
            }
            else {
                //lege neuen an Wellnessbereich an:
                //nutze lokalen Wellnessberich     
                this.Wellnessbereich.groesse_in_m2 = Number(this.wellnessbereichFormGroup.controls.groesse_in_m2.value);
                this.Wellnessbereich.volumen_schwimmbad_in_m3 = Number(this.wellnessbereichFormGroup.controls.volumen_schwimmbad_in_m3.value);
                this.Wellnessbereich.Oeffnungswochen_pro_jahr = Number(this.wellnessbereichFormGroup.controls.Oeffnungswochen_pro_jahr.value);
                //füge in den Dataservice ein:
                this.Dataservice.Umweltchecks.Umweltchecks[this.Dataservice.NummerUmweltcheck].Einrichtung.Wellnessbereich = this.Wellnessbereich;
                this.Dataservice.saveWellnessbereich();
            }
        }
        else { //toggle auf Off  
            //lösche den Wellnessbereich auf dem Server
            this.Dataservice.deleteWellnessbereich();
        }
        //schicke Request ab Wasser steht da drin 
        this.Dataservice.saveEinrichtung();
        //jetzt Behavoir Object updaten: 
        this.Dataservice.UmweltchecksBhobj.next(this.Dataservice.Umweltchecks);
    };
    WasserComponent.prototype.pruefEingabeDerWasserComponente = function () {
        if (this.WasserFormGroup.controls.wasserverbrauch_in_m3.invalid)
            this.Pruefservice.pushNameErrorArray("Wasserverbrauch in m³", this.WasserFormGroup.controls.wasserverbrauch_in_m3.errors);
        if (this._SlideToggle_wellness.checked) {
            if (this.wellnessbereichFormGroup.controls.groesse_in_m2.invalid)
                this.Pruefservice.pushNameErrorArray("Größe in m²", this.wellnessbereichFormGroup.controls.groesse_in_m2.errors);
            if (this.wellnessbereichFormGroup.controls.anzahl_der_saunen.invalid)
                this.Pruefservice.pushNameErrorArray("Anzahl der Saunen", this.wellnessbereichFormGroup.controls.anzahl_der_saunen.errors);
            if (this.wellnessbereichFormGroup.controls.volumen_schwimmbad_in_m3.invalid)
                this.Pruefservice.pushNameErrorArray("Volumen Schwimmbad in m³", this.wellnessbereichFormGroup.controls.volumen_schwimmbad_in_m3.errors);
            if (this.wellnessbereichFormGroup.controls.Oeffnungswochen_pro_jahr.invalid)
                this.Pruefservice.pushNameErrorArray("Öffnungswochen pro Jahr", this.wellnessbereichFormGroup.controls.Oeffnungswochen_pro_jahr.errors);
            this.Pruefservice.pushInvalidFormgroup("Wellnessbereich", this.wellnessbereichFormGroup);
        }
        /*this.wellnessbereichFormGroup = new FormGroup({
            checkedFormControl  :new FormControl() ,
            groesse_in_m2 :new FormControl('', [Validators.required]),
            anzahl_der_saunen : new FormControl('', [Validators.required]),
            volumen_schwimmbad_in_m3 :new FormControl('', [Validators.required]),
            Oeffnungswochen_pro_jahr :new FormControl('', [Validators.required,Validators.min(0)])
          })*/
    };
    WasserComponent.prototype.getOeffnungswochen_pro_jahr = function (valueOption, valueSelection) {
        if (this.wellnessbereichFormGroup && this.wellnessbereichFormGroup.controls.Oeffnungswochen_pro_jahr.value) {
            console.log("FUNKTIONSAUFRUFFFFFFFFF");
            return valueOption === valueSelection;
        }
        return false;
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], WasserComponent.prototype, "saveWasser", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], WasserComponent.prototype, "pruefeEingabe", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('w'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_6__["MatSlideToggle"])
    ], WasserComponent.prototype, "_SlideToggle_wellness", void 0);
    WasserComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-wasser',
            template: __webpack_require__(/*! ./wasser.component.html */ "./src/app/wasser/wasser.component.html"),
            styles: [__webpack_require__(/*! ./wasser.component.css */ "./src/app/wasser/wasser.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_data_service__WEBPACK_IMPORTED_MODULE_5__["DataService"], _services_pruefservice_service__WEBPACK_IMPORTED_MODULE_4__["PruefService"]])
    ], WasserComponent);
    return WasserComponent;
}());



/***/ }),

/***/ "./src/assets/app.css":
/*!****************************!*\
  !*** ./src/assets/app.css ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "a {\r\n    cursor: pointer;\r\n}\r\n\r\n.help-block {\r\n    font-size: 12px;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hc3NldHMvYXBwLmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGVBQWU7QUFDbkI7O0FBRUE7SUFDSSxlQUFlO0FBQ25CIiwiZmlsZSI6InNyYy9hc3NldHMvYXBwLmNzcyIsInNvdXJjZXNDb250ZW50IjpbImEge1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG59XHJcblxyXG4uaGVscC1ibG9jayB7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_4__);





if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\tarik\Google Drive\BUSINESS\Go-Projects\quickcheck-umwelt_angular6\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map